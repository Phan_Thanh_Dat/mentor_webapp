# cyf_app
Choose your future app for mobile - v1

## Setup Database

- DB add role choos562_cyf and choos562_cis
- DB add table choos562_cyf then import choos562_cyf.sql
- DB add table choos562_cis then import choos562_cis.sql

## Deploy
- Copy `.env.example` to `.env` then change API_URL to target environment
- npm install
- npm run fe:build

## Localization
- Create new translation id in _fe_src_
- Run `npm run fe:lang` to scan _fe_src_ and add new translations to api/lang
- Update language files in _api/lang_
- Upload api/lang to server 
