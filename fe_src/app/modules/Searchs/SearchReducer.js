import update from 'immutability-helper';

const SearchReducer = (state, action) => {
  switch (action.type) {
    case 'TOGGLE_SEARCH_BOX':
      if (!action.payload) {
        return update(state, {
          isOpen: { $set: false },
          results: { $set: [] },
          isOpenSearchBox: { $set: false },
        });
      }
      return update(state, { isOpenSearchBox: { $set: action.payload } });
    case 'SEARCH_RESULTS':
      return update(state, { results: { $set: action.payload }, isOpen: { $set: true } });
    default:
      return state || {
        isOpen: false,
        isOpenSearchBox: false,
        results: [],
      };
  }
};

export default SearchReducer;
