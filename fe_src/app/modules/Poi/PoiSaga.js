import { put, takeLatest, all, call } from 'redux-saga/effects';
import { fetchApi, APIError } from '../../helpers/api';

function* getPois({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    const requestUrl = payload.radius && payload.lat && payload.lng ?
      `&distance=${payload.radius}&lat=${payload.lat}&long=${payload.lng}` : '';

    const res = yield call(fetchApi,
      `${process.env.API_URL}/pois?fcategs={Personal}${requestUrl}`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'get',
      },
    );

    yield put({
      type: 'SET_POIS',
      payload: res,
    });

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
  }
}

function* createPoi({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    const res = yield call(fetchApi,
      `${process.env.API_URL}/pois`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify(payload),
      },
    );

    yield put({
      type: 'PUSH_POI',
      payload: res,
    });

    meta.addNotification({
      message: meta.successMessage,
      level: 'success',
    });

    meta.callback(true);

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
    meta.callback();
  }
}

function* updatePoi({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    const res = yield call(fetchApi,
      `${process.env.API_URL}/pois`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'put',
        body: JSON.stringify(payload),
      },
    );

    yield put({
      type: 'UPDATE_POIS_ENTITY',
      payload: res,
      meta: { index: meta.index },
    });

    meta.addNotification({
      message: meta.successMessage,
      level: 'success',
    });

    meta.callback(true);

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    console.log(e);
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
    meta.callback();
  }
}

function* deletePoi({ payload, meta }) {
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    yield call(fetchApi,
      `${process.env.API_URL}/pois`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'delete',
        body: JSON.stringify({
          id: payload.id,
        }),
      },
    );

    yield put({
      type: 'DELETE_POIS_ENTITY',
      payload: payload.index,
    });

    meta.addNotification({
      message: meta.successMessage,
      level: 'success',
    });

    meta.callback(true);

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
    meta.callback();
  }
}

function* watchSagaPoi() {
  yield takeLatest('CREATE_POI', createPoi);
  yield takeLatest('GET_POIS', getPois);
  yield takeLatest('DELETE_POI', deletePoi);
  yield takeLatest('UPDATE_POI', updatePoi);
}

export default function* rootSaga() {
  yield all([
    watchSagaPoi(),
  ]);
}
