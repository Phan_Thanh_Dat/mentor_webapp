import { put, takeLatest, all, call } from 'redux-saga/effects';
import { fetchApi } from '../../helpers/api';

function* getSubscriptions() {
  try {
    const res = yield call(fetchApi,
      `${process.env.API_URL}/interests`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'get',
      },
    );

    yield put({
      type: 'SET_SUBSCRIPTIONS',
      payload: res,
    });
  } catch (e) {
    console.error(e);
  }
}

function* watchSagaSubscriptions() {
  yield takeLatest('GET_EVENTS', getSubscriptions);
}

const saga = function* rootSaga() {
  yield all([
    watchSagaSubscriptions(),
  ]);
};

const name = 'subscriptionsSaga';

export {
  name,
  saga,
};

