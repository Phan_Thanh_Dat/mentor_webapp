import update from 'immutability-helper';

const subscriptionsReducer = (state, action) => {
  switch (action.type) {
    case 'SET_SUBSCRIPTIONS':
      const response = action.payload;
      return state.length === 0 ?
        update(state, { $push: response && response.response ? response.response : [] }) : state;
    default:
      return state || [];
  }
};

export default subscriptionsReducer;
