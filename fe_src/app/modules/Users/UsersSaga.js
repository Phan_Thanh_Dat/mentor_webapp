import { put, takeLatest, all, call } from 'redux-saga/effects';
import { fetchApi, APIError } from '../../helpers/api';

function* checkUser(action) {
  try {
    const res = yield call(fetchApi,
      `${process.env.API_URL}/user`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
      },
    );

    yield put({
      type: 'CHECK_USER_RESPONSE',
      payload: res,
    });

    if (action.meta && action.meta.callback) {
      action.meta.callback(res.response);
    }
  } catch (e) {
    // window.location = process.env.LOGIN_URL;
    alert(`Cannot login. Please contact for support.\n\nError message: ${e.message}`);
  }
}

function* updateUser(action) {
  const { meta, payload } = action;
  try {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: true,
    });

    yield call(fetchApi,
      `${process.env.API_URL}/user/change-lang`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify(payload),
      },
    );

    yield put({
      type: 'CHANGE_LANGUAGE',
      payload,
    });

    yield put({
      type: 'GET_LANG',
      payload,
    });

    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    yield call(meta.addNotification, {
      message: meta.successMessage,
      level: 'success',
    });
  } catch (e) {
    yield put({
      type: 'LOADING_FULL_PAGE',
      payload: false,
    });

    meta.addNotification({
      message: e instanceof APIError ? e.message : 'There is a problem in our server. Please try the action again.',
      level: 'error',
    });
  }
}

function* getLang(action) {
  const { payload } = action;
  try {
    const res = yield call(fetchApi,
      `${process.env.API_URL}/lang?lang=${payload.lang}`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'get',
      },
    );

    yield put({
      type: 'SET_LANG',
      payload: res,
    });
  } catch (e) {
    console.log(e);
  }
}

function* watchSagaUser() {
  yield takeLatest('CHECK_USER', checkUser);
  yield takeLatest('UPDATE_USER', updateUser);
  yield takeLatest('GET_LANG', getLang);
}

const saga = function* rootSaga() {
  yield all([
    watchSagaUser(),
  ]);
};

const name = 'userSaga';

export {
  name,
  saga,
};

