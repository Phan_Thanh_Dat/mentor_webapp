const EMPTY_ARRAY = [];

function formatObjectStringToArray(data) {
  return data.replace(new RegExp(/\"|\{|\}/, 'g'), '').split(',');
}

function formatObjectStringValueSelect(data) {
  return data.replace(new RegExp(/\"|\{|\}/, 'g'), '');
}

function formatObjectStringToArraySelect(data, label, value) {
  let formattedData = data;

  if (typeof data === 'string') {
    formattedData = formatObjectStringToArray(data);
  }

  return formattedData.reduce((final, current) => {
    const newFinal = final.slice(0);
    newFinal.push({
      label: typeof current === 'object' ? current[label] : current,
      value: typeof current === 'object' ? current[value] : current,
    });
    return newFinal;
  }, EMPTY_ARRAY);
}

export {
  formatObjectStringValueSelect,
  formatObjectStringToArray,
  formatObjectStringToArraySelect,
};
