import React from 'react';
import { SingleDatePicker } from 'react-dates';
import { pure } from 'recompose';
import 'react-dates/lib/css/_datepicker.css';
import './datePicker.css';


const DatePickerCom = pure(SingleDatePicker);


class DatePicker extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      date: null,
      focused: false,
    };
  }

  _onDateChange = (date) => {
    this.setState({ date });
  };

  _onFocus = (focused) => {
    this.setState({ focused: focused.focused });
  };

  render() {
    return (
      <DatePickerCom
        name="date"
        withPortal
        placeholder=" "
        date={this.state.date} // momentPropTypes.momentObj or null
        onDateChange={this._onDateChange} // PropTypes.func.isRequired
        focused={this.state.focused} // PropTypes.bool
        onFocusChange={this._onFocus} // PropTypes.func.isRequired
      />
    );
  }
}

export default DatePicker;
