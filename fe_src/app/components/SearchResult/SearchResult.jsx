import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';

import { IconCancel } from '../Icon';
import Result from './Result';
import locales from './searchLocales';
import configStyled from '../../styledConfig';

const Results = styled.div`
  height: 100vh;
  width: 100%;
  position: fixed;
  top: 0;
  margin-top: ${configStyled.headerHeight + configStyled.searchHeight}px;
  background-color: #fff;
  z-index: ${configStyled.zIndexMax};
  overflow-y: auto;
`;

const TextNoResult = styled.div`
  flex: 1;
  font-size: 20px;
  text-align: center;
  position: absolute;
  top: 50%;
  width: 100%;
  margin-top: -92px;
`;

const Content = styled.div`
  padding: 20px 15px ${configStyled.headerHeight + configStyled.searchHeight + 20}px;
`;

const Title = styled.div`
  font-size: 20px;
  margin-bottom: 18px;
`;

const ResultStyle = styled(Result)`
  &:not(:first-child) {
    margin-top: 18px;
  }
`;

const Close = styled.div`
  font-size: 24px;
  position: fixed;
  top: ${configStyled.searchHeight + configStyled.headerHeight + 10}px;
  right: 20px;
`;

function getIntroContent(contents) {
  const contentIntro = contents.content.find(content => content.type === 'textIntro');
  return contentIntro && typeof contentIntro.content === 'string' ? contentIntro.content : '';
}

const SearchResult = ({ isOpen, results, closeSearchBox, intl }) => (

  <Results hidden={!isOpen}>
    <Close onClick={closeSearchBox}>
      <IconCancel />
    </Close>
    <TextNoResult hidden={results.length !== 0}>
      {intl.formatMessage(locales.noResults)}
    </TextNoResult>
    <Content hidden={results.length === 0}>
      <Title>
        {intl.formatMessage(locales.results)}
      </Title>
      <div>
        {
          results.map((result) => {
            const content = JSON.parse(result.content);
            return (
              <ResultStyle
                onClick={closeSearchBox}
                page={result.descr}
                key={result.id}
                content={getIntroContent(content)}
                title={content.title}
              />
            );
          })
        }
      </div>
    </Content>
  </Results>
);

SearchResult.propTypes = {
  intl: intlShape,
  isOpen: PropTypes.bool,
  results: PropTypes.array,
  closeSearchBox: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  results: state.search.results,
  isOpen: state.search.isOpen,
});

const mapDispatchToProps = dispatch => ({
  closeSearchBox: () => {
    dispatch({ type: 'TOGGLE_SEARCH_BOX', payload: false });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(SearchResult));
