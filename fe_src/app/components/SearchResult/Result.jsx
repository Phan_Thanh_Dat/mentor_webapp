import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import config from '../../styledConfig';

const Wrap = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  
  > a {
    color: ${config.colorText};
    text-decoration: none;
  }
`;

const Title = styled.div`
  font-size: 26px;
  font-weight: bold;
  margin-bottom: 4px;
  color: #000;
`;

const Text = styled.div`
  padding-right: 14px;
  padding-left: 14px;
  font-size: 16px;
`;

const Border = styled.div`
  margin-top: 20px;
  height: 1px;
  width: 100%;
  background-color: rgba(204, 3, 60, .3);
`;

const Result = ({ className, title, content, page, onClick }) => (
  <Wrap className={className} onClick={onClick}>
    <Link to={`/info/detail?page=${page}`}>
      <Title>{title}</Title>
      <Text
        dangerouslySetInnerHTML={{
          __html: content,
        }}
      />
      <Border />
    </Link>
  </Wrap>
);

Result.propTypes = {
  onClick: PropTypes.func,
  content: PropTypes.string,
  page: PropTypes.string,
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
};


export default Result;
