import { defineMessages } from 'react-intl';

const messages = defineMessages({
  results: {
    id: 'search.results',
    defaultMessage: 'Search results',
  },
  noResults: {
    id: 'search.noResults',
    defaultMessage: 'No results found',
  },
});

export default messages;
