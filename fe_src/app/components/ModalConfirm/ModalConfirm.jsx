import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ModalBase from 'react-modal';

import styledConfig from '../../styledConfig';

const styleDefaultOverlay = {
  top: 0,
  bottom: 0,
  zIndex: styledConfig.zIndexMax - 1,
  overflow: 'auto',
  backgroundColor: 'transparent',
};

const styleDefaultContent = {
  position: 'static',
  top: '0',
  left: '0',
  bottom: '0',
  right: '0',
  padding: '0',
  border: '0',
  boxSizing: 'border-box',
  boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
  borderRadius: '0',
  width: '100%',
  margin: '0',
  fontFamily: styledConfig.fontFamily,
  background: 'rgba(255,255,255,0.9)',
  WebkitOverflowScrolling: 'touch',
  height: '100%',
};

const Content = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;

const ModalConfirm = (
  {
   contentLabel,
   preventClose,
   styleOverlay,
   styleContent,
   onRequestClose,
   children,
   theme,
   ...props
  },
   ) => (

     <ModalBase
       contentLabel={contentLabel}
       {...props}
       onRequestClose={preventClose ? null : onRequestClose}
       style={{
         overlay: Object.assign({}, styleDefaultOverlay, styleOverlay),
         content: Object.assign({}, styleDefaultContent, styleContent),
       }}
     >
       <Content>
         {children}
       </Content>
     </ModalBase>

);

ModalConfirm.propTypes = {
  preventClose: PropTypes.bool,
  onRequestClose: PropTypes.func,
  contentLabel: PropTypes.string,
  theme: PropTypes.object,
  styleOverlay: PropTypes.object,
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
};
export default ModalConfirm;
