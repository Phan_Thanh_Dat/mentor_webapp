import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import config from '../../styledConfig';

const HeaderBase = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: ${config.headerHeight}px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  z-index: 99;
`;

const HeaderHome = HeaderBase.extend`
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover; 
  background-image: url("${require('./img/header.png')}");
  
  @media (min-width: 600px) {
    background-image: url("${require('./img/header@2x.png')}");
  }
  
  @media (min-width: 1000px) {
    background-image: url("${require('./img/header@3x.png')}");
  }
`;

const HeaderUser = HeaderBase.extend`
  background-color: #a50082;
`;

const HeaderEvents = HeaderBase.extend`
  background-color: #14AA3C;
`;

const HeaderMap = HeaderBase.extend`
  background-color: #C80041;
`;


const Header = ({ location, children }) => {
  let HeaderRender;
  switch (location.pathname.split('/')[1]) {
    case 'user':
      HeaderRender = HeaderUser;
      break;
    case 'events':
      HeaderRender = HeaderEvents;
      break;
    case 'map' :
      HeaderRender = HeaderMap;
      break;
    default:
      HeaderRender = HeaderHome;
      break;
  }

  return (
    <HeaderRender>
      {children}
    </HeaderRender>
  );
};

Header.propTypes = {
  location: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.array,
  ]),
};

export default Header;
