import React from 'react';

const MapMarker = ({ ...props }) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 311.8 311.8"
  >
    <path fill="#662D91" stroke="#FFFFFF" strokeWidth="2" strokeMiterlimit="10" d="M155.9 53.7c-30.5 0-58.3 24.7-58.3 55.3 0 38.4 58.3 116.9 58.3 116.9s58.3-74.6 58.3-116.9c0-30.6-27.8-55.3-58.3-55.3z" />
    <path fill="#FFFFFF" className="st1" d="M172.6 136.3l-17.1-9.5-17.5 8.8 3.7-19.2-13.8-13.9 19.4-2.4 9-17.4 8.3 17.7 19.3 3.2-14.3 13.3z" />
  </svg>
);

export default MapMarker;
