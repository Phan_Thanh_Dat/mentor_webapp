import React from 'react';
import PropTypes from 'prop-types';

const CurrentLocation = ({ height }) => (
  <svg
    height={height || '1em'}
    version="1.1"
    id="marker_color_2_copy"
    xmlns="http://www.w3.org/2000/svg"
    x="0px"
    y="0px"
    viewBox="0 0 311.8 311.8"
  >

    <ellipse
      display="inline"
      fill="#00AEEF"
      stroke="#fff"
      strokeWidth="2"
      strokeMiterlimit="10"
      cx="155.9"
      cy="214.7"
      rx="48.2"
      ry="21.3"
    />
    <path
      display="inline"
      fill="#00AEEF"
      stroke="#fff"
      strokeWidth="2"
      strokeMiterlimit="10"
      d="M155.9,218.7L155.9,218.7
c-20.4,0-37.8-57.2-43.2-77.1c-4.2-15.3,5.6-31.1,24.2-37.7c5.8-2.1,12.3-3.2,19.1-3.2h0c6.8,0,13.3,1.2,19.1,3.2
	c18.6,6.7,28.3,22.4,24.2,37.7C193.7,161.5,176.3,218.7,155.9,218.7z"
    />
    <circle
      display="inline"
      fill="#00AEEF"
      stroke="#fff"
      strokeWidth="2"
      strokeMiterlimit="10"
      cx="155.9"
      cy="89"
      r="27.6"
    />
    <path
      display="inline"
      fill="#00AEEF"
      d="M129.4,120.4c1.8-5.1-5.2-9.2-8.8-5c-5.6,6.4-11.3,16.6-15.7,28.7
	c-4.3,12.1-6.4,23.6-6.1,32.1c0.2,5.5,8.2,6.9,10,1.7c0,0,5.2-23,7.9-30.4C119.1,140.6,129.4,120.4,129.4,120.4z"
    />
    <path
      display="inline"
      fill="#00AEEF"
      d="M182.1,120c-1.8-5.1,5.2-9.2,8.8-5c5.6,6.4,11.3,16.6,15.7,28.7
	c4.3,12.1,6.4,23.6,6.1,32.1c-0.2,5.5-8.2,6.9-10,1.7c0,0-5.2-23-7.9-30.4C192.3,140.1,182.1,120,182.1,120z"
    />
  </svg>

);

CurrentLocation.propTypes = {
  height: PropTypes.string,
};

export default CurrentLocation;
