import React from 'react';

const Heart = ({ ...props }) => (
  <svg fill="currentColor" height="1em" {...props} xmlns="http://www.w3.org/2000/svg" viewBox="456 3272 15 12.312">
    <g id="Group_288" transform="translate(456.062 3272)">
      <g id="Group_277" transform="translate(-0.062)">
        <g id="Group_276" transform="translate(0)">
          <path id="Path_392" d="M14.557,2.163A3.286,3.286,0,0,0,11.1.19h-.057A3.875,3.875,0,0,0,7.842,1.935l-.4.664-.4-.664A4.489,4.489,0,0,0,3.839,0H3.782A3.709,3.709,0,0,0,.33,2.163C-.524,4,.045,6.412,1.79,8.442a16.55,16.55,0,0,0,5.634,3.87,15.928,15.928,0,0,0,5.634-3.87C14.841,6.412,15.392,4,14.557,2.163Z" transform="translate(0.062)" />
        </g>
      </g>
    </g>
  </svg>
);

export default Heart;
