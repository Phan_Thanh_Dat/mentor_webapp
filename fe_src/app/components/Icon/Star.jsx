import React from 'react';

const Star = ({ ...props }) => (
  <svg {...props} xmlns="http://www.w3.org/2000/svg" viewBox="351.146 3258 17.454 17.1" fill="currentColor" height="1em">
    <path id="Path_191" d="M15.277,18.243,9.866,15.355,4.488,18.3l1-6.182L1.1,7.768l6.025-.936L9.791,1.2l2.727,5.6,6.036.868-4.342,4.4Z" transform="translate(350.046 3256.8)" />
  </svg>
);

export default Star;
