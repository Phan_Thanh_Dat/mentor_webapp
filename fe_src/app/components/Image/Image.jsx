import React from 'react';
import PropType from 'prop-types';

const ImageRaw = (props) => {
  const { src, srcSet, alt, className, ...otherProps } = props;
  return (
    <picture className={className}>
      { srcSet ?
        srcSet.map(set => (
          <source
            key={set.media}
            media={set.media}
            srcSet={set.srcset}
          />
        )) : ''
      }
      <img src={src} alt={alt} {...otherProps} />
    </picture>
  );
};

ImageRaw.propTypes = {
  src: PropType.string.isRequired,
  className: PropType.string,
  srcSet: PropType.array,
  alt: PropType.string.isRequired,
};

export default ImageRaw;
