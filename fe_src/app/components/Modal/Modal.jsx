import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ModalBase from 'react-modal';
import IconClose from 'react-icons/lib/md/close';

import styledConfig from '../../styledConfig';

const styleDefaultOverlay = {
  zIndex: styledConfig.zIndexMax - 1,
  overflow: 'auto',
  backgroundColor: 'rgba(255, 255, 255, 0.5)',
};

const styleDefaultContent = {
  position: 'static',
  top: '22px',
  padding: '0',
  border: '0',
  boxSizing: 'border-box',
  boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
  borderRadius: '5px',
  width: '90%',
  maxWidth: '500px',
  margin: '20px auto',
  fontFamily: styledConfig.fontFamily,
  background: 'rgba(255,255,255,0.9)',
  WebkitOverflowScrolling: 'touch',
};

const ModalHeader = styled.header`
  padding-top: 12px;
  padding-bottom: 12px;
  position: relative;
  text-align: center;
  font-size: 27px;
  color: #000;
  box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
`;

const ModalContent = styled.div`
  color: ${styledConfig.colorText};
`;

const CloseSection = styled.span`
  position: absolute;
  font-size: 35px;
  left: 10px;
  top: 50%;
  margin-top: -20.5px;
  display: inline-block;
  height: 35px;
  width: 35px;
  line-height: 35px;
  color: ${styledConfig.colorText};
`;

const Modal = ({ contentLabel,
  preventClose,
  styleOverlay,
  styleContent,
  onRequestClose,
  children,
  theme,
  ...props }) => (

    <ModalBase
      {...props}
      onRequestClose={preventClose ? null : onRequestClose}
      contentLabel={contentLabel}
      style={{
        overlay: Object.assign({}, styleDefaultOverlay, styleOverlay),
        content: Object.assign({}, styleDefaultContent, styleContent),
      }}
    >
      <ModalHeader>
        <CloseSection onClick={onRequestClose}>
          <IconClose />
        </CloseSection>
        {contentLabel}
      </ModalHeader>
      <ModalContent>
        {children}
      </ModalContent>
    </ModalBase>

);


Modal.propTypes = {
  preventClose: PropTypes.bool,
  onRequestClose: PropTypes.func,
  contentLabel: PropTypes.string,
  theme: PropTypes.object,
  styleOverlay: PropTypes.object,
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
};

export default Modal;
