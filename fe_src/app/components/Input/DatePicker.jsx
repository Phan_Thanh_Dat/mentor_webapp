import React from 'react';
import { SingleDatePicker } from 'react-dates';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { pure } from 'recompose';
import 'react-dates/lib/css/_datepicker.css';
import './datePicker.css';
import config from '../../styledConfig';

const Label = styled.label`
  font-size: 18px;
  margin-right: 7px;
`;

const Wrap = styled.div`
  .DateInput__display-text {
    border: ${({ error }) => error ? `1px solid ${config.colorError};` : `1px solid ${config.colorBorder};`}
  }
`;

const DatePickerCom = pure(SingleDatePicker);


class DatePicker extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      date: null,
      focused: false,
    };
  }

  _onDateChange = (date) => {
    if (!this.props.controlled) {
      this.setState({ date });
    } else
    if (this.props.onChange) {
      this.props.onChange({
        persist() {

        },
        target: {
          value: date,
          name: this.props.name,
        },
      });
    }
  };

  _onFocus = (focused) => {
    this.setState({ focused: focused.focused });
  };

  _getValue = (controlled, value, state) => {
    if (controlled) {
      return this.props.value ? this.props.value : null;
    }
    return state;
  };

  render() {
    const { label, name, error } = this.props;
    return (
      <Wrap error={error}>
        <Label htmlFor={name}>{label}</Label>
        <DatePickerCom
          displayFormat={this.props.displayFormat}
          showClearDate
          numberOfMonths={1}
          withPortal
          placeholder=" "
          date={this._getValue(this.props.controlled, this.props.value, this.state.date)}
          onDateChange={this._onDateChange}
          focused={this.state.focused}
          onFocusChange={this._onFocus}
        />
      </Wrap>
    );
  }
}

DatePicker.propTypes = {
  value: PropTypes.any,
  controlled: PropTypes.bool,
  error: PropTypes.bool,
  onChange: PropTypes.func,
  name: PropTypes.string,
  label: PropTypes.string,
  displayFormat: PropTypes.string,
};

export default DatePicker;
