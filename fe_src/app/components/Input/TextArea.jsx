import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import config from '../../styledConfig';

const TextAreaInput = styled.textarea`
  font-size: 19px;
  box-sizing: border-box;
  display: block;
  padding: 5px 15px;
  border-radius: 5px;
  width: 100%;
  border: 1px solid ${config.colorBorder};
  outline: none;
  
  &:focus {
    border: 1px solid ${config.colorBorder};
  }
`;

const Label = styled.label`
  display: block;
  margin-bottom: 5px;
  font-style: normal;
  font-size: 18px;
`;

const TextArea = (
  {
    theme,
    style,
    className,
    name,
    label,
    pending,
    error,
    valid,
    value,
    errorMessage,
    ...rest
  }) => (
    <div className={className}>
      <Label htmlFor={name}>{label}</Label>
      <TextAreaInput name={name} {...rest} value={value} />
    </div>

);

TextArea.propTypes = {
  className: PropTypes.string,
  theme: PropTypes.object,
  label: PropTypes.string,
  name: PropTypes.string,
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  valid: PropTypes.bool,
  pending: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ]),
  style: PropTypes.object,
};

export default TextArea;
