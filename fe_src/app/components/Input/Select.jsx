import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import onClickOutside from 'react-onclickoutside';
import isEqual from 'lodash.isequal';
import { compose, withHandlers } from 'recompose';

import config from '../../styledConfig';
import Image from '../Image';

const InputRaw = styled.input`
  font-size: 19px;
  box-sizing: border-box;
  display: block;
  padding: 5px 25px 5px 15px;
  border-radius: ${props => props.open && props.noBorder ? '0' : '5px'};
  width: 100%;
  border: ${({ error, noBorder }) => {
    if (noBorder) return '0;';
    return error ? `1px solid ${config.colorError};` : `1px solid ${config.colorBorder};`;
  }}

  text-align: ${props => props.center ? 'center' : 'left'};
  background-color: ${props => props.open && props.noBorder ? '#656565' : '#fff'};
  outline: none;
  color: ${props => props.open && props.noBorder ? '#fff' : 'inherit'};
  &:focus {
    border: ${props => props.noBorder ? '0' : `1px solid ${config.colorBorder}`};
  }
`;

const Label = styled.label`
  display: block;
  margin-bottom: ${props => props.center ? '14px' : '5px'};
  text-align: ${props => props.center ? 'center' : 'left'};
  font-style: normal;
  font-size: 20px;
`;
const WrapAll = styled.div`
  text-align: ${props => props.center ? 'center' : 'left'};
`;

const WrapInput = styled.div`
  position: relative;
  display: ${props => props.center ? 'inline-block' : 'block'};
`;

const Icon = styled(Image)`
  position: absolute;
  right: 8px;
  top: 50%;
  margin-top: -6px;
  width: 15px;
`;

const List = styled.div`
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
  position: absolute;
  left: 0;
  top 100%;
  z-index: 1;
  width: 100%;
  background-color: rgba(255, 255, 255, 0.9);
  box-sizing: border-box;
  max-height: 40vh;
  overflow: auto;
`;

const SelectBox = styled.div`
  padding: 10px 12px;
  box-sizing: border-box;
  font-size: 19px;
`;

const SelectItem = styled.div`
  display: flex;
  align-items: center;
  text-align: left;
  
  > input {
    min-width: 16px;
    margin-right: 3px;
  }
`;

const SelectTu = compose(
  withHandlers({
    onClick: props => () => {
      if (!props.multiple) props.onSelected(props.value, props.label);
    },
  }),
)(props => (
  <SelectBox onClick={props.onClick}>
    {props.children}
  </SelectBox>
));

function _setValueMultiple(current, newValue) {
  if (current) {
    const hold = current.split(',');
    hold.push(newValue);
    return hold.join(',');
  }
  return newValue;
}

function _removeValueMultiple(current, newValue) {
  if (current) {
    const hold = current.split(',');
    const abc = hold.reduce((newCaValue, currentValue) => {
      if (currentValue !== newValue) {
        newCaValue.push(currentValue);
      }
      return newCaValue;
    }, []);
    return abc.join(',');
  }
  return newValue;
}

class Select extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      selectedValue: props.value || props.defaultValue || '',
      selectedLabel: props.value ?
        this._getDisplayLabel(props.multiple, props.value, '', props.placeholder) : (props.placeholder || ''),
    };
  }

  shouldComponentUpdate = (nextProps, nextState) =>
    !(isEqual(nextProps, this.props) && isEqual(nextState, this.state));

  _onClickSelect = (e, label) => {
    this.setState({
      open: false,
      selectedValue: e,
      selectedLabel: label,
    });

    if (this.props.onChange) {
      this.props.onChange({
        persist() {

        },
        target: {
          name: this.props.name,
          value: e,
        },
      });
    }
  };

  _onChangeSelect = (e) => {
    const { controlled, name } = this.props;
    const currentValue = controlled ? this.props.value : this.state.selectedValue;
    const newValue = e.target.getAttribute('data-value');
    const checked = e.target.checked;

    const final = checked ? _setValueMultiple(currentValue, newValue) :
    _removeValueMultiple(currentValue, newValue);

    this.setState({
      selectedValue: checked ? _setValueMultiple(currentValue, newValue) :
        _removeValueMultiple(currentValue, newValue),
    }, () => {
      if (this.props.onChange) {
        this.props.onChange({
          persist() {

          },
          target: {
            name,
            value: final,
          },
        });
      }
    });
  };

  handleClickOutside = () => {
    this.setState({
      open: false,
    });
  };

  open = () => {
    if (!this.props.disabled) {
      this.setState({
        open: !this.state.open,
      });
    }
  };

  _findLabel = (value, data) => {
    const selected = data.find(select => select.value === value);
    return selected.label;
  };

  _getDisplayLabel = (multiple, value, label, holder) => {
    if (multiple) {
      if (value) {
        const selectedLabel = [];
        value.replace(new RegExp(/\"|\{|\}/, 'g'), '').split(',').forEach((v) => {
          selectedLabel.push(this._findLabel(v, this.props.data));
        });

        return selectedLabel.join(', ');
      }
    }
    if (!value) return holder || '';
    return this._findLabel(value, this.props.data) || label;
  };

  _isChecked(val, totalVal, stateTotalVal, controlled) {
    const finalTotal = controlled ? totalVal : stateTotalVal;
    return finalTotal ? finalTotal.indexOf(val) !== -1 : false;
  }

  render() {
    const {
      data,
      className,
      name,
      label,
      error,
      value,
      center,
      noBorder,
      controlled,
      multiple,
    } = this.props;
    return (
      <WrapAll center={center} className={className}>
        {
          label ? (<Label htmlFor={name} center={center}>{label}</Label>) : ''
        }
        <WrapInput center={center}>
          <Icon onClick={this.open} src={require('./img/arrow.png')} alt="arrow" />
          <InputRaw
            error={error}
            open={this.state.open}
            center={center}
            noBorder={noBorder}
            readOnly
            onClick={this.open}
            name={name}
            value={
              this._getDisplayLabel(
                multiple,
                controlled ? value : this.state.selectedValue,
                this.state.selectedLabel,
                this.props.placeholder)
            }
          />
          <List hidden={!this.state.open} innerRef={(ele) => { this.$content = ele; }}>
            {
              data.map(select => (
                <SelectTu
                  key={`${select.label + select.value}_select`}
                  multiple={multiple}
                  onSelected={this._onClickSelect}
                  value={select.value}
                  label={select.label}
                >
                  <SelectItem>
                    {
                      multiple ? (
                        <input
                          checked={
                            this._isChecked(select.value, value, this.state.selectedValue, controlled)
                          }
                          data-value={select.value}
                          data-label={select.label}
                          type="checkbox"
                          onChange={this._onChangeSelect}
                        />
                      ) : ''
                    }
                    {select.label}
                  </SelectItem>
                </SelectTu>
              ))
            }
          </List>
        </WrapInput>
      </WrapAll>

    );
  }
}

Select.propTypes = {
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
  data: PropTypes.array.isRequired,
  className: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  valid: PropTypes.bool,
  pending: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ]),
  defaultValue: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ]),
  center: PropTypes.bool,
  noBorder: PropTypes.bool,
  placeholder: PropTypes.string,
  controlled: PropTypes.bool,
  onChange: PropTypes.func,
};

export default onClickOutside(Select);
