import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import config from '../../styledConfig';

const Wrap = styled.div`
  direction: ${({ type }) => type === 'checkbox' ? 'rtl' : 'ltr'};
  text-align: left;
`;

const InputRaw = styled.input`
  color: ${({ disabled }) => disabled ? '#CCC;' : `${config.colorText};`}
  font-size: 19px;
  box-sizing: border-box;
  display: block;
  padding: 5px 15px;
  border-radius: 5px;
  width: 100%;
  border: ${({ error }) => error ? `1px solid ${config.colorError};` : `1px solid ${config.colorBorder};`}
  outline: none;
`;

const InputCheckbox = styled.input`
  font-size: 19px;
  margin-right: 3px;
`;

const Label = styled.label`
  display: ${({ type }) => type === 'checkbox' ? 'inline' : 'block'};
  margin-bottom: 3px;
  font-style: normal;
  font-size: 18px;
`;

const Message = styled.div`
  margin-top: 4px;
  color: ${config.colorError};
  font-size: 18px;
`;

const Input = (
  {
    hidden,
    type,
    style,
    className,
    name,
    label,
    pending,
    error,
    valid,
    value,
    errorMessage,
    ...rest
  }) => {
  const InputRender = type === 'checkbox' ? InputCheckbox : InputRaw;
  return (
    <Wrap type={type} className={className} hidden={hidden}>
      <Label type={type} htmlFor={name}>{label}</Label>
      <InputRender error={error} name={name} type={type} {...rest} value={value} />
      <Message>{errorMessage}</Message>
    </Wrap>
  );
};

Input.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  error: PropTypes.bool,
  errorMessage: PropTypes.string,
  valid: PropTypes.bool,
  pending: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ]),
  style: PropTypes.object,
};

export default Input;
