import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import onClickOutside from 'react-onclickoutside';

const Wrap = styled.div`
  position: relative;
`;

class UIDropDown extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  _onClickSelect = () => {
    this.setState({
      open: false,
    });
  };

  handleClickOutside = () => {
    this.setState({
      open: false,
    });
  };

  open = () => {
    this.setState({
      open: !this.state.open,
    });
  };

  render() {
    const {
      action: Action,
      dropDown: Content,
    } = this.props;
    return (
      <Wrap>
        <Action open={this.state.open} onClick={this.open} />
        <Content hidden={!this.state.open} />
      </Wrap>
    );
  }
}

UIDropDown.propTypes = {
  action: PropTypes.func,
  dropDown: PropTypes.func,
};

export default onClickOutside(UIDropDown);
