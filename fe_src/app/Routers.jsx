import React from 'react';
import styled from 'styled-components';
import NotificationSystem from 'react-notification-system';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Switch,
  HashRouter as Router,
  Route,
} from 'react-router-dom';
import { RouteTransition } from 'react-router-transition';
import Loadable from 'react-loadable';


import { IntlProvider, addLocaleData } from 'react-intl';

import en from 'react-intl/locale-data/en';
import fi from 'react-intl/locale-data/fi';

import Footer from './components/Footer';
import Header from './components/Header';
import { LoadingPage } from './components/Loading';

import { sagaMiddlewareControl } from './store';
import * as usersSaga from './modules/Users/UsersSaga';
import configStyled from './styledConfig';
import SearchResult from './components/SearchResult';

addLocaleData([...en, ...fi]);

const Body = styled.div`
  position: relative;
  color: ${configStyled.colorText};
  font-family: ${configStyled.fontFamily};
`;
const Loading = () => (
  <h1>Loading</h1>
);

const LoadableMyComponent = Loadable({
  loader: () => import('./pages/Home/Home'),
  loading: Loading,
});

const LoadableEventsPage = Loadable({
  loading: Loading,
  loader: () => import('./pages/Events/Events'),
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
});

const LoadableUserPage = Loadable({
  loading: Loading,
  loader: () => import('./pages/User'),
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
});

const LoadableMapPage = Loadable({
  loading: Loading,
  loader: () => import('./pages/Map'),
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
});

const LoadableDetailPage = Loadable({
  loading: Loading,
  loader: () => import('./pages/Detail'),
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
});

const LoadablePoiPage = Loadable({
  loading: Loading,
  loader: () => import('./pages/Poi'),
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
});

const LoadableInfoPage = Loadable({
  loading: Loading,
  loader: () => import('./pages/Info'),
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
});

const styleNotification = {
  Containers: { // Override the notification item
    DefaultStyle: { // Applied to every notification, regardless of the notification level
      zIndex: configStyled.zIndexMax,
    },
  },
};

class Routers extends React.PureComponent {

  componentWillMount = () => {
    sagaMiddlewareControl.run(usersSaga.name, usersSaga.saga);
  };

  componentDidMount = () => {
    this.props.checkUser({
      callback: this.props.getLang,
    });
  };

  _addNotification = (settings) => {
    this.$notificationSystem.addNotification(settings);
  };

  _renderEventPages = () => (
    <LoadableEventsPage addNotification={this._addNotification} />
  );

  _renderUserPages = () => (
    <LoadableUserPage addNotification={this._addNotification} />
  );

  _renderMapPages = props => (
    <LoadableMapPage {...props} addNotification={this._addNotification} />
  );

  _renderDetail = props => (
    <LoadableDetailPage {...props} addNotification={this._addNotification} />
  );

  _renderPoi = props => (
    <LoadablePoiPage {...props} addNotification={this._addNotification} />
  );

  _renderInfo = props => (
    <LoadableInfoPage {...props} addNotification={this._addNotification} />
  );

  render() {
    const user = this.props.user.email;
    const messages = this.props.lang;

    return user && Object.keys(messages).length !== 0 ? (
      <IntlProvider locale={this.props.user.lang} messages={messages}>
        <Router>
          <Body>
            <Route render={propsRoute => (
              <Header {...propsRoute} />
        )}
            />

            <Route
              render={({ location }) => (
                <RouteTransition
                  pathname={location.pathname}
                  atEnter={{ opacity: 0.2 }}
                  atLeave={{ opacity: 0 }}
                  atActive={{ opacity: 1 }}
                  mapStyles={styles => (
                    {
                      opacity: styles.opacity,
                    }
            )}
                >
                  <Switch key={location.key} location={location}>
                    <Route exact path="/" component={LoadableMyComponent} />
                    <Route exact path="/user" render={this._renderUserPages} />
                    <Route exact path="/info/detail" render={this._renderDetail} />
                    <Route exact path="/info" render={this._renderInfo} />
                    <Route exact path="/map" render={this._renderMapPages} />
                    <Route exact path="/map/poi" render={this._renderPoi} />
                    <Route
                      exact
                      path="/events"
                      render={this._renderEventPages}
                    />
                  </Switch>
                </RouteTransition>
        )}
            />
            <Route render={propsRoute => (
              <Footer {...propsRoute} />
        )}
            />

            <SearchResult />

            <NotificationSystem
              style={styleNotification}
              ref={(ele) => { this.$notificationSystem = ele; }}
            />

            {
            this.props.loading.isFullPage ? (<LoadingPage spinnerSize="30px" />) : null
          }

          </Body>
        </Router>
      </IntlProvider>
    ) : (<div>Loading</div>);
  }
}

Routers.propTypes = {
  user: PropTypes.object,
  loading: PropTypes.object,
  lang: PropTypes.object,
  checkUser: PropTypes.func,
  getLang: PropTypes.func,
};

const mapStateToProps = state => ({
  user: state.user,
  loading: state.loading,
  lang: state.lang,
});

const mapDispatchToProps = dispatch => ({
  checkUser: (meta) => {
    dispatch({ type: 'CHECK_USER', meta });
  },
  getLang: (lang) => {
    dispatch({ type: 'GET_LANG', payload: lang });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Routers);

