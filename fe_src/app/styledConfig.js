export default {
  searchHeight: 42,
  headerHeight: 50,
  footerHeight: 70,
  mediaMedium: '@media (min-width: 600px)',
  mediaLarge: '@media (min-width: 1000px)',
  fontFamily: 'arial,sans-serif',
  colorText: '#95989a',
  colorError: '#EE6066',
  colorBorder: '#95989a',
  zIndexMax: 99999,
};
