import { defineMessages } from 'react-intl';

const global = defineMessages({
  language: {
    id: 'global.language',
    defaultMessage: 'Language',
  },
  about: {
    id: 'global.about',
    defaultMessage: 'About',
  },
  delete: {
    id: 'global.delete',
    defaultMessage: 'Delete',
  },
  edit: {
    id: 'global.edit',
    defaultMessage: 'Edit',
  },
  cancel: {
    id: 'global.cancel',
    defaultMessage: 'Cancel',
  },
  save: {
    id: 'global.save',
    defaultMessage: 'Save',
  },
  feedback: {
    id: 'global.feedback',
    defaultMessage: 'Feedback',
  },
  logout: {
    id: 'global.logout',
    defaultMessage: 'Logout',
  },
  date: {
    id: 'global.date',
    defaultMessage: 'Date',
  },
  time: {
    id: 'global.time',
    defaultMessage: 'Time',
  },
  contact: {
    id: 'global.contact',
    defaultMessage: 'Contact',
  },
  search: {
    id: 'global.search',
    defaultMessage: 'Search',
  },
  mode: {
    id: 'global.mode',
    defaultMessage: 'Mode',
  },
  addNew: {
    id: 'global.addNew',
    defaultMessage: 'Add new',
  },
});

const filters = defineMessages({
  all: {
    id: 'filters.all',
    defaultMessage: 'All',
  },
  lastMonth: {
    id: 'filters.lastMonth',
    defaultMessage: 'Last month',
  },
  lastWeek: {
    id: 'filters.lastWeek',
    defaultMessage: 'Last week',
  },
  yesterday: {
    id: 'filters.yesterday',
    defaultMessage: 'Yesterday',
  },
  today: {
    id: 'filters.today',
    defaultMessage: 'Today',
  },
  nextWeek: {
    id: 'filters.nextWeek',
    defaultMessage: 'Next week',
  },
  nextMonth: {
    id: 'filters.nextMonth',
    defaultMessage: 'Next month',
  },
  future: {
    id: 'filters.future',
    defaultMessage: 'In the future',
  },
  wot: {
    id: 'filters.wot',
    defaultMessage: 'Without time',
  },
  tomorrow: {
    id: 'filters.tomorrow',
    defaultMessage: 'Tomorrow',
  },
  thisWeek: {
    id: 'filters.thisWeek',
    defaultMessage: 'This week',
  },
  thisMonth: {
    id: 'filters.thisMonth',
    defaultMessage: 'This month',
  },
  previous: {
    id: 'filters.previous',
    defaultMessage: 'Previous events',
  },
  interests: {
    id: 'filters.interests',
    defaultMessage: 'Interests',
  },
});

const form = defineMessages({
  name: {
    id: 'form.name',
    defaultMessage: 'Name',
  },
  visible: {
    id: 'form.visible',
    defaultMessage: 'Visible for',
  },
  categories: {
    id: 'form.categories',
    defaultMessage: 'Categories',
  },
  description: {
    id: 'form.description',
    defaultMessage: 'Description',
  },
  startDate: {
    id: 'form.startDate',
    defaultMessage: 'Start date',
  },
  endDate: {
    id: 'form.endDate',
    defaultMessage: 'End date',
  },
  time: {
    id: 'form.time',
    defaultMessage: 'Time',
  },
  location: {
    id: 'form.location',
    defaultMessage: 'Location',
  },
  url: {
    id: 'form.url',
    defaultMessage: 'URL',
  },
  contact: {
    id: 'form.contact',
    defaultMessage: 'Contact',
  },
  email: {
    id: 'form.email',
    defaultMessage: 'Email',
  },
  tel: {
    id: 'form.tel',
    defaultMessage: 'Tel',
  },
  openingTimes: {
    id: 'form.openingTimes',
    defaultMessage: 'Opening times',
  },
});

export { global, filters, form };
