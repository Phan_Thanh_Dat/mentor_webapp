import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { global as globalLocales } from '../../globalLocales';
import userLocales from './userLocales';
import { Select } from '../../components/Input';
import { ModalBase, ModalBody } from '../../components/Modal';
import Button from '../../components/Button';
import { Spacing, CenterLR } from '../../components/Layout';
import AboutModal from './modals/AboutModal';
import FeedbackFrameModal from './modals/FeedbackFrameModal';

import config from '../../styledConfig';

const Wrap = styled.main`
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: ${config.footerHeight}px;
  padding-top: ${config.headerHeight + 50}px;
`;

const Name = styled.div`
  font-size: 33px;
  text-align: center;
`;

const ButtonGroup = styled.div`
  margin-top: 50px;
  text-align: center;
`;

const dataLanguage = [
  {
    label: 'English',
    value: 'en',
  },
  {
    label: 'Finnish',
    value: 'fi',
  },
];

class UserPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpenFeedBack: false,
      isOpenLogout: false,
      isOpenAbout: false,
    };
  }

  _openLogoutModal = () => {
    this.setState({
      isOpenLogout: true,
    });
  };

  _closeLogoutModal = () => {
    this.setState({
      isOpenLogout: false,
    });
  };

  _openFeedbackModal = () => {
    // this.setState({
    //   isOpenFeedBack: true,
    // });
    window.open('https://chooseyourfuture.fi/feedback', '_self');
  };

  _closeFeedbackModal = () => {
    this.setState({
      isOpenFeedBack: false,
    });
  };

  _openAboutModal = () => {
    this.setState({
      isOpenAbout: true,
    });
  };

  _closeAboutModal = () => {
    this.setState({
      isOpenAbout: false,
    });
  };

  _onChangeLanguage = (e) => {
    this.props.changeLanguage(
      {
        lang: e.target.value,
      },
      {
        addNotification: this.props.addNotification,
        successMessage: this.props.intl.formatMessage(userLocales.languageChangeSuccess),
      });
  };

  _logout = () => {
    window.location = process.env.LOGOUT_URL;
  };


  render() {
    const { user, intl } = this.props;
    return (
      (
        <Wrap>
          <Name>{user.name}</Name>
          <Spacing top="33px" />
          <Select
            onChange={this._onChangeLanguage}
            center
            value={user.lang}
            data={dataLanguage}
            label={intl.formatMessage(globalLocales.language)}
            type="dropdown"
          />
          <Spacing top="18px" />
          <ButtonGroup>
            <Button width="220px" big type="button" onClick={this._openLogoutModal}>
              {intl.formatMessage(globalLocales.logout)}
            </Button>
            <Spacing top="29px" />
            <Button light width="205px" type="button" onClick={this._openAboutModal}>
              {intl.formatMessage(globalLocales.about)}
            </Button>
            <Spacing top="9px" />
            <Button light width="205px" type="button" onClick={this._openFeedbackModal}>
              {intl.formatMessage(globalLocales.feedback)}
            </Button>
          </ButtonGroup>

          {
            this.state.isOpenLogout ?
              (
                <ModalBase
                  isOpen={this.state.isOpenLogout}
                  onRequestClose={this._closeLogoutModal}
                  contentLabel="Logout"
                >
                  <ModalBody>
                    <div>
                      {intl.formatMessage(userLocales.confirmLogout)}
                    </div>
                    <Spacing top="15px" />
                    <CenterLR>
                      <Button onClick={this._logout}>
                        {intl.formatMessage(globalLocales.logout)}
                      </Button>
                      <Spacing left="5px" display="inline-block" />
                      <Button light onClick={this._closeLogoutModal}>
                        {intl.formatMessage(globalLocales.cancel)}
                      </Button>
                    </CenterLR>
                  </ModalBody>
                </ModalBase>
              ) : ''
          }

          {
            this.state.isOpenAbout ?
              (
                <AboutModal
                  textWelcome={intl.formatMessage(userLocales.welcome)}
                  textIntro={intl.formatMessage(userLocales.intro)}
                  textEnjoy={intl.formatMessage(userLocales.enjoy)}
                  isOpen={this.state.isOpenAbout}
                  onRequestClose={this._closeAboutModal}
                  contentLabel={intl.formatMessage(globalLocales.about)}
                />
              ) : ''
          }

          {
            this.state.isOpenFeedBack ? (
              <FeedbackFrameModal
                isOpen={this.state.isOpenFeedBack}
                onRequestClose={this._closeFeedbackModal}
                contentLabel={intl.formatMessage(globalLocales.feedback)}
              />
            ) : ''
          }
        </Wrap>
      )
    );
  }
}

UserPage.propTypes = {
  intl: PropTypes.object,
  addNotification: PropTypes.func,
  user: PropTypes.object,
  changeLanguage: PropTypes.func,
};

const mapStateToProps = ({ user, lang }) => ({
  user,
  lang,
});

const mapDispatchToProps = dispatch => ({
  changeLanguage(data, meta) {
    dispatch(
      {
        type: 'UPDATE_USER',
        payload: data,
        meta,
      },
    );
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(UserPage, { withRef: true }));
