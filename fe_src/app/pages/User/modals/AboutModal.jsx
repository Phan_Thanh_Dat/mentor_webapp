import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ModalBody, ModalBase } from '../../../components/Modal';

const AboutText = styled.div`
  font-size: 20px;
`;

const AboutModal = ({ textWelcome, textIntro, textEnjoy, ...props }) => (

  <ModalBase
    {...props}
  >
    <ModalBody>
      <AboutText>
        <b>{textWelcome}</b>
        <br />
        <br />
        {textIntro}
        <br />
        <br />
        {textEnjoy}
      </AboutText>
    </ModalBody>
  </ModalBase>
);

AboutModal.propTypes = {
  textEnjoy: PropTypes.string,
  textIntro: PropTypes.string,
  textWelcome: PropTypes.string,
  locales: PropTypes.object,
  intl: PropTypes.object,
};

export default AboutModal;
