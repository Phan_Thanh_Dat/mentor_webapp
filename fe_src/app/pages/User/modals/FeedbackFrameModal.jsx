import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ModalBody, ModalBase } from '../../../components/Modal';

const FrameFeddback = styled.iframe`
  width: 100%;
  height: 90vh;
  border: 0;
`;

const AboutModal = ({ textWelcome, textIntro, textEnjoy, ...props }) => (

  <ModalBase
    {...props}
  >
    <ModalBody full>
      <FrameFeddback src="https://chooseyourfuture.fi/feedback/" title="feedback" />
    </ModalBody>
  </ModalBase>
);

AboutModal.propTypes = {
  textEnjoy: PropTypes.string,
  textIntro: PropTypes.string,
  textWelcome: PropTypes.string,
  locales: PropTypes.object,
  intl: PropTypes.object,
};

export default AboutModal;
