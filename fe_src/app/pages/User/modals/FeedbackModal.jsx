import React from 'react';
import { Input, TextArea, Select } from '../../../components/Input';
import Button from '../../../components/Button';
import { Spacing } from '../../../components/Layout';
import { ModalBody, ModalBase, ModalFooter } from '../../../components/Modal';

const ModalFooterFeedBack = ModalFooter.extend`
  text-align: center;
`;

const dataTest = [
  {
    label: 'Type 1',
    value: 't1',
  },
  {
    label: 'Type 2',
    value: 't2',
  },
];

const FeedbackModal = ({ ...props }) => (

  <ModalBase
    {...props}
  >
    <ModalBody>
      <Input label="Your name" type="text" />
      <Spacing top="18px" />
      <Input label="Your email" type="email" />
      <Spacing top="18px" />
      <Select
        data={dataTest}
        label="Feedback type"
        type="dropdown"
      />
      <Spacing top="18px" />
      <TextArea
        rows="5"
        label="Feedback or problem description"
      />
      <Spacing top="18px" />
      <Select
        data={dataTest}
        label="Device"
        type="dropdown"
      />
    </ModalBody>
    <ModalFooterFeedBack>
      <Button>Send</Button>
    </ModalFooterFeedBack>
  </ModalBase>
);

export default FeedbackModal;
