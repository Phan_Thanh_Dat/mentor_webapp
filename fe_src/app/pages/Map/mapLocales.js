import { defineMessages } from 'react-intl';

const mapLoacales = defineMessages({
  modeWhere: {
    id: 'map.modeWhere',
    defaultMessage: 'Where am I',
  },
  modeNearby: {
    id: 'map.modeNearby',
    defaultMessage: 'Search places nearby',
  },
  modeAddress: {
    id: 'map.modeAddress',
    defaultMessage: 'Search by address',
  },
  modeClear: {
    id: 'map.modeClear',
    defaultMessage: 'Clear search results',
  },
  modePOI: {
    id: 'map.modePOI',
    defaultMessage: 'My places',
  },
  radius: {
    id: 'map.radius',
    defaultMessage: 'Radius',
  },
  addToPoi: {
    id: 'map.addToPoi',
    defaultMessage: 'Add to POI',
  },
  showPath: {
    id: 'map.showPath',
    defaultMessage: 'Show path',
  },
  showOnMap: {
    id: 'map.showOnMap',
    defaultMessage: 'Show on map',
  },
  canNotGetLocation: {
    id: 'map.canNotGetLocation',
    defaultMessage: 'Can not get your current position. Code',
  },
  canNotSearch: {
    id: 'map.canNotSearch',
    defaultMessage: 'Can not search. Code',
  },
  ensure: {
    id: 'map.ensure',
    defaultMessage: 'Ensure turn on your device location and allow the app access your location.',
  },
  canNotFindPath: {
    id: 'map.canNotFindPath',
    defaultMessage: 'Can not find path. Code',
  },
});

export default mapLoacales;
