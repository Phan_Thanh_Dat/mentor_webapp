import React from 'react';

const InfoBox = ({ ...props }) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="1809.5 4090.1 111.8 159.9"
  >
    <path id="Path_65" fill="#fff" opacity="0.84" d="M.5,5.776H86.3l26-30.676L96.5,30.413V135H.5Z" transform="translate(1809 4115)" />
  </svg>
);

export default InfoBox;
