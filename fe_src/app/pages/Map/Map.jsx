/* global google */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GoogleMapReact from 'google-map-react';
import trunc from 'trunc-html';
import query from 'query-string';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import { global as globalLocales } from '../../globalLocales';
import mapLocales from './mapLocales';
import { MapMarker, IconCancel, IconMagnifyingGlass, IconCurrentLocation, IconPlaces } from '../../components/Icon';
import config from '../../styledConfig';
import { Select } from '../../components/Input/';
import Button from '../../components/Button/';
import { Spacing } from '../../components/Layout/';
import InfoBox from './components/InfoBox';

const EMPTY_ARRAY = [];

const EMPTY_OBJECT = {};

const Page = styled.div`
  position: relative;
  min-height: 100vh;
  height: 100vh;
  box-sizing: border-box;
  padding-bottom: ${config.footerHeight}px;
  padding-top: ${config.headerHeight}px;
`;

const SelectMode = styled.div`
    width: 265px;
    position: absolute;
    top: 60px;
    z-index: 1;
    margin: 0 auto;
    left: 0;
    right: 0;
    opacity: .8;
    border-radius: 5px;
    display: flex;
`;

const PoiMenu = styled.div`
    display: flex;
    width: 60px;
    background-color: #fff;
    border-right: 1px solid ${config.colorBorder};
    justify-content: center;
    align-items: center;
    font-size: 25px;
`;

const SearchPlaceNearby = styled.div`
  width: 310px;
  position: absolute;
  top: 60px;
  z-index: 1;
  margin: 0 auto;
  left: 0;
  right: 0;
  box-shadow: 0px 1px 6px rgba(0,0,0,0.16);
`;

const SPNHeader = styled.div`
  position: relative;
  padding: 12px 20px;
  text-align: center;
  font-size: 20px;
  box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
  
  &:before {
    position: absolute;
    content: "";
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: #fff;
    z-index: -1;
    opacity: 0.7;
  }
`;

const SPNControl = styled.div`
  text-align: center;
  position: relative;
  padding: 11px 9px;
  
  &::before {
    position: absolute;
    content: "";
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: #fff;
    z-index: -1;
    filter: blur(3px);
    opacity: 0.6;
  }
`;

const SPNSearch = styled.div`
  position: relative;
  font-size: 20px;
  
  input {
    font-size: 18px;
    box-sizing: border-box;
    display: block;
    background-color: #fff;
    padding: 4px 15px 4px 40px;
    width : 100%;
    outline: none;
    border: 1px solid rbg(238, 238, 238); 
  }
  
  span {
    position: absolute;
    top: 50%;
    margin-top: -10px;
    left: 17px;
    z-index: 1;
  }
`;

const SPNRadius = styled.div`
  margin-top: 20px;
  font-size: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  
  label {
    margin-right: 11px;
  }
  
  > div {
    width: 120px;
  }
`;

const SPNCancel = styled.div`
  position: absolute;
  font-size: 16px;
  width: 16px;
  height: 16px;
  top: 50%;
  margin-top: -9px;
  position: absolute;
  font-size: 16px;
`;

const SPNButton = styled(Button)`
  width: 168px;
  font-size: 18px;
`;


const dataRadius = [
  {
    label: '<0.5 km',
    value: '500',
  },
  {
    label: '<1 km',
    value: '1000',
  },
  {
    label: '<3 km',
    value: '3000',
  },
  {
    label: '>3 km',
    value: '50000',
  },

];

const defaultCenter = { lat: 60.451555, lng: 22.2564718 };

const mapOptions = {
  styles: [{
    featureType: 'all',
    elementType: 'labels',
    stylers: [{
      visibility: '#on',
    }],
  }],
};

const MapMarkerContainer = styled.div`
  position: absolute;
  top: -30px;
  left: -30px;
  color: ${({ color }) => color || 'inherit'};
`;

const MapMarkerText = styled.div`
  width: 120px;
  font-weight: bold;
  font-size: ${props => props.zoom > 15 ? '14px' : '10px'};
  color: #C80041;
  margin-top: -9px;
`;

const InfoBoxContainer = styled.div`
  position: absolute;
  top: -45px;
  width: 140px;
  left: 8px;
  z-index: -1;
`;

const InfoBoxWrap = styled.div`
  position: absolute;
  top: 41px;
  width: 60px;
  left: -140px;
  padding-top: 12px;
  padding-left: 17px;
}
`;

const InfoBoxButton = styled(Button)`
  width: 105px;
  padding: 4px 6px;
  font-size: 20px;
`;

class Map extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showInfoBox: false,
      infoBoxSettings: EMPTY_OBJECT,
      mode: null,
      center: defaultCenter,
      current: null,
      zoom: 3,
      nearbyLocations: EMPTY_ARRAY,
      searchLocations: EMPTY_ARRAY,
      radius: '500',
    };
  }

  _toCurrenPosition = (shouldCenter, cb) => {
    this.props.loading(true);
    navigator.geolocation.getCurrentPosition((position) => {
      const pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      this.setState({
        current: pos,
        center: shouldCenter ? pos : this.state.center,
        zoom: shouldCenter ? 12 : this.state.zoom,
      }, () => {
        this.props.loading(false);
        if (cb) cb();
      });
    }, (err) => {
      this.props.loading(false);
      this.props.addNotification({
        message: `${this.props.intl.formatMessage(mapLocales.canNotGetLocation)} ${err.code}`,
        level: 'error',
      });
    });
  };

  _goToPoi = () => {
    this.props.history.push('/map/poi');
  };

  _modeChange = (e) => {
    if (e.target.value === 'poi') {
      this._goToPoi();
    } else if (e.target.value !== 'clear') {
      this.setState({
        mode: e.target.value,
        showInfoBox: false,
      });
      if (e.target.value === 'where') {
        this._toCurrenPosition(true);
      }
    } else {
      this.directionDisplay.set('directions', null);
      this.setState({
        mode: '',
        radius: '500',
        showInfoBox: false,
        nearbyLocations: EMPTY_ARRAY,
        searchLocations: EMPTY_ARRAY,
        single: EMPTY_OBJECT,
      });
    }
  };

  _radiusChanged = (e) => {
    this.setState({
      radius: e.target.value,
    });
  };

  _onChange = ({ center, zoom }) => {
    this.setState({
      center,
      zoom,
    });
  };

  _cancelNearby = () => {
    this.setState({
      mode: '',
    });
  };

  _searchNearbyKeyUp = (e) => {
    if (e.keyCode === 13) {
      if (e.currentTarget.getAttribute('data-mode') === 'address') {
        this._searchAddress();
      } else {
        this._searchNearby();
      }
    }
  };

  _searchNearby = (option) => {
    const pyrmont = new google.maps.LatLng(this.state.current.lat, this.state.current.lng);
    const data = {
      location: pyrmont,
      radius: option.radius ? parseInt(option.radius, 10) : this.state.radius,
      type: option && option.type && !option.query ? option.type : null,
      keyword: option && option.query ?
        option.query : (!option.query ? this.$searchNearby.value : null),
    };

    this.placeService.nearbySearch(data, (a, b) => {
      if (b === 'OK') {
        this.setState({
          single: EMPTY_OBJECT,
          zoom: 12,
          mode: '',
          showInfoBox: false,
          searchLocations: EMPTY_ARRAY,
          nearbyLocations: a,
          center: a.length > 0 ?
          {
            lat: a[0].geometry.location.lat(),  // a[0] is not the closest place
            lng: a[0].geometry.location.lng(),  //  set current location
          } : this.state.center,
        });
      } else {
        this.props.addNotification({
          message: `${this.props.intl.formatMessage(mapLocales.canNotSearch)} "${b}"`,
          level: 'error',
        });
      }
    });
  };

  _searchAddress = () => {
    const pyrmont = new google.maps.LatLng(this.state.current.lat, this.state.current.lng);
    this.placeService.textSearch({
      location: pyrmont,
      query: this.$searchAddress.value,
    }, (a, b) => {
      if (b === 'OK') {
        this.setState({
          single: EMPTY_OBJECT,
          mode: '',
          showInfoBox: false,
          center: a.length > 0 ?
          {
            lat: a[0].geometry.location.lat(),
            lng: a[0].geometry.location.lng(),
          } : this.state.center,
          nearbyLocations: EMPTY_ARRAY,
          searchLocations: a,
        });
      } else {
        this.props.addNotification({
          message: `${this.props.intl.formatMessage(mapLocales.canNotSearch)} "${b}"`,
          level: 'error',
        });
      }
    });
  };

  _showPoi = (poi) => {
    this.setState({
      single: {
        mode: 'poi',
        name: (poi.poiname || poi.title) ? (`POI: ${poi.poiname || poi.title}`) : '',
        lat: parseFloat(poi.lat),
        lng: parseFloat(poi.longt),
      },
      zoom: 15,
      center: {
        lat: parseFloat(poi.lat),
        lng: parseFloat(poi.longt),
      },
    });
  };

  _showEvent = (event) => {
    this.setState({
      single: {
        mode: 'event',
        name: `Event: ${event.title}`,
        lat: parseFloat(event.lat),
        lng: parseFloat(event.longt),
      },
      zoom: 15,
      center: {
        lat: parseFloat(event.lat),
        lng: parseFloat(event.longt),
      },
    });
  };

  _onMapLoaded = ({ map, maps }) => {
    if (!this.$map) {
      this.props.addNotification({
        message: this.props.intl.formatMessage(mapLocales.ensure),
        level: 'info',
      });
    }
    this.$map = map;
    this.placeService = new maps.places.PlacesService(map);
    this.directionService = new google.maps.DirectionsService();
    this.directionDisplay = new maps.DirectionsRenderer({
      map,
    });

    const queryParse = query.parse(this.props.history.location.search);

    if (Object.prototype.hasOwnProperty.call(queryParse, 'showPoi') && this.props.map.currentPoi) {
      this._toCurrenPosition();
      this._showPoi(this.props.map.currentPoi);
    } else if (Object.prototype.hasOwnProperty.call(queryParse, 'showNearby')) {
      this._toCurrenPosition(false, () => {
        this._searchNearby({
          query: queryParse.query, type: queryParse.type, radius: queryParse.radius,
        });
      });
    } else if (Object.prototype.hasOwnProperty.call(queryParse, 'event')) {
      this._toCurrenPosition(false, () => {
        this._showEvent(
          {
            mode: 'event',
            lat: queryParse.lat,
            longt: queryParse.longt,
            title: queryParse.event,
          },
        );
      });
    } else if (Object.prototype.hasOwnProperty.call(queryParse, 'poi')) {
      this._toCurrenPosition(false, () => {
        this._showPoi(
          {
            lat: queryParse.lat,
            longt: queryParse.longt,
            title: queryParse.poi,
          },
        );
      });
    } else {
      this._toCurrenPosition(true);
    }
  };

  _showInfoBox = (e) => {
    const lat = parseFloat(e.currentTarget.getAttribute('data-lat'));
    const lng = parseFloat(e.currentTarget.getAttribute('data-lng'));
    const index = parseFloat(e.currentTarget.getAttribute('data-lng'));
    const address = e.currentTarget.getAttribute('data-address');
    const mode = e.currentTarget.getAttribute('data-mode');

    this.setState({
      center: {
        lat,
        lng,
      },
      showInfoBox: true,
      infoBoxSettings: {
        address,
        index,
        mode,
        lat,
        lng,
      },
    });
  };

  _showPath = (e) => {
    this.setState({
      showInfoBox: false,
    });

    const lat = parseFloat(e.currentTarget.parentElement.getAttribute('data-lat'));
    const lng = parseFloat(e.currentTarget.parentElement.getAttribute('data-lng'));

    this.directionService.route({
      origin: {
        lat: this.state.current.lat,
        lng: this.state.current.lng,
      },
      destination: {
        lat,
        lng,
      },
      travelMode: google.maps.TravelMode.DRIVING,
    }, (response, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.directionDisplay.setDirections(response);
      } else {
        this.props.addNotification({
          message: `${this.props.intl.formatMessage(mapLocales.canNotFindPath)} "${status}"`,
          level: 'error',
        });
      }
    });
  };

  _addToPoi = (e) => {
    const lat = parseFloat(e.currentTarget.parentElement.getAttribute('data-lat'));
    const lng = parseFloat(e.currentTarget.parentElement.getAttribute('data-lng'));
    const address = e.currentTarget.parentElement.getAttribute('data-address');
    const mode = e.currentTarget.parentElement.getAttribute('data-mode');

    if (mode === 'nearby') {
      this.props.history.push(`/map/poi?addPoi=true&lat=${lat}&longt=${lng}&address=${address}`);
    }
  };

  _checkPlace = place => place.name
    && place.types.indexOf('street_address') === -1
    && place.types.indexOf('route') === -1;


  render() {
    const intl = this.props.intl;

    const dataMode = [
      {
        label: intl.formatMessage(mapLocales.modeWhere),
        value: 'where',
      },
      {
        label: intl.formatMessage(mapLocales.modeNearby),
        value: 'nearby',
      },
      {
        label: intl.formatMessage(mapLocales.modeAddress),
        value: 'address',
      },
      {
        label: intl.formatMessage(mapLocales.modePOI),
        value: 'poi',
      },
      {
        label: intl.formatMessage(mapLocales.modeClear),
        value: 'clear',
      },
    ];
    const mode = this.state.mode;
    return (
      <Page>
        <SelectMode hidden={this.state.mode === 'nearby' || this.state.mode === 'address'}>
          <PoiMenu onClick={this._goToPoi}>
            <IconPlaces />
          </PoiMenu>
          <Select
            value={this.state.mode}
            controlled
            onChange={this._modeChange}
            noBorder
            type="dropdown"
            placeholder={intl.formatMessage(globalLocales.mode)}
            data={dataMode}
          />
        </SelectMode>
        {
          mode === 'nearby' ? (
            <SearchPlaceNearby>
              <SPNHeader>
                <SPNCancel onClick={this._cancelNearby}>
                  <IconCancel />
                </SPNCancel>
                {intl.formatMessage(mapLocales.modeNearby)}
              </SPNHeader>
              <SPNControl>
                <SPNSearch>
                  <span>
                    <IconMagnifyingGlass />
                  </span>
                  <input onKeyUp={this._searchNearbyKeyUp} type="search" ref={(e) => { this.$searchNearby = e; }} />
                </SPNSearch>
                <SPNRadius>
                  <label htmlFor="radius">{intl.formatMessage(mapLocales.radius)}</label>
                  <Select
                    onChange={this._radiusChanged}
                    noBorder
                    name="radius"
                    type="dropdown"
                    defaultValue="500"
                    data={dataRadius}
                  />
                </SPNRadius>
                <Spacing top="10px" />
                <SPNButton onClick={this._searchNearby}>
                  {intl.formatMessage(globalLocales.search)}
                </SPNButton>
              </SPNControl>
            </SearchPlaceNearby>
          ) : ''
        }

        <SearchPlaceNearby hidden={this.state.mode !== 'address'}>
          <SPNHeader>
            <SPNCancel onClick={this._cancelNearby}>
              <IconCancel />
            </SPNCancel>
            {intl.formatMessage(mapLocales.modeAddress)}
          </SPNHeader>
          <SPNControl>
            <SPNSearch>
              <span>
                <IconMagnifyingGlass />
              </span>
              <input data-mode="address" onKeyUp={this._searchNearbyKeyUp} type="search" ref={(e) => { this.$searchAddress = e; }} />
            </SPNSearch>
            <Spacing top="10px" />
            <SPNButton onClick={this._searchAddress}>
              {intl.formatMessage(globalLocales.search)}
            </SPNButton>
          </SPNControl>
        </SearchPlaceNearby>

        <GoogleMapReact
          onGoogleApiLoaded={this._onMapLoaded}
          bootstrapURLKeys={{
            key: process.env.GOOGLE_MAP_KEY,
            libraries: 'places',
          }}
          onChange={this._onChange}
          center={this.state.center}
          zoom={this.state.zoom}
          options={mapOptions}
        >
          {
            this.state.nearbyLocations.map((location, index) => (
              <MapMarkerContainer
                key={location.id}
                lat={location.geometry.location.lat()}
                lng={location.geometry.location.lng()}
              >
                <MapMarker
                  data-mode="nearby"
                  data-index={index}
                  data-lat={location.geometry.location.lat()}
                  data-lng={location.geometry.location.lng()}
                  data-address={`${this._checkPlace(location) ? `${location.name}, ` : ''}${location.formatted_address || location.vicinity}`}
                  height="60px"
                  onClick={this._showInfoBox}
                />
                <MapMarkerText zoom={this.state.zoom} >
                  {trunc(location.name, 30).text}
                </MapMarkerText>
              </MapMarkerContainer>
            ))
          }

          {
            this.state.searchLocations.map((location, index) => (
              <MapMarkerContainer
                key={location.id}
                lat={location.geometry.location.lat()}
                lng={location.geometry.location.lng()}
              >
                <MapMarker
                  data-mode="nearby"
                  data-index={index}
                  data-lat={location.geometry.location.lat()}
                  data-lng={location.geometry.location.lng()}
                  data-address={`${this._checkPlace(location) ? `${location.name}, ` : ''}${location.formatted_address}`}
                  onClick={this._showInfoBox}
                  height="60px"
                />
                <MapMarkerText zoom={this.state.zoom} >
                  {trunc(location.name, 30).text}
                </MapMarkerText>
              </MapMarkerContainer>
            ))
          }

          {
            this.state.current ? (
              <MapMarkerContainer
                color="#14AA3C"
                lat={this.state.current.lat}
                lng={this.state.current.lng}
              >
                <IconCurrentLocation height="60px" />
              </MapMarkerContainer>
            ) : null
          }

          {
            this.state.single ? (
              <MapMarkerContainer
                lat={this.state.single.lat}
                lng={this.state.single.lng}
              >
                <MapMarker
                  data-mode={this.state.single.mode}
                  data-lat={this.state.single.lat}
                  data-lng={this.state.single.lng}
                  onClick={this._showInfoBox}
                  height="60px"
                />
                <MapMarkerText zoom={this.state.zoom} >
                  {trunc(this.state.single.name, 30).text}
                </MapMarkerText>
              </MapMarkerContainer>
            ) : null
          }

          <InfoBoxWrap
            hidden={!this.state.showInfoBox}
            lat={this.state.infoBoxSettings.lat}
            lng={this.state.infoBoxSettings.lng}
            data-lat={this.state.infoBoxSettings.lat}
            data-lng={this.state.infoBoxSettings.lng}
            data-index={this.state.infoBoxSettings.index}
            data-address={this.state.infoBoxSettings.address}
            data-mode={this.state.infoBoxSettings.mode}
          >
            <InfoBoxButton
              onClick={this._addToPoi}
              hidden={this.state.single && this.state.single.mode === 'poi'}
            >
              {intl.formatMessage(mapLocales.addToPoi)}
            </InfoBoxButton>
            <Spacing top="5px" />
            <InfoBoxButton onClick={this._showPath}>
              {intl.formatMessage(mapLocales.showPath)}
            </InfoBoxButton>

            <InfoBoxContainer>
              <InfoBox />
            </InfoBoxContainer>
          </InfoBoxWrap>


        </GoogleMapReact>
      </Page>
    );
  }
}

Map.propTypes = {
  intl: intlShape,
  map: PropTypes.object,
  location: PropTypes.object,
  history: PropTypes.object,
  addNotification: PropTypes.func,
  loading: PropTypes.func,
};

const mapStateToProps = ({ map }) => ({
  map,
});

const mapDispatchToProps = dispatch => ({
  loading: (data) => {
    dispatch({ type: 'LOADING_FULL_PAGE', payload: data });
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Map));
