import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import query from 'query-string';
import { fetchApi } from '../../helpers/api';
import Image from '../../components/Image';
import { Title, ExpandContent, ListPois, MapNearby } from './modules';
import config from '../../styledConfig';

function _renderHtml(text) {
  return {
    __html: text,
  };
}

const Page = styled.div`
  min-height: 100vh;
  box-sizing: border-box;
  padding-bottom: ${config.footerHeight}px;
  padding-top: ${config.headerHeight}px;
`;

const Content = styled.div`
  overflow: hidden;
  .ui-content {  
    padding-left: 10px;
    padding-right: 10px;
  }
  
  h1 {
    font-size: 20px;
    background-color: #A50082;
    padding 11px 20px;
    margin: 1px 0 0;
    color: #fff;
    text-align: center;
    text-transform: lowercase;
    font-weight: 400;
  }
  
  h1:first-letter {
    text-transform: uppercase;
  }
  
  img, picture {
    width: 100%;
  } 
`;

const Banner = styled(Image)`
  margin-bottom: 10px;
`;

const TextIntro = styled.div`
  padding: 10px;
`;


class Detail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      content: '',
    };
  }

  componentDidMount = () => {
    this.setState({
      search: this.props.history.location.search,
    });
    this._getInfo(this.props.history.location.search);
  };

  componentWillReceiveProps = (nextProps) => {
    const paths = nextProps.history.location.pathname.split('/');
    if (nextProps.history.location.search !== this.state.search && paths[2] && paths[2] === 'detail') {
      this.setState({
        search: nextProps.history.location.search,
      });
      this._getInfo(nextProps.history.location.search);
    }
  };

  _getInfo = (search) => {
    const queryParse = query.parse(search);
    try {
      fetchApi(
        `${process.env.API_URL}/info?page=${queryParse.page.toUpperCase()}&lang=${this.props.user.lang}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'get',
        }).then((result) => {
          this.setState({
            content: JSON.parse(result.response.content),
          });
        }).catch((e) => {
          this.props.addNotification({
            message: e.message,
            level: 'error',
          });
        });
    } catch (e) {
      this.props.addNotification({
        message: e.message,
        level: 'error',
      });
    }
  };

  renderContent = (content, type) => {
    if (typeof content === 'string') {
      return type === 'html' ?
        (
          <div dangerouslySetInnerHTML={_renderHtml(content)} />
        ) : content;
    }

    return content.map(m => this.renderModule(m));
  };

  renderModule = (module) => {
    switch (module.type) {
      case 'nearby': {
        return (
          <MapNearby
            history={this.props.history}
            title={module.title}
            query={module.query}
            radius={module.radius}
          />
        );
      }
      case 'banner': {
        return (<Banner src={module.src} alt={module.alt} />);
      }
      case 'textIntro': {
        return (
          <TextIntro>
            {this.renderContent(module.content, module.contentType)}
          </TextIntro>
        );
      }
      case 'text': {
        return this.renderContent(module.content, module.contentType);
      }
      case 'expandButton' : {
        return (
          <ExpandContent title={module.title}>
            {this.renderContent(module.content, module.contentType)}
          </ExpandContent>
        );
      }
      case 'pois': {
        return (
          <ListPois
            history={this.props.history}
            query={module.query}
            buttonTitle={module.buttonTitle}
            title={module.title}
          />
        );
      }
      case 'subPage': {
        return (
          <ExpandContent title={module.title} type="subPage">
            {this.renderContent(module.content, module.contentType)}
          </ExpandContent>
        );
      }
      default: {
        return '';
      }
    }
  };

  render() {
    const content = this.state.content;
    return content ? (
      <Page>
        <Content>
          <Title>
            {content.title}
          </Title>
          {
            content.content.map(module => this.renderModule(module))
          }
        </Content>
      </Page>
    ) : null;
  }
}

Detail.propTypes = {
  user: PropTypes.object,
  history: PropTypes.object,
  addNotification: PropTypes.func.isRequired,
};

const mapStateToProps = ({ user }) => ({
  user,
});


export default connect(mapStateToProps, null)(Detail);
