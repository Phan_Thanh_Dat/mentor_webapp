import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchApi } from '../../../helpers/api';
import ButtonRaw from '../../../components/Button';
import { IconRightCher } from '../../../components/Icon';
import { ModalBase } from '../../../components/Modal';

const Button = styled(ButtonRaw)`
  margin-top: 10px;
  display: block;
  width: 100%;
`;

const Wrap = styled.div`
  position: relative;
  padding: 8px 0;
`;

const Content = styled.div`
  padding: 0 50px 0 13px;
`;

const Icon = styled.div`
  position: absolute;
  top: 0;
  right: 30px;
  font-size: 17px;
  bottom: 0;
  display: flex;
  align-items: center;
`;

const Title = styled.div`
  font-size: 26px;
`;

const Address = styled.div`
  font-size: 18px;
`;

const HR = styled.div`
    height: 1px;
    width: 100%;
    background-color: #DDDEDE;
    max-width: 300px;
    margin: 10px auto 0 auto;
    display: block;
`;

class ListPois extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      pois: [],
    };
  }

  _open = () => {
    this.setState({
      isOpen: true,
    });

    fetchApi(
      `${process.env.API_URL}/pois?fcategs=${this.props.query}`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'get',
      }).then((result) => {
        this.setState({
          pois: result.response,
        });
      });
  };

  _close = () => {
    this.setState({
      isOpen: false,
      pois: [],
    });
  };

  _openMap = (e) => {
    this.props.showPoi(this.state.pois[e.currentTarget.getAttribute('data-index')]);
    this.props.history.push('/map?showPoi');
  };

  render() {
    return (
      <div>
        <Button onClick={this._open}>
          {this.props.buttonTitle}
        </Button>
        {
          this.state.isOpen ? (
            <ModalBase
              onRequestClose={this._close}
              isOpen={this.state.isOpen}
              contentLabel={this.props.title}
            >
              {
                this.state.pois.map((poi, index) => (
                  <Wrap key={poi.id} data-index={index} onClick={this._openMap}>
                    <Icon><IconRightCher /> </Icon>
                    <Content>
                      <Title>{poi.poiname}</Title>
                      <Address>Address: {poi.locat}</Address>
                    </Content>
                    <HR />
                  </Wrap>
                ))
              }
            </ModalBase>
          ) : null
        }
      </div>
    );
  }
}

ListPois.propTypes = {
  showPoi: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  buttonTitle: PropTypes.string,
  title: PropTypes.string,
  query: PropTypes.string.isRequired,
};


const mapDispatchToProps = dispatch => ({
  showPoi: (data) => {
    dispatch({ type: 'MAP_SHOW_POI', payload: data });
  },
});

export default connect(null, mapDispatchToProps)(ListPois);
