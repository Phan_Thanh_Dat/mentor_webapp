import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ButtonRaw from '../../../components/Button';

const Button = styled(ButtonRaw)`
  margin-top: 10px;
  display: block;
  width: 100%;
`;


class MapNearby extends React.PureComponent {

  _click = () => {
    this.props.history.push(`/map?showNearby&query=${this.props.query}&type=${this.props.locationType ? this.props.locationType : ''}&radius=${parseInt(this.props.radius, 10) || 5000}`);
  };

  render() {
    return (
      <Button onClick={this._click}>{this.props.title}</Button>
    );
  }
}

MapNearby.propTypes = {
  radius: PropTypes.string,
  locationType: PropTypes.string,
  query: PropTypes.string,
  history: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
};


export default MapNearby;
