import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';
import { defaultRules } from 'react-hoc-form-validatable';
import { ModalBase } from '../../../components/Modal';
import AddPoiForm from '../forms/AddPoiForm';

const AddPoiModal = ({
     chooseMap,
     contentLabel,
     onRequestClose,
     isOpen,
     submitHandler,
     poi,
     addToPoi,
     clearAddToPoi,
   }) => (
     <ModalBase
       isOpen={isOpen}
       onRequestClose={onRequestClose}
       contentLabel={contentLabel}
     >
       <AddPoiForm
         addToPoi={addToPoi}
         poi={poi}
         chooseMap={chooseMap}
         onRequestClose={onRequestClose}
         submitCallback={submitHandler}
         validateLang="en"
         clearAddToPoi={clearAddToPoi}
         rules={defaultRules}
       />
     </ModalBase>
);

AddPoiModal.propTypes = {
  poi: PropTypes.object,
  addToPoi: PropTypes.object,
  submitHandler: PropTypes.func,
  clearAddToPoi: PropTypes.func,
  chooseMap: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  contentLabel: PropTypes.string,
};

export default pure(AddPoiModal);
