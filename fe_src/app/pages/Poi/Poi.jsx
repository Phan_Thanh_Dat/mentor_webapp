import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import query from 'query-string';
import { injectIntl, intlShape } from 'react-intl';
import { sagaMiddlewareControl } from '../../store';
import PoiSaga from '../../modules/Poi/PoiSaga';
import { IconPlus } from '../../components/Icon';
import { Select } from '../../components/Input';
import styledConfig from '../../styledConfig';
import AddPoiModal from './modals/AddPoiModal';
import PoiDisplay from './PoiDisplay';
import DeleteModalConFirm from '../Events/modals/DeleteModalConfirm';
import mapLocales from '../Map/mapLocales';
import poiLocales from './poiLocales';
import { global as globalLocales, filters as filterlLocales } from '../../globalLocales';

const Wrap = styled.div`
  padding-top: ${styledConfig.headerHeight}px;
  padding-bottom: ${styledConfig.footerHeight}px;
`;

const Title = styled.div`
  border-top: 1px solid #fff;
  padding: 10px 20px;
  font-size: 20px;
  color: #fff;
  background-color: #C80041;
  text-align: center;
`;

const Fixed = styled.div`
  position: fixed;
  width: 100%;
  top: ${styledConfig.headerHeight}px;
  left: 0;
  z-index: ${styledConfig.zIndexMax - 50};
`;

const Filters = styled.div`
  display: flex;
  align-items: center;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
  background-color: #fff;
  z-index: ${styledConfig.zIndexMax - 100};
`;

const FilterSelect = styled.div`
  flex: 1;
`;

const AddNew = styled.div`
  flex: 1;
  text-align: center;
  font-size: 19px;
`;

const AddNewIcon = styled.span`
  font-size: 15px;
  margin-left: 34px;
`;

const Page = styled.div`
  padding-top: 88px;
  padding-bottom: ${styledConfig.footerHeight}px;
`;

class Poi extends React.PureComponent {

  constructor(props) {
    super(props);
    const queryParse = query.parse(props.history.location.search);
    this.state = {
      isOpenPoi: {},
      openAdd: !!queryParse.addPoi,
      addToPoint: queryParse.addPoi ? {
        mapAddress: queryParse.address,
        addressLat: queryParse.lat,
        addressLng: queryParse.longt,
      } : '',
      mapAddress: '',
      addressLat: '',
      addressLng: '',
      isOpenDelete: false,
      deletePoi: null,
      editPoi: null,
    };
  }

  componentWillMount = () => {
    sagaMiddlewareControl.run('poiSagas', PoiSaga);
  };

  componentDidMount = () => {
    this.props.getPoi({}, {
      addNotification: this.props.addNotification,
    });
  };

  _openAdd = () => {
    this.setState({
      openAdd: true,
    });
  };

  _closeAdd = () => {
    this.setState({
      openAdd: false,
      editPoi: null,
      addToPoint: '',
    });
  };

  _openEdit = (index) => {
    this.setState({
      openAdd: true,
      editPoi: index,
    });
  };

  _chooseMap = (e) => {
    this.setState(e);
  };

  _closeDelete = () => {
    this.setState({
      isOpenDelete: false,
    });
  };

  _openDelete = (index) => {
    this.setState({
      isOpenDelete: true,
      deletePoi: index,
    });
  };

  _handlerDeletePoi = () => {
    this.props.deletePoi(
      {
        id: this.props.pois[this.state.deletePoi].id,
        index: this.state.deletePoi,
      },
      {
        addNotification: this.props.addNotification,
        successMessage: this.props.intl.formatMessage(poiLocales.deleteSuccess),
        callback: (status) => {
          if (status) {
            this.setState({
              isOpenDelete: false,
            });
          }
        },
      });
  };


  _handlerSubmitAddEvent = (inputs, done) => {
    if (!this.state.addToPoint && inputs.location && !inputs.location.value) {
      this.props.addNotification({
        message: this.props.intl.formatMessage(poiLocales.mustHaveLocation),
        level: 'error',
      });
      return done();
    }

    const callback = (status) => {
      done(status);
      if (status) {
        this._closeAdd();
      }
    };

    const currentEditPoi = this.state.editPoi === 0 || this.state.editPoi ?
      this.props.pois[this.state.editPoi] : null;

    const addToPoint = this.state.addToPoint ? this.state.addToPoint : {};

    const data = {
      poiname: inputs.name.value,
      categs: '{"Personal"}',
      descr: inputs.description.value,
      aday: inputs.openingTimes.value,
      url: inputs.url.value,
      contact: inputs.contact.value,
      tel: inputs.tel.value,
      locat: addToPoint.mapAddress || this.state.mapAddress || inputs.location.value,
      lat: addToPoint.addressLat || this.state.addressLat || (currentEditPoi && currentEditPoi.lat ? currentEditPoi.lat : ''),
      longt: addToPoint.addressLng || this.state.addressLng || (currentEditPoi && currentEditPoi.longt ? currentEditPoi.longt : ''),
    };

    if (this.state.editPoi === 0 || this.state.editPoi) {
      data.pid = currentEditPoi.id;
      this.props.editPoi(data, {
        index: this.state.editPoi,
        callback,
        successMessage: this.props.intl.formatMessage(poiLocales.editSuccess),
        addNotification: this.props.addNotification,
      });
    } else {
      this.props.createPoi(data, {
        callback,
        successMessage: this.props.intl.formatMessage(poiLocales.createSuccess),
        addNotification: this.props.addNotification,
      });
    }
  };

  _togglePoi = (index) => {
    this.setState({
      isOpenPoi: update(
        this.state.isOpenPoi,
        {
          [index]: {
            $set: !this.state.isOpenPoi[index],
          },
        },
      ),
    });
  };

  _filterChange = (e) => {
    if (e.target.value === 'all') {
      this.props.getPoi({}, {
        addNotification: this.props.addNotification,
      });
    } else {
      this.props.loading(true);
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          radius: e.target.value,
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        this.props.getPoi(pos, {
          addNotification: this.props.addNotification,
        });
      }, (err) => {
        this.props.loading(false);
        this.props.addNotification({
          message: `${this.props.intl.formatMessage(mapLocales.canNotGetLocation)} ${err.code}`,
          level: 'error',
        });
      });
    }
  };

  _clearAddToPoi = () => {
    this.setState({
      addToPoint: '',
    });
  };

  render() {
    const dataFilterRadius = [
      {
        label: this.props.intl.formatMessage(filterlLocales.all),
        value: 'all',
      },
      {
        label: '<0.5km',
        value: '<0.5km',
      },
      {
        label: '<1km',
        value: '<1km',
      },
      {
        label: '<3km',
        value: '<3km',
      },
    ];
    return (
      <Wrap>
        <Fixed>
          <Title>{this.props.intl.formatMessage(poiLocales.title)}</Title>
          <Filters>
            <FilterSelect>
              <Select
                onChange={this._filterChange}
                noBorder
                center
                data={dataFilterRadius}
                type="dropdown"
                placeholder="All"
              />
            </FilterSelect>
            <AddNew onClick={this._openAdd}>
              {this.props.intl.formatMessage(globalLocales.addNew)}
              <AddNewIcon><IconPlus /></AddNewIcon>
            </AddNew>
          </Filters>
        </Fixed>
        <Page>
          {
            this.props.pois.map((poi, index) => (
              <PoiDisplay
                textOpen=""
                textShowOnMap={this.props.intl.formatMessage(mapLocales.showOnMap)}
                textEdit={this.props.intl.formatMessage(globalLocales.edit)}
                textDelete={this.props.intl.formatMessage(globalLocales.delete)}
                key={poi.id}
                id={poi.id}
                isOpened={this.state.isOpenPoi[poi.id]}
                isOwned={this.props.user.email === poi.author}
                onClickIntro={this._togglePoi}
                onClickDelete={this._openDelete}
                onClickEdit={this._openEdit}
                index={index}
                title={poi.poiname}
                description={poi.descr}
                address={poi.locat}
                url={poi.url}
                lat={poi.lat}
                lng={poi.longt}
                openTimes={poi.aday}
                contact={poi.contact}
                tel={poi.tel}
              />
            ))
          }
        </Page>
        {
          this.state.openAdd ? (
            <AddPoiModal
              poi={this.props.pois[this.state.editPoi]}
              addToPoi={this.state.addToPoint}
              chooseMap={this._chooseMap}
              submitHandler={this._handlerSubmitAddEvent}
              contentLabel={
                this.state.editPoi || this.state.editPoi === 0 ?
                  this.props.intl.formatMessage(poiLocales.editPoi) :
                  this.props.intl.formatMessage(poiLocales.addPoi)
              }
              onRequestClose={this._closeAdd}
              clearAddToPoi={this._clearAddToPoi}
              isOpen={this.state.openAdd}
            />
          ) : null
        }
        {
          this.state.isOpenDelete ? (
            <DeleteModalConFirm
              confirmText={this.props.intl.formatMessage(poiLocales.confirmDelete)}
              cancelText={this.props.intl.formatMessage(globalLocales.cancel)}
              deleteText={this.props.intl.formatMessage(globalLocales.delete)}
              onClickConfirm={this._handlerDeletePoi}
              onClickCancel={this._closeDelete}
              isOpen={this.state.isOpenDelete}
            />
          ) : null
        }
      </Wrap>
    );
  }
}

Poi.propTypes = {
  user: PropTypes.object,
  history: PropTypes.object,
  loading: PropTypes.func,
  pois: PropTypes.array,
  intl: intlShape.isRequired,
  getPoi: PropTypes.func.isRequired,
  editPoi: PropTypes.func.isRequired,
  createPoi: PropTypes.func.isRequired,
  deletePoi: PropTypes.func.isRequired,
  addNotification: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  getPoi(payload, meta) {
    dispatch(
      {
        type: 'GET_POIS',
        payload,
        meta,
      },
    );
  },

  createPoi(data, meta) {
    dispatch(
      {
        type: 'CREATE_POI',
        payload: data,
        meta,
      },
    );
  },

  editPoi(data, meta) {
    dispatch(
      {
        type: 'UPDATE_POI',
        payload: data,
        meta,
      },
    );
  },

  deletePoi(data, meta) {
    dispatch(
      {
        type: 'DELETE_POI',
        payload: data,
        meta,
      },
    );
  },

  loading(payload) {
    dispatch({
      type: 'LOADING_FULL_PAGE',
      payload,
    });
  },
});

const mapStateToProps = ({ user, pois }) => ({
  pois,
  user,
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Poi));
