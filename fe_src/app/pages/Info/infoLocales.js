import { defineMessages } from 'react-intl';

const messages = defineMessages({
  everyDayLife: {
    id: 'info.everyDayLife',
    defaultMessage: 'Everyday life',
  },
  hobbies: {
    id: 'info.hobbies',
    defaultMessage: 'Hobbies',
  },
  foodDrink: {
    id: 'info.foodDrink',
    defaultMessage: 'Food & drinks',
  },
  transport: {
    id: 'info.transport',
    defaultMessage: 'Transport',
  },
  shopping: {
    id: 'info.shopping',
    defaultMessage: 'Shopping',
  },
  groceries: {
    id: 'info.groceries',
    defaultMessage: 'Groceries',
  },
  housing: {
    id: 'info.housing',
    defaultMessage: 'Housing',
  },
  services: {
    id: 'info.services',
    defaultMessage: 'Services',
  },
  heath: {
    id: 'info.heath',
    defaultMessage: 'Heath',
  },
  fun: {
    id: 'info.fun',
    defaultMessage: 'Fun',
  },
  safety: {
    id: 'info.safety',
    defaultMessage: 'Safety',
  },
  religion: {
    id: 'info.religion',
    defaultMessage: 'Religion',
  },
  sports: {
    id: 'info.sports',
    defaultMessage: 'Sports',
  },
  music: {
    id: 'info.music',
    defaultMessage: 'Music',
  },
  artCraft: {
    id: 'info.artCraft',
    defaultMessage: 'Art and Crafts',
  },
  outdoors: {
    id: 'info.outdoors',
    defaultMessage: 'Outdoors',
  },
  studentLunch: {
    id: 'info.studentLunch',
    defaultMessage: 'Student Lunch',
  },
  restaurants: {
    id: 'info.restaurants',
    defaultMessage: 'Restaurants',
  },
  cafes: {
    id: 'info.cafes',
    defaultMessage: 'Cafes',
  },
  nightlife: {
    id: 'info.nightlife',
    defaultMessage: 'Nightlife',
  },
});

export default messages;
