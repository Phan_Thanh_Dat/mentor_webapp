import React from 'react';
import PropTypes from 'prop-types';
import { HOCForm } from 'react-hoc-form-validatable';
import { injectIntl, intlShape } from 'react-intl';
import styled from 'styled-components';
import moment from 'moment';
import { global as globalLocales, form as formLocales } from '../../../globalLocales';
import eventLocales from '../eventLocales';
import { InputValidatable, TextAreaValidatable, SelectValidatable, DatePickerValidatable, Input } from '../../../components/Input';
import Button from '../../../components/Button';
import { Spacing } from '../../../components/Layout/';
import { formatObjectStringValueSelect } from '../../../helpers/formatObjectString';
import MapModal from '../modals/MapModal';

const RowGroupSelect = styled.div`
  display: flex;
`;

const SelectLeft = styled(SelectValidatable)`
  input {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
`;

const SelectRight = styled(SelectValidatable)`
  input {
    border-left: 0;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
`;

const FormAction = styled.div`
  display: flex;
  
  ${Button} {
    width: 100%;
    border-radius: 0;
  }
  
  ${Button}:first-child {
     border-bottom-left-radius: 5px;
  }
  
  ${Button}:last-child {
     border-bottom-right-radius: 5px;
  }
`;

const Form = styled.div`
  padding: 15px;
  
  .DateInput__display-text {
    min-height: 20px;
    width: 110px;
  }
`;

const TimeRow = styled.div`
  padding-left: 10px;
  display: flex;
  align-items: center;
  
  input {
    width: 110px;
    height: 28px;
    font-size: 14px;
  }
`;

const TimeLabel = styled.div`
  margin-right: 5px;
  font-size: 20px;
`;

const TimeInputs = styled.div`
  display: flex;
  align-items: center;
`;

const TimeSer = styled.div`
  margin: 0 7px;
`;

class AddEventForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpenSelectAddress: false,
      mapAddress: '',
      addressLat: null,
      addressLng: null,
    };
  }

  _openSelectAddress = () => {
    this.setState({
      isOpenSelectAddress: true,
    });
  };

  _closeSelectAddress = () => {
    this.setState({
      isOpenSelectAddress: false,
    });
  };

  _selectAddress = (e) => {
    this.setState({
      isOpenSelectAddress: false,
      mapAddress: e.address,
      addressLat: e.lat,
      addressLng: e.lng,
    });
    this.props.chooseMap({
      mapAddress: e.address,
      addressLat: e.lat,
      addressLng: e.lng,
    });
  };

  _clearLocation = () => {
    this.setState({
      isOpenSelectAddress: false,
      mapAddress: null,
      addressLat: null,
      addressLng: null,
    });
  };

  _submit = (inputs, done) => {
    this.props.onSubmit(inputs, done, {
      address: this.state.mapAddress,
      lat: this.state.addressLat,
      lng: this.state.addressLng,
    });
  };

  _renderEditLocation = event => (
    <div>
      <Input
        type="text"
        label={this.props.intl.formatMessage(formLocales.location)}
        value={event.locat}
        disabled
      />
      <Button type="button" onClick={this._openSelectAddress}>Choose on map</Button>
    </div>
  );

  render() {
    const { intl, event } = this.props;
    return (

      <div>
        <form
          noValidate
          onSubmit={this._submit}
        >
          <Form>
            <InputValidatable
              defaultValue={event && event.descr ? event.descr : undefined}
              name="name"
              label={intl.formatMessage(formLocales.name)}
              rule="notEmpty"
              type="text"
            />
            <Spacing top="10px" />
            <RowGroupSelect>
              <SelectLeft
                defaultValue={
                event && event.sgroups ? formatObjectStringValueSelect(event.sgroups) : undefined
              }
                multiple
                name="visible"
                rule="notEmpty"
                data={this.props.memberGroupsData}
                label={intl.formatMessage(formLocales.visible)}
                type="dropdown"
              />
              <SelectRight
                defaultValue={
                event && event.formatCategs ?
                  formatObjectStringValueSelect(event.formatCategs) : undefined
              }
                multiple
                name="category"
                data={this.props.subscriptions}
                label={intl.formatMessage(formLocales.categories)}
                rule="notEmpty"
                type="dropdown"
              />
            </RowGroupSelect>
            <Spacing top="10px" />
            <TextAreaValidatable
              defaultValue={
              event && event.ldescr ? formatObjectStringValueSelect(event.ldescr) : undefined
            }
              name="description"
              rows="2"
              label={intl.formatMessage(formLocales.description)}
            />
            <Spacing top="10px" />
            <DatePickerValidatable
              defaultValue={
              event && event.sdate ? moment(event.sdate) : undefined
            }
              displayFormat="DD/MM/YYYY"
              type="date"
              rule="notEmpty"
              name="date"
              label={intl.formatMessage(formLocales.startDate)}
            />
            <DatePickerValidatable
              defaultValue={
              event && event.edate ? moment(event.edate) : undefined
            }
              displayFormat="DD/MM/YYYY"
              type="date"
              rule="notEmpty"
              name="edate"
              label={intl.formatMessage(formLocales.endDate)}
            />
            <TimeRow>
              <TimeLabel>{intl.formatMessage(formLocales.time)}</TimeLabel>
              <TimeInputs>
                <InputValidatable
                  defaultValue={
                  event && event.stime ? event.stime : undefined
                }
                  rule="notEmpty"
                  name="stime"
                  type="time"
                />
                <TimeSer>-</TimeSer>
                <InputValidatable
                  defaultValue={
                  event && event.etime ? event.etime : undefined
                }
                  rule="notEmpty"
                  name="etime"
                  type="time"
                />
              </TimeInputs>
            </TimeRow>
            <Spacing top="10px" />
            <div>

              {
                this.state.mapAddress ?
                  (
                    <Input
                      type="text"
                      label={intl.formatMessage(formLocales.location)}
                      value={this.state.mapAddress}
                      disabled
                    />
                  ) :
                  (
                    <InputValidatable
                      defaultValue={
                      event && event.locat ? event.locat : undefined
                    }
                      label={intl.formatMessage(formLocales.location)}
                      name="location"
                      type="text"
                    />
                  )
              }
              {
                this.state.mapAddress ?
                  (<Button type="button" onClick={this._clearLocation}>
                    {intl.formatMessage(eventLocales.clearLocation)}
                  </Button>) :
                  (<Button type="button" onClick={this._openSelectAddress}>
                    {intl.formatMessage(eventLocales.chooseOnMap)}
                  </Button>)
              }
              {event && event.lat ? (
                <Spacing top="5px">{intl.formatMessage(eventLocales.warnEditLocation)}</Spacing>
              ) : ''}
            </div>
            <Spacing top="10px" />
            <InputValidatable
              defaultValue={
              event && event.url ? event.url : undefined
            }
              label={intl.formatMessage(formLocales.url)}
              name="url"
              type="text"
            />
            <Spacing top="10px" />
            <InputValidatable
              defaultValue={
              event && event.contact ? event.contact : undefined
            }
              label={intl.formatMessage(formLocales.contact)}
              name="contact"
              type="text"
            />
            <Spacing top="10px" />
            <InputValidatable
              defaultValue={
              event && event.email ? event.email : undefined
            }
              label={intl.formatMessage(formLocales.email)}
              name="email"
              type="text"
            />
            <Spacing top="10px" />
            <InputValidatable
              defaultValue={
              event && event.tel ? event.tel : undefined
            }
              label={intl.formatMessage(formLocales.tel)}
              name="tel"
              type="text"
            />
          </Form>

          <FormAction>
            <Button
              display="block"
              disabled={this.props.submitted}
              light
              type="button"
              onClick={this.props.onRequestClose}
            >
              {this.props.intl.formatMessage(globalLocales.cancel)}
            </Button>
            <Button display="block" disabled={this.props.submitted} type="submit">
              {this.props.intl.formatMessage(globalLocales.save)}
            </Button>
          </FormAction>
        </form>
        {
          this.state.isOpenSelectAddress ? (
            <MapModal
              onChoose={this._selectAddress}
              isOpen={this.state.isOpenSelectAddress}
              onRequestClose={this._closeSelectAddress}
            />
          ) : null
        }
      </div>
    );
  }
}


AddEventForm.propTypes = {
  intl: intlShape.isRequired,
  subscriptions: PropTypes.array,
  memberGroupsData: PropTypes.array,
  hasError: PropTypes.bool,
  onSubmit: PropTypes.func,
  chooseMap: PropTypes.func,
  onRequestClose: PropTypes.func,
  submitted: PropTypes.bool,
  children: PropTypes.node,
};

export default HOCForm(injectIntl(AddEventForm));
