import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import isEqual from 'lodash.isequal';
import { injectIntl } from 'react-intl';
import { filters as filterLocales, global as globalLocales } from '../../globalLocales';
import eventLocales from './eventLocales';
import config from '../../styledConfig';
import { IconPlus } from '../../components/Icon';
import { Select } from '../../components/Input';
import AddEventModal from './modals/AddEventModal';
import Event from './components/Event';
import { sagaMiddlewareControl } from '../../store';
import eventSagas from '../../modules/Events/EventsSaga';
import * as subscriptionsSaga from '../../modules/Subscriptions/SubscriptionsSaga';
import { formatObjectStringToArray, formatObjectStringToArraySelect } from '../../helpers/formatObjectString';
import DeleteModalConFirm from './modals/DeleteModalConfirm';

const Wrap = styled.div`
  min-height: 100vh;
  box-sizing: border-box;
  padding-bottom: ${config.footerHeight}px;
  padding-top: ${config.headerHeight + 31}px;
`;

const Filters = styled.div`
  display: flex;
  align-items: center;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
  position: fixed;
  width: 100%;
  top: 50px;
  left: 0;
  background-color: #fff;
  z-index: ${config.zIndexMax - 100};
`;

const FilterSelect = styled.div`
  flex: 1;
`;

const AddInterestWrap = styled.div`
  height: 31px;
  font-size: 15px;
  width: 44px;
  display: flex;
  justify-items: center;
  align-items: center;
  background-color: ${({ open }) => open ? '#656565' : '#fff'};
  color: ${({ open }) => open ? '#fff' : config.colorText};
  
  > svg {
    display: block;
    margin: 0 auto;
  }
`;

const AddInterest = (({ ...props }) => (
  <AddInterestWrap {...props}>
    <IconPlus />
  </AddInterestWrap>
));

const EMPTY_OBJECT = {};

class Events extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isOpenDelete: false,
      editingEvent: null,
      deleteEvent: null,
      isOpen: false,
      isOpenEvent: EMPTY_OBJECT,
      isOpenAdd: false,
      filterDate: '',
      filterInterest: '',
      mapAddress: '',
      mapAddressLat: null,
      mapAddressLng: null,
    };
  }

  componentWillMount = () => {
    sagaMiddlewareControl.run('eventSagas', eventSagas);
    sagaMiddlewareControl.run(subscriptionsSaga.name, subscriptionsSaga.saga);
  };

  componentDidMount = () => {
    this.props.getEvents();
    this.props.getSubscriptions();
  };

  componentWillReceiveProps = (nextProps) => {
    if (!isEqual(nextProps.events, this.props.events)) {
      this.setState({ isOpenEvent: {} });
    }
  };

  _toggleEvent = (index) => {
    this.setState({
      isOpenEvent: update(
        this.state.isOpenEvent,
        {
          [index]: {
            $set: !this.state.isOpenEvent[index],
          },
        },
      ),
    });
  };

  _openAddEvent = (index) => {
    this.setState({
      isOpenAdd: true,
      editingEvent: typeof index === 'object' ? null : index,
      mapAddress: '',
      mapAddressLat: null,
      mapAddressLng: null,
    });
  };

  _closeAddEvent = () => {
    this.setState({
      editingEvent: null,
      isOpenAdd: false,
    });
  };

  _closeDelete = () => {
    this.setState({
      isOpenDelete: false,
      deleteEvent: null,
    });
  };

  _openDelete = (index) => {
    this.setState({
      isOpenDelete: true,
      deleteEvent: index,
    });
  };

  _handlerDeleteEvent = () => {
    this.props.deleteEvent({
      id: this.props.events[this.state.deleteEvent].id,
      index: this.state.deleteEvent,
    }, {
      callback: this._closeDelete,
      successMessage: this.props.intl.formatMessage(eventLocales.deleteSuccess),
      addNotification: this.props.addNotification,
    });
  };

  _handlerSubmitAddEvent = (inputs, done) => {
    const callback = (status) => {
      done(status);
      if (status) {
        this._closeAddEvent();
      }
    };
    const sdate = inputs.date.value.format('YYYY-MM-DD');
    const edate = inputs.edate.value ? inputs.edate.value.format('YYYY-MM-DD') : false;

    const currentEditEvent = this.state.editingEvent === 0 || this.state.editingEvent ?
      this.props.events[this.state.editingEvent] : null;

    const newCatData = [];

    inputs.category.value.split(',').forEach((cat) => {
      this.props.subscriptions.forEach((sub) => {
        const content = JSON.parse(sub.content);
        if (content[this.props.user.lang] === cat) {
          newCatData.push(sub.id);
        }
      });
    });

    const data = {
      sgroups: `{${inputs.visible.value}}`,
      descr: inputs.name.value,
      categs: `{${newCatData.join(',')}}`,
      contact: inputs.contact.value,
      tel: inputs.tel.value,
      email: inputs.email.value,
      ldescr: inputs.description.value,
      url: inputs.url.value,
      locat: this.state.mapAddress || inputs.location.value,
      lat: this.state.mapAddressLat || (currentEditEvent && currentEditEvent.lat ? currentEditEvent.lat : ''),
      longt: this.state.mapAddressLng || (currentEditEvent && currentEditEvent.longt ? currentEditEvent.longt : ''),
      stimes: `${sdate} ${inputs.stime.value}`,
      etimes: `${edate || sdate} ${inputs.etime.value}`,
      sdate,
      eid: -1,
    };

    if (this.state.editingEvent === 0 || this.state.editingEvent) {
      data.eid = currentEditEvent.id;
      if (data.lat && data.longt &&
        data.longt === currentEditEvent.longt &&
        data.lat === currentEditEvent.lat &&
        data.locat !== currentEditEvent.locat) {
        data.lat = '';
        data.longt = '';
      }
      this.props.updateEvent(data, {
        callback,
        index: this.state.editingEvent,
        successMessage: this.props.intl.formatMessage(eventLocales.updateSuccess),
        addNotification: this.props.addNotification,
      });
    } else {
      this.props.createEvent(data, {
        callback,
        successMessage: this.props.intl.formatMessage(eventLocales.addSuccess),
        addNotification: this.props.addNotification,
      });
    }
  };

  _changeFilters = (e) => {
    this.setState({
      filterDate: e.target.value,
    }, () => {
      this.props.getEvents({
        filterDate: this.state.filterDate,
        filterInterest: this.state.filterInterest,
      });
    });
  };

  _changeInterest = (e) => {
    const filter = [];
    e.target.value.split(',').forEach((i) => {
      this.props.subscriptions.forEach((da) => {
        const content = JSON.parse(da.content)[this.props.user.lang];
        if (i === content) {
          filter.push(da.id);
        }
      });
    });
    this.setState({
      filterInterest: filter.join(','),
    }, () => {
      this.props.getEvents({
        filterDate: this.state.filterDate,
        filterInterest: this.state.filterInterest,
      });
    });
  };

  _onClickEventEdit = (index) => {
    this._openAddEvent(index);
  };

  _chooseMap = (e) => {
    this.setState({
      mapAddress: e.mapAddress,
      mapAddressLat: e.addressLat,
      mapAddressLng: e.addressLng,
    });
  };

  _handlerFavorite = (e) => {
    this.props.favEvent({
      id: this.props.events[e].id,
    });
  };

  _formatInterestsData = data => data.reduce((final, current) => {
    const content = JSON.parse(current.content);
    const newFinal = final.slice(0);
    newFinal.push({
      label: content[this.props.user.lang],
      value: content[this.props.user.lang],
    });
    return newFinal;
  }, []);

  _getDataEditEvent = (event, subscriptions) => {
    if (event.categs) {
      const cats = event.categs.replace(new RegExp(/\"|\{|\}/, 'g'), '').split(',');
      event.formatCategs = cats.reduce((format, cat) => {
        subscriptions.forEach((da) => {
          if (da.id === cat) {
            return format.push(JSON.parse(da.content)[this.props.user.lang]);
          }
          return format;
        });
        return format;
      }, []).join(',');
    }

    return event;
  };

  render() {
    const intl = this.props.intl;
    const eventMessages = {
      showOnMap: intl.formatMessage(eventLocales.showOnMap),
      date: intl.formatMessage(globalLocales.date),
      time: intl.formatMessage(globalLocales.time),
      contact: intl.formatMessage(globalLocales.contact),
    };
    const dataFilter = [
      {
        label: intl.formatMessage(filterLocales.all),
        value: 'all',
      },
      {
        label: intl.formatMessage(filterLocales.today),
        value: 'tday',
      },
      {
        label: intl.formatMessage(filterLocales.tomorrow),
        value: 'tomorrow',
      },
      {
        label: intl.formatMessage(filterLocales.thisWeek),
        value: 'tweek',
      },
      {
        label: intl.formatMessage(filterLocales.nextWeek),
        value: 'nweek',
      },
      {
        label: intl.formatMessage(filterLocales.thisMonth),
        value: 'tmonth',
      },
      {
        label: intl.formatMessage(filterLocales.nextMonth),
        value: 'nmonth',
      },
      {
        label: intl.formatMessage(filterLocales.future),
        value: 'future',
      },
      {
        label: intl.formatMessage(filterLocales.previous),
        value: 'previous',
      },
      {
        label: intl.formatMessage(filterLocales.wot),
        value: 'notime',
      },
    ];
    const props = this.props;
    const subscriptionsData = this._formatInterestsData(
      this.props.subscriptions,
    );
    return (
      <Wrap>
        <Filters>
          <FilterSelect>
            <Select
              onChange={this._changeFilters}
              noBorder
              center
              data={dataFilter}
              type="dropdown"
              placeholder={intl.formatMessage(filterLocales.all)}
            />
          </FilterSelect>
          <FilterSelect>
            <Select
              onChange={this._changeInterest}
              multiple
              noBorder
              center
              data={subscriptionsData}
              type="dropdown"
              placeholder={intl.formatMessage(filterLocales.interests)}
            />
          </FilterSelect>
          <AddInterest onClick={this._openAddEvent} />
        </Filters>
        <div>
          {
            this.props.events.map((event, index) => (
              <Event
                lang={this.props.user.lang}
                deleteText={intl.formatMessage(globalLocales.delete)}
                editText={intl.formatMessage(globalLocales.edit)}
                onClickFav={this._handlerFavorite}
                onClickDelete={this._openDelete}
                onClickEdit={this._onClickEventEdit}
                isOwned={props.user.email === event.author}
                messages={eventMessages}
                key={`event_${event.id}`}
                lat={event.lat}
                longt={event.longt}
                onClickIntro={this._toggleEvent}
                isOpened={this.state.isOpenEvent[index]}
                index={index}
                title={event.descr}
                address={event.locat}
                sdate={event.sdate}
                edate={event.edate}
                stime={event.stime}
                etime={event.etime}
                ldescr={event.ldescr}
                categs={event.categs}
                categsData={this.props.subscriptions}
                contact={event.contact}
                email={event.email}
                tel={event.tel}
                url={event.url}
              />
            ))
          }
        </div>

        {
          this.state.isOpenAdd ? (
            <AddEventModal
              chooseMap={this._chooseMap}
              event={this.state.editingEvent || this.state.editingEvent === 0 ?
                this._getDataEditEvent(
                  this.props.events[this.state.editingEvent],
                  this.props.subscriptions,
                ) : undefined
              }
              submitHandler={this._handlerSubmitAddEvent}
              memberGroupsData={
                formatObjectStringToArraySelect(
                  formatObjectStringToArray(props.user.membergroups).concat([props.user.email]),
                )
              }
              subscriptions={subscriptionsData}
              isOpen={this.state.isOpenAdd}
              onRequestClose={this._closeAddEvent}
              contentLabel={
                this.state.editingEvent ?
                  this.props.intl.formatMessage(eventLocales.update) :
                  this.props.intl.formatMessage(eventLocales.new)
              }
            />
          ) : ''
        }
        {
          this.state.isOpenDelete ? (
            <DeleteModalConFirm
              deleteText={this.props.intl.formatMessage(globalLocales.delete)}
              confirmText={this.props.intl.formatMessage(eventLocales.confirmDelete)}
              cancelText={this.props.intl.formatMessage(globalLocales.cancel)}
              onClickConfirm={this._handlerDeleteEvent}
              onClickCancel={this._closeDelete}
              isOpen={this.state.isOpenDelete}
            />
          ) : null
        }
      </Wrap>
    );
  }
}

Events.propTypes = {
  intl: PropTypes.object,
  user: PropTypes.object,
  addNotification: PropTypes.func,
  getEvents: PropTypes.func,
  favEvent: PropTypes.func,
  createEvent: PropTypes.func,
  updateEvent: PropTypes.func,
  deleteEvent: PropTypes.func,
  getSubscriptions: PropTypes.func,
  events: PropTypes.array,
  subscriptions: PropTypes.array,
};

const mapDispatchToProps = dispatch => ({
  getEvents: (filters) => {
    dispatch({ type: 'GET_EVENTS', filters });
  },
  getSubscriptions: () => {
    dispatch({ type: 'GET_SUBSCRIPTIONS' });
  },
  createEvent(data, meta) {
    dispatch(
      {
        type: 'CREATE_EVENT',
        payload: data,
        meta,
      },
    );
  },
  updateEvent(data, meta) {
    dispatch(
      {
        type: 'UPDATE_EVENT',
        payload: data,
        meta,
      },
    );
  },
  deleteEvent(data, meta) {
    dispatch(
      {
        type: 'DELETE_EVENT',
        payload: data,
        meta,
      },
    );
  },
  favEvent(data, meta) {
    dispatch(
      {
        type: 'FAV_EVENT',
        payload: data,
        meta,
      },
    );
  },
});

const mapStateToProps = ({ user, events, subscriptions }) => ({
  user,
  events,
  subscriptions,
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Events, { withRef: true }));
