import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ModalBase from 'react-modal';
import GoogleMapReact from 'google-map-react';
import debounce from 'lodash.debounce';
import { injectIntl, intlShape } from 'react-intl';
import Button from '../../../components/Button';
import { MapMarker, IconCancel } from '../../../components/Icon';
import eventLocales from '../eventLocales';
import styledConfig from '../../../styledConfig';

const styleDefaultOverlay = {
  top: 0,
  bottom: 0,
  zIndex: styledConfig.zIndexMax - 1,
  overflow: 'auto',
  backgroundColor: 'transparent',
};

const styleDefaultContent = {
  position: 'static',
  top: '0',
  left: '0',
  bottom: '0',
  right: '0',
  padding: '0',
  border: '0',
  boxSizing: 'border-box',
  boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
  borderRadius: '0',
  width: '100%',
  margin: '0',
  fontFamily: styledConfig.fontFamily,
  background: 'rgba(255,255,255,0.9)',
  WebkitOverflowScrolling: 'touch',
  height: '100%',
};

const Content = styled.div`
  height: 100%;
  position: relative;
`;


const mapOptions = {
  disableDefaultUI: true,
  zoomControl: true,
  styles: [{
    featureType: 'all',
    elementType: 'labels',
    stylers: [{
      visibility: '#on',
    }],
  }],
};

const SearchBox = styled.div`
  position: absolute;
  top: 10px;
  left: 60px;
  width: 283px;
  z-index: 9999;
  
  > input {
    box-sizing: border-box;
    width: 100%;
    padding: 5px 10px;
    border: 1px solid ${styledConfig.colorBorder};
  }
`;

const CurrentAddress = styled.div`
  position: absolute;
  z-index: 999;
  bottom: 10px;
  right: 41px;
  left: 10px;
  background-color: #fff;
  padding-top: 10px;
  padding: 10px;
  box-sizing: border-box;
`;

const Controls = styled.div`
  margin-top: 10px;
  font-family: ${styledConfig.fontFamily};
  > ${Button} {
    display: block;
    margin-bottom: 5px;
    width: 100%;
  }
`;

const Results = styled.div`
  background-color: #fff;
  padding: 0 10px;
  max-height: 150px;
  overflow: auto;
  
  > ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    
    > li {
      padding: 5px 0;
      width: 100%;
      display: block;
      width: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`;

const MapMarkerContainer = styled.div`
  position: absolute;
  top: -30px;
  left: -30px;
  color: ${({ color }) => color || 'inherit'};
`;

const Close = styled.div`
    font-size: 20px;
    position: absolute;
    z-index: 999;
    background-color: #fff;
    padding: 10px;
    border-radius: 50%;
    height: 40px;
    width: 40px;
    box-sizing: border-box;
    top: 5px;
    left: 10px;
`;

const defaultCenter = { lat: 60.451555, lng: 22.2564718 };

class MapModal extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      placeLat: null,
      placeLng: null,
      currentAddress: '',
      center: defaultCenter,
      zoom: 15,
      searchResult: [],
      searchQuery: '',
    };
  }

  componentWillMount() {
    navigator.geolocation.getCurrentPosition((position) => {
      const pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      this.setState({
        center: pos,
      });
    }, (err) => {
      console.log(err);
    });

    this.search = debounce((e) => {
      const val = e.target.value;
      if (val) {
        this.placeService.textSearch({
          query: val,
        }, (results, status) => {
          if (status === 'OK') {
            this.setState({
              searchResult: results,
            });
          }
        });
      }
    }, 300);
  }

  componentWillUnmount = () => {
    this.search.cancel();
    this.maps.event.clearListeners('click', this._mapClick);
  };


  _onPlacesChanged = (e) => {
    this.setState({
      searchResult: [],
      searchQuery: e.target.value,
    });
    e.persist();
    this.search(e);
  };

  _onLoaded = ({ map, maps }) => {
    this.$map = map;
    this.maps = maps;
    this.placeService = new maps.places.PlacesService(map);
    this.geocoder = new maps.Geocoder();
    map.addListener('click', this._mapClick);
  };

  _mapClick = (e) => {
    if (e.placeId) {
      this.placeService.getDetails({
        placeId: e.placeId,
      }, (place, status) => {
        if (status === 'OK') {
          this.setState({
            center: {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
            },
            currentAddress: `${this._checkPlace(place) ? `${place.name}, ` : ''} ${place.formatted_address}`,
            placeLat: place.geometry.location.lat(),
            placeLng: place.geometry.location.lng(),
          });
        }
      });
    } else {
      this.geocoder.geocode({
        location: e.latLng,
      }, (places, status) => {
        if (status === 'OK') {
          this.setState({
            center: {
              lat: places[0].geometry.location.lat(),
              lng: places[0].geometry.location.lng(),
            },
            currentAddress: places[0].formatted_address,
            placeLat: places[0].geometry.location.lat(),
            placeLng: places[0].geometry.location.lng(),
          });
        }
      });
    }
  };

  _onChange = ({ center, zoom }) => {
    this.setState({
      center,
      zoom,
    });
  };

  _clear = () => {
    this.setState({
      placeLat: null,
      placeLng: null,
      currentAddress: '',
      searchResult: [],
    });
  };

  _callbackChoose = () => {
    this.props.onChoose({
      lat: this.state.placeLat,
      lng: this.state.placeLng,
      address: this.state.currentAddress,
    });
  };

  _chooseSearch = (e) => {
    const index = e.target.getAttribute('data-index');
    const place = this.state.searchResult[index];

    this.setState({
      searchQuery: '',
      searchResult: [],
      center: {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
      },
      currentAddress: place.formatted_address,
      placeLat: place.geometry.location.lat(),
      placeLng: place.geometry.location.lng(),
    });
  };

  _checkPlace = place => place.name
      && place.types.indexOf('street_address') === -1
      && place.types.indexOf('route') === -1;

  render() {
    const {
      contentLabel,
      preventClose,
      styleOverlay,
      styleContent,
      onRequestClose,
      intl,
      children,
      theme,
      ...props
    } = this.props;

    return (

      <ModalBase
        contentLabel={contentLabel}
        {...props}
        onRequestClose={preventClose ? null : onRequestClose}
        style={{
          overlay: Object.assign({}, styleDefaultOverlay, styleOverlay),
          content: Object.assign({}, styleDefaultContent, styleContent),
        }}
        options={mapOptions}
      >
        <Content>
          <Close onClick={this.props.onRequestClose}>
            <IconCancel />
          </Close>
          <SearchBox>
            <input
              value={this.state.searchQuery}
              placeholder={intl.formatMessage(eventLocales.clickOrSearch)}
              type="text"
              onChange={this._onPlacesChanged}
              ref={(ele) => { this.$searchBox = ele; }}
            />
            <Results>
              <ul>
                {
                  this.state.searchResult.map((place, index) => (
                    <li key={place.id} data-index={index} onClick={this._chooseSearch}>
                      {`${this._checkPlace(place) ? `${place.name}, ` : ''} ${place.formatted_address}`}
                    </li>
                    ))
                }
              </ul>
            </Results>
          </SearchBox>
          {
            this.state.currentAddress ? (
              <CurrentAddress>
                {this.state.currentAddress}
                <Controls>
                  <Button onClick={this._callbackChoose}>
                    {intl.formatMessage(eventLocales.setAddress)}
                  </Button>
                  <Button light onClick={this._clear}>
                    {intl.formatMessage(eventLocales.clearLocation)}
                  </Button>
                </Controls>
              </CurrentAddress>
            ) : null
          }
          <GoogleMapReact
            onChange={this._onChange}
            options={mapOptions}
            center={this.state.center}
            zoom={this.state.zoom}
            onGoogleApiLoaded={this._onLoaded}
            bootstrapURLKeys={{
              key: process.env.GOOGLE_MAP_KEY,
              libraries: 'places',
            }}
          >
            {
              this.state.placeLat ? (
                <MapMarkerContainer
                  lat={this.state.placeLat}
                  lng={this.state.placeLng}
                >
                  <MapMarker
                    height="60px"
                  />
                </MapMarkerContainer>
              ) : ''
            }
          </GoogleMapReact>
        </Content>

      </ModalBase>
    );
  }
}

MapModal.propTypes = {
  preventClose: PropTypes.bool,
  onRequestClose: PropTypes.func,
  onChoose: PropTypes.func,
  contentLabel: PropTypes.string,
  theme: PropTypes.object,
  styleOverlay: PropTypes.object,
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  intl: intlShape.isRequired,
};
export default injectIntl(MapModal);
