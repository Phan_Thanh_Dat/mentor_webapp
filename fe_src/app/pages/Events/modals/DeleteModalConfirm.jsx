import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { pure } from 'recompose';
import ModalConfirm from '../../../components/ModalConfirm';
import Button from '../../../components/Button';
import config from '../../../styledConfig';

const Text = styled.div`
  text-align: center;
  font-size: 20px;
  color: ${config.colorText};
`;

const ButtonAction = styled(Button)`
  margin-top: 10px;
  display: block;
  width: 150px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const DeleteModalConfirm = ({ isOpen, onClickCancel, onClickConfirm, deleteText, cancelText, confirmText }) => (
  <ModalConfirm isOpen={isOpen} contentLabel="Confirm delete">
    <Content>
      <Text>{confirmText}</Text>
      <ButtonAction danger onClick={onClickConfirm}>
        {deleteText}
      </ButtonAction>
      <ButtonAction onClick={onClickCancel}>
        {cancelText}
      </ButtonAction>
    </Content>
  </ModalConfirm>
);

DeleteModalConfirm.propTypes = {
  isOpen: PropTypes.bool,
  deleteText: PropTypes.string,
  cancelText: PropTypes.string,
  confirmText: PropTypes.string,
  onClickCancel: PropTypes.func.isRequired,
  onClickConfirm: PropTypes.func.isRequired,
};

export default pure(DeleteModalConfirm);
