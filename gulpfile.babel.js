
const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('scss', () => gulp.src('fe_src/**/*.scss')
  .pipe(sass({
    includePaths: ['node_modules'],
  }))
  .pipe(gulp.dest('fe')));

gulp.task('watch', () => {
  gulp.watch('fe_src/**/*.scss', ['scss']);
});
