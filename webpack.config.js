const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    vendor: ['babel-polyfill', './fe_src/vendor.js'],
  },

  output: {
    filename: '[name].js',
        // the output bundle

    path: path.resolve(__dirname, 'fe'),

    publicPath: '/',
        // necessary for HMR to know where to load the hot update chunks
  },
  module: {
    rules: [
            // {
            //     enforce: 'pre',
            //     test: /\.js|jsx?$/,
            //     loaders: [
            //         {
            //             loader: 'eslint-loader',
            //             options: {
            //                 fix: true,
            //                 emitWarning: true,
            //             },
            //         },
            //     ],
            //     exclude: /node_modules/,
            //     include: [path.join(__dirname, 'src')],
            // },
      {
        test: /\.js$/,
        use: [
          'babel-loader',
        ],
        exclude: /node_modules/,
      },
    ],
  },

  resolve: {
    extensions: ['.js'],
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
  ],
};
