<?php

namespace App\Controllers;

class PoisController
{
    protected $container;
    protected  $cyf;
    protected $param;

    const POI_PERSONAL = 'Personal';

    public function __construct($container)
    {
        $this->container = $container;
        $auth = new \AuthClass();
        $this->param = $auth->isAuth();
//        $this->param['email'] = 'teppo@yliopisto.fi';
        $this->cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());
    }

    /*
        POST /api/index.php/pois HTTP/1.1
        Host: cyf-app.local
        Content-Type: application/x-www-form-urlencoded

        sgroups=%7B%22Test+Group%22%7D&poiname=This+is+poi+name&categs=%7B%22Health%22%2C+%22Second+categ%22%7D&descr=This+is+description&url=http%3A%2F%2Ftest.url.com&locat=This+is+locat+name&longt=22.123456&lat=66.321654&aday=Aday+text&opentime=08%3A10%3A00&closetime=17%3A10%3A00&contact=Tan+Bui&tel=3216549687&email=tan%40mail.com&status=Active&contract=WTF
     */
    public function create($request, $response, $args)
    {
        $requestJson = $request->getParsedBody();

        $i = array();
        $i['author'] = "'" . $this->param['email'] . "'";
        if (!empty($requestJson['sgroups'])) {
            $i['sgroups'] = "'" . pg_escape_string($requestJson['sgroups']) . "'";//availabled
        } else {
            $i['sgroups'] = "'{" . $this->param['email'] . "}'";//availabled
        }
        $i['poiname'] = "'" . pg_escape_string($requestJson['poiname']) . "'";
        if (empty($requestJson['categs'])
            || $requestJson['categs'] != '{}')
            $i['categs'] = "'" . pg_escape_string($requestJson['categs']) . "'"; //subscriptions
        else $i['categs'] = "'{" . $requestJson['categs'] . "}'";

        $i['descr'] = "'" . pg_escape_string($requestJson['descr']) . "'";
        if (!empty($requestJson['url']))
            $i['url'] = "'" . pg_escape_string($requestJson['url']) . "'";
        if (!empty($requestJson['locat']))
            $i['locat'] = "'" . pg_escape_string($requestJson['locat']) . "'";
        if (!empty($requestJson['longt']))
            $i['longt'] = "'" . pg_escape_string($requestJson['longt']) . "'";
        if (!empty($requestJson['lat']))
            $i['lat'] = "'" . pg_escape_string($requestJson['lat']) . "'";

        if (!empty($requestJson['aday']))
            $i['aday'] = "'" . pg_escape_string($requestJson['aday']) . "'";
        if (!empty($requestJson['opentime'])
            and $requestJson['opentime'] != ' ')
            $i['opentime'] = "'" . pg_escape_string($requestJson['opentime']) . "'";
        if (!empty($requestJson['closetime'])
            and $requestJson['closetime'] != ' ')
            $i['closetime'] = "'" . pg_escape_string($requestJson['closetime']) . "'";

        if (!empty($requestJson['contact']))
            $i['contact'] = "'" . pg_escape_string($requestJson['contact']) . "'";
        if (!empty($requestJson['tel']))
            $i['tel'] = "'" . pg_escape_string($requestJson['tel']) . "'";
        if (!empty($requestJson['email']))
            $i['email'] = "'" . pg_escape_string($requestJson['email']) . "'";
        if (!empty($requestJson['status']))
            $i['status'] = "'" . pg_escape_string($requestJson['status']) . "'";
        if (!empty($requestJson['contract']))
            $i['contract'] = "'" . pg_escape_string($requestJson['contract']) . "'";

        $q = \GlobalConfiguration::pgGetInsertString($this->cyf, 'pois', $i);
        $q .= " RETURNING id, author, sgroups, timest, poiname, categs, descr, url, locat, longt, lat, aday, opentime, closetime, contact, tel, email, status, contract;";

        $result = pg_query($this->cyf, $q);
        $final = pg_fetch_all($result);
        if (empty($final)) {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the event.'
                ]);

        }

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($final);

    }

    /*
        PUT /api/index.php/pois HTTP/1.1
        Host: cyf-app.local
        Content-Type: application/x-www-form-urlencoded

        sgroups=%7B%22Test+Group+1%22%7D&poiname=This+is+poi+name+1&categs=%7B%22Health+1%22%2C+%22Second+categ+1%22%7D&descr=This+is+description+1&url=http%3A%2F%2Ftest1.url.com&locat=This+is+locat+name1&longt=22.1234561&lat=66.3216541&aday=Aday+text1&opentime=08%3A10%3A01&closetime=17%3A10%3A01&contact=Tan+Bui+Trung&tel=32165496871&email=tan1%40mail.com&status=Active1&contract=WTF1&pid=432
     */
    public function update($request, $response, $args)
    {
        $requestJson = $request->getParsedBody();

        $q = "UPDATE pois SET ";
        if (!empty($requestJson['pid']) AND $requestJson['pid'] > 0
            AND isset($requestJson['poiname'])) {
            if (!empty($requestJson['sgroups'])) {
                $i['sgroups'] = pg_escape_string($requestJson['sgroups']);//availabled
            } else {
                $i['sgroups'] = "{" . $this->param['email'] . "}";//availabled
            }
            $i['poiname'] = pg_escape_string($requestJson['poiname']);
            if (empty($requestJson['categs'])
                || $requestJson['categs'] != '{}')
                $i['categs'] = pg_escape_string($requestJson['categs']); //subscriptions
            else $i['categs'] = "'{" . $requestJson['categs'] . "}'";

            $i['descr'] = pg_escape_string($requestJson['descr']);
            if (isset($requestJson['url']))
                $i['url'] = pg_escape_string($requestJson['url']);
            if (isset($requestJson['locat']))
                $i['locat'] = pg_escape_string($requestJson['locat']);
            if (isset($requestJson['longt']))
                $i['longt'] = pg_escape_string($requestJson['longt']);
            if (isset($requestJson['lat']))
                $i['lat'] = pg_escape_string($requestJson['lat']);

            if (isset($requestJson['aday']))
                $i['aday'] = pg_escape_string($requestJson['aday']);
            if (isset($requestJson['opentime'])
                and $requestJson['opentime'] != ' ')
                $i['opentime'] = pg_escape_string($requestJson['opentime']);
            if (isset($requestJson['closetime'])
                and $requestJson['closetime'] != ' ')
                $i['closetime'] = pg_escape_string($requestJson['closetime']);

            if (isset($requestJson['contact']))
                $i['contact'] = pg_escape_string($requestJson['contact']);
            if (isset($requestJson['tel']))
                $i['tel'] = pg_escape_string($requestJson['tel']);
            if (isset($requestJson['email']))
                $i['email'] = pg_escape_string($requestJson['email']);
            if (isset($requestJson['status']))
                $i['status'] = pg_escape_string($requestJson['status']);
            if (isset($requestJson['contract']))
                $i['contract'] = pg_escape_string($requestJson['contract']);
            $q .= \GlobalConfiguration::pgGetUpdateFieldString($i);
            $q .= " WHERE id=" . pg_escape_string($requestJson['pid']);
            $q .= " AND (";
            $q .= " author='" . $this->param['email'] . "'";
            $q .= ") ";
            $q .= " RETURNING id, author, sgroups, timest, poiname, categs, descr, url, locat, longt, lat, aday, opentime, closetime, contact, tel, email, status, contract;";
        } else {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the POI.'
                ]);
        }

        $result = pg_query($this->cyf, $q);
        $final = pg_fetch_all($result);

        if (empty($final)) {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the POI.'
                ]);

        }

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson(pg_fetch_all($result));

    }

    /*
     DELETE /api/index.php/pois HTTP/1.1
    Host: cyf-app.local
    Content-Type: application/x-www-form-urlencoded

    id=432
     */
    public function delete($request, $response)
    {
        $requestJson = $request->getParsedBody();
        $id = pg_escape_string($requestJson['id']);
        $q = "DELETE FROM pois WHERE id={$id} AND author='{$this->param['email']}';";

        $res = pg_query($this->cyf, $q);
        $final = pg_affected_rows($res);

        if ($final === 1) {
            return $response->withHeader(
                'Content-Type',
                'application/json'
            )
                ->withJson('ok');
        } else {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the POI or you do not have the permission.'
                ]);
        }
    }

    /**
        http://cyf-app.local/api/index.php/pois?fcategs={}
        http://cyf-app.local/api/index.php/pois?fcategs={Personal}

     *  http://cyf-app.local/api/index.php/pois?fcategs={Football%20fields}
        http://cyf-app.local/api/index.php/pois?fcategs={Football%20fields,Health}
     *  http://cyf-app.local/api/index.php/pois?fcategs={}&distance=<0.5km&lat=60.4518126&long=22.2666302
        http://cyf-app.local/api/index.php/pois?fcategs={}&distance=<1km&lat=60.4518126&long=22.2666302

     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function get($request, $response, $args)
    {
        $q = "SELECT id, author, sgroups, timest, poiname, categs, descr, url, locat, longt, lat, aday, opentime, closetime, contact, tel, email, status, contract FROM pois";

        if ($_REQUEST['fcategs'] == '{' . $this::POI_PERSONAL . '}') {     //for logged in user's POIs
            $q .= " WHERE author='{$this->param['email']}'";
        } elseif ($_REQUEST['fcategs'] != '{}') {
            //for list of POIs for info pages
            $q .= "  WHERE categs && '" . pg_escape_string($_REQUEST['fcategs']) . "'";
        }
        $q .= " ORDER BY timest;";

        $result = pg_query($this->cyf, $q);
        $pois = pg_fetch_all($result);

        if (!empty($_REQUEST['distance']) && !empty($_REQUEST['lat']) && !empty($_REQUEST['long'])) {
            $filterResult = [];
            switch ($_REQUEST['distance']) {
                case "<0.5km": $distance = 500; break;
                case "<3km": $distance = 3000; break;
                default: $distance = 1000; break;
            }

            foreach ($pois as $poi) {
                $measureDistance = \GlobalConfiguration::getDistanceMeters($_REQUEST['lat'], $_REQUEST['long'], $poi['lat'], $poi['longt']);
                if ($measureDistance < $distance) {
                    $poi['distance'] = $measureDistance;
                    $filterResult[] = $poi;
                }
            }
            $pois = $filterResult;
        }
        return $response
        ->withHeader(
            'Content-Type',
            'application/json'
        )
        ->withJson($pois == false ? [] : $pois);
    }

}
