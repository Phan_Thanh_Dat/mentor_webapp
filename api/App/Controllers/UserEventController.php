<?php

namespace App\Controllers;

class UserEventController
{
    protected $container;
    protected  $cyf;
    protected $param;

    public function __construct($container)
    {
        //Disable these two lines for testing without authenticate
        //  but the request param must have email
        $auth = new \AuthClass();
        $this->param = $auth->isAuth();

        $this->container = $container;
        $this->cyf = pg_connect(\GlobalConfiguration::getCyfConnectionString());
    }

    private function responseWrapper($response, $final = []){
        if (empty($final)) {
            return $response
                ->withHeader(
                    'Content-Type',
                    'application/json'
                )
                ->withStatus(404)
                ->withJson([
                    'message' => 'Can not find the event.'
                ]);

        }

        return $response
            ->withHeader(
                'Content-Type',
                'application/json'
            )
            ->withJson($final);
    }

    public function getEventIdsByUser(){
        $q = "SELECT event_id FROM user_event";
        $q .= " WHERE email='{$this->param['email']}';";

        $result = pg_query($this->cyf, $q);
        return pg_fetch_all($result);
    }

    /**
     *  POST /api/index.php/user-event HTTP/1.1
        Host: cyf-app.local
        Content-Type: application/x-www-form-urlencoded
        Postman-Token: 29bafcb8-30be-840a-bc75-806d757a2013

        event_id=1111&email=teppo%40yliopisto.fi

     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function add($request, $response, $args) {
        $requestJson = $request->getParsedBody();

        $data['email'] = $this->param['email'] ? $this->param['email'] : $requestJson['email'];
        $data['email'] = "'" . $data['email'] . "'";
        $data['event_id'] = "'" . $requestJson['id'] ."'";

        $q = \GlobalConfiguration::pgGetInsertString($this->cyf, 'user_event', $data);
        $q .= " RETURNING id, email, event_id, favorite_flag, description";

        $result = pg_query($this->cyf, $q);
        return $this->responseWrapper($response, pg_fetch_all($result));
    }

    /**
     *  DELETE /api/index.php/user-event HTTP/1.1
        Host: cyf-app.local
        Content-Type: application/x-www-form-urlencoded
        Postman-Token: c70eb4b6-83eb-d02e-5c6d-04ca98098748

        event_id=1111&email=teppo%40yliopisto.fi
     *
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function delete ($request, $response, $args) {
        $requestJson = $request->getParsedBody();
        $data['email'] = $this->param['email'] ? $this->param['email'] : $requestJson['email'];

        $q = "DELETE FROM user_event WHERE email='{$data['email']}' AND event_id='{$requestJson['event_id']}';";
        $res = pg_query($this->cyf, $q);
        $final = pg_affected_rows($res);

        $message = "Cannot delete event";
        if ($final == 1){
            $message = "Deleted";
        }

        return $this->responseWrapper($response, [$message]);
    }
}
