<?php
include_once "Haka/HakaAuth.php";
$haka = new HakaAuth();
if (!empty($haka->haka_attributes)) {
    header("Location: /fe/index.html");
    exit;
}
function echo_redirect() {
    echo <<<EOT
<!DOCTYPE html><html><head>
</head>
<body>
<script>
setTimeout(function(){ 
    window.location.replace("/fe/index.html");
 }, 1000);
</script>
</body>
</html>
EOT;
}

if ($_POST){
    $testAccount = array (
        0 => 'cyf@gmail.com',
        1 => 'P@ssw0rd',
    );
    if ($_POST['username'] == $testAccount[0] && $_POST['password'] == $testAccount[1]) {
        setcookie("Haka_Info", "YTo2OntzOjExOiJkaXNwbGF5TmFtZSI7YToxOntpOjA7czo1OiJUZXBwbyI7fXM6MjE6InNjaGFjSG9tZU9yZ2FuaXphdGlvbiI7YToxOntpOjA7czoxMjoieWxpb3Bpc3RvLmZpIjt9czoyMjoiZWR1UGVyc29uUHJpbmNpcGFsTmFtZSI7YToxOntpOjA7czoxODoidGVwcG9AeWxpb3Bpc3RvLmZpIjt9czoyOiJzbiI7YToxOntpOjA7czoxNjoiVGVzdGlrw6R5dHTDpGrDpCI7fXM6MjU6InNjaGFjSG9tZU9yZ2FuaXphdGlvblR5cGUiO2E6MTp7aTowO3M6NDQ6InVybjpzY2hhYzpob21lT3JnYW5pemF0aW9uVHlwZTpmaTp1bml2ZXJzaXR5Ijt9czoyOiJjbiI7YToxOntpOjA7czoyMjoiVGVwcG8gVGVzdGlrw6R5dHTDpGrDpCI7fX1teWNvb2tpZWtleTEyMzQ1Ng%3D%3D",time()+604800, "/", "", false);
        echo_redirect();
        header("Location: /fe/index.html");
        exit;
    }
} else {?>

    <!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.icons-1.4.5.min.css">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.inline-png-1.4.5.min.css">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.inline-svg-1.4.5.min.css">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.structure-1.4.5.min.css">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.theme-1.4.5.min.css">
        <link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.external-png-1.4.5.min.css">
        <style>
            .ui-icon-mydownload:after {
                background-image: url("../cyf/img/download.png");
                background-position: 3px 3px;
                background-size: 70%;
            }
        </style>
        <script src="../cis/jquery-2.1.4.min.js"></script>
        <!--<script src="../cis/jquery-3.1.0.min.js"></script>-->
        <title>Invitation page</title>
    </head>
    <body>
    <div data-role="page">
        <div data-role="header">
            <a href="../index.php" class="ui-btn ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext" rel="external">Home</a>
            <h1>CYF Digital Services</h1>
        </div>
        <div role="main" class="ui-content">
            <form method="POST" action="/cis/login_other.php">
                <input type="email" name="username" id="username" placeholder="Your email...">
                <input type="password" name="password" id="password">
                <input type="submit" value="Enter with authorization">
            </form>
        </div>
    </body>
    </html>

<?php }?>
