<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<!--<link rel="stylesheet" type="text/css" href="main.css"> -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="functions.js"></script>
<style>
td {margin: 2px;padding-left: 6px;text-align:left;}
label {padding-left: 10px;font-style: normal;font-style: italic;font-size: 1em;font-weight: normal;}
input[type=text]{
font-size: 0.875em;
width: 100%;
padding: 3px 3px;
margin: 4px 0;
box-sizing: border-box;
}
input[type=submit]{
font-size: 0.875em;
width: 100%;
}
</style>
<script>
window.onload = function(){
}
</script>
<title>CIS Groups Control Tool</title></head>
<body>
EOT;
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';

echo '<form id="f" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';

echo '<center>';


$StringId='';
if(isset($_POST['StringId']))
    $StringId=htmlspecialchars(stripslashes(trim($_POST['StringId'])));
$name='';
$rungroups='';
if(isset($_POST['owner']))
    $owner=htmlspecialchars(stripslashes(trim($_POST['owner'])));
$usefilters = '';
if(isset($_POST['usefilters']))
    $usefilters=htmlspecialchars(stripslashes(trim($_POST['usefilters'])));
$fgname = '';
if(isset($_POST['fgname']))
    $fgname=htmlspecialchars(stripslashes(trim($_POST['fgname'])));
$fowner = '';
if(isset($_POST['fowner']))
    $fowner=htmlspecialchars(stripslashes(trim($_POST['fowner'])));

$tcmd='';


if(isset($_POST['tcmd'])){
    $tcmd = htmlspecialchars(stripslashes(trim($_POST['tcmd'])));
    if($tcmd=="Insert"){
        $query = "INSERT INTO groups (gname,owner) VALUES (";
        $query .= " '".pg_escape_string($_POST['gname'])."'";
        $query .= ",'".pg_escape_string($_POST['owner'])."'";
        $query .= ");";
//        echo $query . "<br>";
        $result = @pg_query($conn, $query);
        if (!$result) $last_error = pg_last_error($conn);
    }else if($tcmd=="Update"){
        if($StringId != ''){
            $query = "UPDATE groups SET";
            $query .= " gname='".pg_escape_string($_POST['gname'])."'";
            $query .= ",owner='".pg_escape_string($_POST['owner'])."'";
            $query .= " WHERE id=".pg_escape_string($_POST['StringId']).";";
//        echo $query . "<br>";
            $result = @pg_query($conn, $query);
            if (!$result) $last_error = pg_last_error($conn);
        }else $last_error = 'item for update not selected';
    }else if($tcmd=="Delete"){
        $query = "DELETE FROM groups WHERE id=".pg_escape_string($_POST['StringId']).";";
//        echo $query . "<br>";
        $result = @pg_query($conn, $query);
        if (!$result) $last_error = pg_last_error($conn);
    }
}
echo <<<EOT
<div style="text-align:center;">CIS Groups Control Tool ({$param['name']})</div>
<hr>
EOT;

    $query = <<<EOT
SELECT id,gname,owner
FROM groups
EOT;

if($_REQUEST['usefilters'] =='on'){
    $qs='';
    $filters = array();
    if(($_POST['fgname'] =='on') & ($_POST['gname'] !=''))
       $filters[] = "gname LIKE '%" . pg_escape_string($_POST['gname']) ."%'";
    if(($_POST['fowner'] =='on') & ($owner !=''))
       $filters[] = "owner LIKE '%" . pg_escape_string($_POST['owner']) ."%'";

    $qs = implode(' AND ',$filters);
    if($qs != '') $query .= ' WHERE ' . $qs;
}
$query .= ' ORDER BY gname;';
//echo $query; echo '<br>';
echo '<table><caption style="text-align:center;">Groups</caption><tr><td>';

echo '<select style="width:auto;"id="String" size=14 onclick="';
echo "document.getElementById('StringId').value=document.getElementById('String').value;";
echo "var e = GetSimpleListFromSelected(document.getElementById('String'));";
echo "document.getElementById('gname').value=e[0].trim();";
echo "document.getElementById('owner').value=e[1].trim();";
echo '">';
    
    $result = @pg_query($conn, $query);
    if (!$result) echo pg_last_error($conn);
    $firows = pg_num_rows($result);
    if($firows ==0){
 echo '<option value="0">';
        echo '(empty)';
 echo '</option>';
    }else{
        $fi=getValues($result);
        for ($j=0;$j<$firows;$j++){
 echo '<option value="'. $fi[$j]['id'] .'"';
 echo '>';
            echo ''. $fi[$j]['gname'] .' | ';
            echo ''. $fi[$j]['owner'] .'';
 echo '</option>';
        }
    }
    echo '</select></td></tr></table>';
    
    echo <<<EOT
<input type="hidden" id="StringId" name="StringId" value="" size=5 >
<table><caption style="text-align:center;">Groups/Filters</caption>
<tr>
<th>Name:</th>
<td><input type="text" id="gname" name="gname" value=""></td>
<td><input type="checkbox" id="fgname" name="fgname" ><label>find&nbsp;groups&nbsp;with name like this</label></td>
</tr>
<tr>
<th>owner:</th>
<td><input type="text" id="owner" name="owner" value=""></td>
<td><input type="checkbox" id="fowner" name="fowner" ><label>find&nbsp;groups&nbsp;with owner like this</label></td>
</tr>
</table>
EOT;

echo <<<EOT
<table><caption style="text-align:center;">What are we doing?</caption>
<tr>
<td><input type="radio" id="tcmd" name="tcmd" value="Insert"><label>Insert&nbsp;group&nbsp;</label></td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete"><label>Delete&nbsp;group&nbsp;</label></td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update"><label>Update&nbsp;group&nbsp;</label></td>
</tr>
<tr>
<td><input type="radio" id="tcmd" name="tcmd" value="View" checked><label>View&nbsp;groups</label></td>
<td>
EOT;

echo '<input type="checkbox" id="usefilters" name="usefilters" ';
if($usefilters=='on'){    
    echo 'checked';
}
echo '><label>use filters to view apps</label>';

    echo <<<EOT
</td>
</tr>
<tr>
<td></td>
<td><input type="submit" name="pcmd" value="Groups" style="width:120px;"></td>
<td></td>
</tr>
</table>
EOT;
    
$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) == 2){
    echo $stat[1];
}else echo $last_error;
echo '</div>';
echo '<div><a href="../cis/login.php">Go Back</a></div>';


echo <<<EOT
<center><div>&copyCIS Groups Control Tool</div>
</div></center>
</form></body>
</html>
EOT;
?>