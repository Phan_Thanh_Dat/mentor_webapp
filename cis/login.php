<?php
include '../cis/validate.php';
$auth = new \AuthClass();
$param = $auth->isAuth(false);

if (!is_null($param)) {
    //header("Location: /fe/index.html?haka=" . $haka->getEncodedAuthJwt());
    header("Location: /fe/index.html");
    exit;
}

include '../cis/dbfunctions.php';
function get_list_apps($conn,$query){
    $result = pg_query($conn, $query);
    if ($result) {
        $firows = pg_num_rows($result);
        if($firows !=0){
            $fi=getValues($result);
            if($firows ==1){
                $data = array('app'=>$fi[0]['name'], 'rgroups'=>$fi[0]['rungroups']);
                $s="Location: ".$fi[0]['modelurl']."?".http_build_query($data);
//                echo $s;
                header($s);
            }
    echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.icons-1.4.5.min.css">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.inline-png-1.4.5.min.css">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.inline-svg-1.4.5.min.css">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.structure-1.4.5.min.css">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.theme-1.4.5.min.css">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.external-png-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<!--<script src="../cis/jquery-3.1.0.min.js"></script>-->
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<title>CYF Digital Services</title>
<script></script>
</head><body>
<div data-role="page">
  <div data-role="header" data-position="fixed">
    <a href="../index.php" class="ui-btn ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext" rel="external">Home</a>
    <h1>CYF system</h1>
  </div>
<div role="main" class="ui-content">
EOT;
    echo '<form id="f" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';

            for ($j=0;$j<$firows;$j++){
                echo '<a class="ui-btn" rel="external"';
                echo " href='{$fi[$j]['modelurl']}?";
                $data = array('app'=>$fi[$j]['name'], 'rgroups'=>$fi[$j]['rungroups']);
                echo http_build_query($data);
                echo "'>".$fi[$j]['name'].'</a>';
            }
        }
        echo "</form></div></body></html>";
    }
}

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth(false);
if ($param['user_id'] != 0) {
    $q="SELECT id, owner, rungroups, name, modelurl, iconurl FROM apps";
    if(!empty($param['membergroups'])) {
        $q .= " WHERE rungroups && '{$param['membergroups']}'";
        if(!empty($param['ownergroups'])) $q .= " OR rungroups && '{$param['ownergroups']}'";
    }else
        if(!empty($param['ownergroups'])) $q .= " WHERE rungroups && '{$param['ownergroups']}'";
    $q .= " ORDER BY name;";
//            echo $q.'<br>';
    get_list_apps($conn,$q);
    exit(0);
}
if(isset($_POST['pcmd'])){
    $cmd = htmlspecialchars(stripslashes(trim($_POST['pcmd'])));
    if($cmd === 'Registration'){
        $auth->invitation();
        exit(0);
    } else {
        if($cmd === 'Enter with authorization'){
            if($auth->auth($conn)){
                $param = $auth->isAuth(false);
                if ($param['user_id'] != 0) {
                    $q="SELECT id, owner, rungroups, name, modelurl, iconurl FROM apps";
                    if(!empty($param['membergroups'])) {
                        $q .= " WHERE rungroups && '{$param['membergroups']}'";
                        if(!empty($param['ownergroups'])) $q .= " OR rungroups && '{$param['ownergroups']}'";
                    }else
                        if(!empty($param['ownergroups'])) $q .= " WHERE rungroups && '{$param['ownergroups']}'";
                    $q .= " ORDER BY name;";
//                  echo $q.'<br>';
                    get_list_apps($conn,$q);
                    exit(0);
                }
            }
        }
//        if($cmd === "My mobile tutor(Android)"){
//            $s="Location: ../cyf/mymobiletutor.apk";
//            echo "<html><head></head><body>".$s."</body></html>";
//            header($s);
//            exit(0);
//        }
    }
    $auth->auth_nok();
    $q="SELECT id,owner,rungroups,name,modelurl,iconurl FROM apps WHERE rungroups && '{All}' ORDER BY name;";
    get_list_apps($conn,$q);
    exit(0);
}
$auth->invitation();
exit(0);


?>