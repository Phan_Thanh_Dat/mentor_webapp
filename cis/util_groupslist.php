<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Groups List</title>
<style>
tr:nth-child(even) {background-color: #f2f2f2}
tr:hover {background-color: #ddd;}
td {
padding-left:6px;
text-align:left;
}
</style>
<script>
EOT;
echo "function setParam(e)";
echo "{";
if(!empty($_REQUEST['action'])){
    echo "var f=document.createElement('form');";
    echo "var i=document.createElement('input');";
    echo "f.method='GET';";
    echo "f.action='{$_REQUEST['action']}';";
    if(!empty($_REQUEST['frame'])){
        echo "f.target='{$_REQUEST['frame']}';";
    }
    echo "i.type='hidden';";
    echo "i.value=e.dataset.id;";
    echo "i.name='gid';";
    echo "f.appendChild(i);";
    echo "document.body.appendChild(f);";
    echo "f.submit();";
}
echo "}";
/* echo "function setParam(e){"; */
/* if(!empty($_REQUEST['frame'])){ */
/*    echo "document.getElementById('gid').value=e.dataset.id;"; */
/*    echo "document.getElementById('{$_REQUEST['frame']}').submit();"; */
/* } */
/* echo "}"; */

echo "</script></head><body style='font-size:12px;'><center><table>";

//echo "post: ";print_r($_REQUEST);echo "<br>";

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$query = "SELECT id,gname,owner FROM groups";
if($_REQUEST['fltrs'] =='y'){
    $qs='';
    $filters = array();
    if(!empty($_REQUEST['fgname']))
        $filters[] = "gname LIKE '%" . pg_escape_string($_REQUEST['fgname']) ."%'";
    if(!empty($_REQUEST['fowner']))
        $filters[] = "owner LIKE '%" . pg_escape_string($_REQUEST['fowner']) ."%'";
    $qs = implode(' AND ',$filters);
    if($qs != '') $query .= ' WHERE ' . $qs;
}
$query .= ' ORDER BY gname;';
//echo $query; echo '<br>';
$result = @pg_query($conn, $query);
if (!$result) $last_error = pg_last_error($conn);
else{
    $firows = pg_num_rows($result);
    if($firows >0){
        $fi=getValues($result);
        for ($j=0;$j<$firows;$j++){
            echo "<tr onclick='setParam(this)' data-id='{$fi[$j]['id']}'><td>{$fi[$j]['gname']}</td>";
            echo "<td>{$fi[$j]['owner']}</td></tr>";
        }
    }
}

echo "</table></center>";
if(!empty($_REQUEST['frame']) and !empty($_REQUEST['action'])){
   echo "<form method='post' action='{$_REQUEST['action']}' target='{$_REQUEST['frame']}' id='{$_REQUEST['frame']}'>";
   echo "<input type='hidden' id='gid' name='gid'></form>"; // value='{$gid}'
}

echo <<<EOT
<form method='get' action='{$_REQUEST['action']}?gid=$gid' target='{$_REQUEST['frame']}' id='{$_REQUEST['frame']}'>
<input type='hidden' id='gid' name='gid'></form>
</body></html>
<!--
target="blank |  _parent |  _self |  _top | framename"
<a target="_self" href="../cis/
util_groupslist.php?frame=_self&action=../cis/util_groupsform.php&fltrs=usefilters&fowner=Kate+Panina
">

for embed:
<iframe name="groupslist" width="400" height="800"
 src="../cis/util_groupslist.php?frame=YOUR_FRAME&action=YOUR_MODULE&fltrs=y&fowner=Kate+Panina">
</iframe>
and/or form
<form method="post" action="../cis/util_groupslist.php" target="groupslist" id="groupslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="fltrs" name="fltrs">
<input type="hidden" id="fgname" name="fgname">
<input type="hidden" id="fowner" name="fowner">
</form>
-->
EOT;
?>