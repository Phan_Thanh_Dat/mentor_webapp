<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cis/util_contractsform.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Groups List</title>
<style>
label {padding-left:6px;}
input[type=checkbox] {margin-right:6px;}
input[type=radio] {margin-right:6px;}
input[type=text] {width:300px;}
textarea {width:300px;}
legend {font-size:1.2em;}
td {
margin-left: 6px;
padding-left:6px;
text-align:left;
}
</style>
<script>
function setParam(){
 document.getElementById('frame').value='contractsform';
 document.getElementById('action').value='../cis/util_contractsform.php';
 if(document.getElementById('ufilters').checked){
   document.getElementById('usefilters').value='usefilters';
   if(document.getElementById('ifcname').checked)
     document.getElementById('fcname').value=document.getElementById('icname').value;
   if(document.getElementById('ifbname').checked)
     document.getElementById('fbname').value=document.getElementById('ibname').value;
   if(document.getElementById('ifaddress').checked)
     document.getElementById('faddress').value=document.getElementById('iaddress').value;
   if(document.getElementById('ifbid').checked)
     document.getElementById('fbid').value=document.getElementById('ibid').value;
   if(document.getElementById('ifosec').checked)
     document.getElementById('fosec').value=document.getElementById('iosec').value;
   if(document.getElementById('ifcontact').checked)
     document.getElementById('fcontact').value=document.getElementById('icontact').value;
   if(document.getElementById('iflang').checked)
     document.getElementById('flang').value=document.getElementById('ilang').value;
   if(document.getElementById('iftel').checked)
     document.getElementById('ftel').value=document.getElementById('itel').value;
   if(document.getElementById('ifemail').checked)
     document.getElementById('femail').value=document.getElementById('iemail').value;
   if(document.getElementById('ifurl').checked)
     document.getElementById('furl').value=document.getElementById('iurl').value;
   if(document.getElementById('ifdstart').checked)
     document.getElementById('fdstart').value=document.getElementById('idstart').value;
   if(document.getElementById('ifdend').checked)
     document.getElementById('fdend').value=document.getElementById('idend').value;
   if(document.getElementById('ifmanager').checked)
     document.getElementById('fmanager').value=document.getElementById('imanager').value;
   if(document.getElementById('ifstatus').checked)
     document.getElementById('fstatus').value=document.getElementById('istatus').value;
   if(document.getElementById('ifcontractgroups').checked)
     document.getElementById('fcontractgroups').value=document.getElementById('icontractgroups').value;
   if(document.getElementById('ifusersgroups').checked)
     document.getElementById('fusersgroups').value=document.getElementById('iusersgroups').value;
   if(document.getElementById('ifapps').checked)
     document.getElementById('fapps').value=document.getElementById('iapps').value;
 }
 document.getElementById('contractslist').submit();

	var f = document.createElement("form");
	f.setAttribute('method',"post");
	f.setAttribute('target',"groupslist");
	f.setAttribute('action',"../cis/util_groupslist.php");

//var i = document.createElement("input");
//	i.setAttribute('type',"hidden");
//	i.setAttribute('name',"fmanager");
//  i.setAttribute('value','{$_REQUEST['fmanager']}');
//	f.appendChild(i);

//var j = document.createElement("input");
//	j.setAttribute('type',"hidden");
//	j.setAttribute('name',"fltrs");
//  j.setAttribute('value','on');
//	f.appendChild(j);

	document.getElementsByTagName('body')[0].appendChild(f);
	f.submit();
}
</script>
</head>
<body onload="setParam();" style="font-size:12px;">
EOT;

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf=cyfConnect();
//echo "post: ";print_r($_POST);echo "<br>";


$gid = 0;
if(!empty($_POST['gid'])) $gid = $_POST['gid'];
$cname = $_POST['cname'];
$bname = $_POST['bname'];
$address = $_POST['address'];
$bid = $_POST['bid'];
$osec = $_POST['osec'];
$contact = $_POST['contact'];
$lang= $_POST['lang'];
$tel = $_POST['tel'];
$email = $_POST['email'];
$url = $_POST['url'];
$dstart = $_POST['dstart'];
$dend = $_POST['dend'];
$manager = $_POST['manager'];
$status = $_POST['status'];
$maxusers = $_POST['maxusers'];
$contractgroups = $_POST['contractgroups'];
$usersgroups = $_POST['usersgroups'];
$apps = $_POST['apps'];

echo '<form id="contractsform" target="contractsform" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';

if(!empty($_POST['pcmd']) and !empty($_POST['tcmd'])){
//self

if(($_POST['tcmd']==="Delete") or ($_POST['tcmd']==="Update") or ($_POST['tcmd']==="Insert")){
    $c=array();
    $c['cname'] = "'".$_POST['cname']."'";
    $c['bname'] = "'".$_POST['bname']."'";
    $c['address'] = "'".$_POST['address']."'";
    $c['bid'] = "'".$_POST['bid']."'";
    $c['osec'] = "'".$_POST['osec']."'";
    $c['contact'] = "'".$_POST['contact']."'";
    $c['lang'] = "'".$_POST['lang']."'";
    $c['tel'] = "'".$_POST['tel']."'";
    $c['email'] = "'".$_POST['email']."'";
    $c['url'] = "'".$_POST['url']."'";
    $c['dstart'] = "'".$_POST['dstart']."'";
    $c['dend'] = "'".$_POST['dend']."'";
    $c['status'] = "'".$_POST['status']."'";
    $c['manager'] = "'".$_POST['manager']."'";
    $c['maxusers'] = $_POST['maxusers'];
    $c['contractgroups'] = "'".$_POST['contractgroups']."'";
    $c['usersgroups'] = "'".$_POST['usersgroups']."'";
    $c['apps'] = "'".$_POST['apps']."'";

    if($_POST['tcmd']==="Delete" and !empty($_POST['gid'])){
        $c['id'] = "'".$_POST['gid']."'";
        $q="DELETE FROM contracts WHERE id={$_POST['gid']};";
//      echo "contracts:";print_r($q);echo "<hr>";
        pg_query($cyf, $q);
        $q="DELETE FROM logins WHERE email='{$_POST['email']}';";
//      echo "logins:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        $q="DELETE FROM groups WHERE ARRAY[gname] && '{{$_POST['usersgroups']}}' OR  ARRAY[owner] && '{{$_POST['usersgroups']}}';";
//      echo "groups:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        $q="DELETE FROM groups WHERE ARRAY[gname] && '{{$_POST['contractgroups']}}' OR  ARRAY[owner] && '{{$_POST['contractgroups']}}';";
//      echo "groups:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        $q="DELETE FROM users WHERE ARRAY[manager] && '{{$_POST['contractgroups']}}';";
//      echo "users:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        unset($_POST['email']);
        unset($_POST['gid']);

    }else{
        $cgroups = ltrim(rtrim($_POST['contractgroups'],'" }'),'" {');
        $ugroups = ltrim(rtrim($_POST['usersgroups'],'" }'),'" {');
        $q="SELECT count(*) as c  FROM groups WHERE gname='{$cgroups}' AND owner!={$c['manager']};";
//echo "<hr>";print_r($q);echo "<hr>";
        $res=pg_query($conn,$q);
        $arr = pg_fetch_assoc($res, 0);
//echo "<hr>";print_r($arr);echo "<hr>";
        if($arr['c'] ==0) {
            $q="DELETE FROM groups WHERE gname='{$cgroups}' AND owner={$c['manager']};";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            $q="INSERT INTO groups(gname,owner) VALUES ('{$cgroups}',{$c['manager']});";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            $q="DELETE FROM groups WHERE owner={$cgroups};";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            $q="INSERT INTO groups(gname,owner) VALUES ('{$ugroups}','{$cgroups}');";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            if($_POST['tcmd']==="Update" and !empty($_POST['gid'])){
                $p = '';
                if(!empty($_POST['psword'])) $p= "psword='".$_POST['psword']."',";
                $q="UPDATE logins SET email={$c['email']}, {$p} name={$c['contact']}, ownergroups={$c['usersgroups']}, membergroups={$c['contractgroups']} WHERE email={$c['email']};";
echo "<hr>";print_r($q);echo "<hr>";
                pg_query($conn,$q);
                $cond=array();
                $cond['id']=$_POST['gid'];
                pgUpdate($cyf, 'contracts', $c, $cond);
                unset($_POST['gid']);
            }
            if($_POST['tcmd']==="Insert"){
//                if($_POST['qusers'] >0)
                    if(!empty($_POST['email']) and !empty($_POST['psword'])){
                        $q="INSERT INTO logins(email,psword,name,ownergroups,membergroups) VALUES ({$c['email']},'{$_POST['psword']}',{$c['contact']},{$c['usersgroups']},{$c['contractgroups']});"; 
echo "<hr>";print_r($q);echo "<hr>";
                        pg_query($conn,$q);
                        pgInsert($cyf, 'contracts', $c);
                        unset($_POST['gid']);
                
                    }
            }
        }

    }
}
}
        
if(empty($_POST['usefilters']) and !empty($_POST['gid'])){
//callback
    $q = <<<EOT
SELECT id,cname,bname,address,bid,osec,contact,lang,tel,email,url,manager,
to_char(dstart,'YYYY-MM-DD') as dstart,
to_char(dend,'YYYY-MM-DD') as dend,
status,maxusers,contractgroups,usersgroups,apps FROM contracts WHERE id={$_POST['gid']};
EOT;

//    echo $q; echo '<br>';
    $result = @pg_query($cyf, $q);
    if (!$result) $last_error = pg_last_error($cyf);
    else{
        $firows = pg_num_rows($result);
        if($firows >0){
            $fi=getValues($result);
            for ($j=0;$j<$firows;$j++){
                $gid = $fi[$j]['id'];
                $cname = $fi[$j]['cname'];
                $bname = $fi[$j]['bname'];
                $address = $fi[$j]['address'];
                $bid = $fi[$j]['bid'];
                $osec = $fi[$j]['osec'];
                $contact = $fi[$j]['contact'];
                $lang= $fi[$j]['lang'];
                $tel = $fi[$j]['tel'];
                $email = $fi[$j]['email'];
                $url = $fi[$j]['url'];
                $dstart = $fi[$j]['dstart'];
                $dend = $fi[$j]['dend'];
                $manager = $fi[$j]['manager'];
                $status = $fi[$j]['status'];
                $maxusers = $fi[$j]['maxusers'];
                $contractgroups = $fi[$j]['contractgroups'];
                $usersgroups = $fi[$j]['usersgroups'];
                $apps = $fi[$j]['apps'];
//                $contractgroups = ltrim(rtrim($fi[$j]['contractgroups'],'}'),'{');
//                $usersgroups = ltrim(rtrim($fi[$j]['usersgroups'],'}'),'{');
//                $apps = ltrim(rtrim($fi[$j]['apps'],'}'),'{');
            }
        }
    }
}

$tcurr=$maxcount-$ocurcount-$curcount;

echo "<input type='hidden' id='qusers' name='qusers' value='{$tcurr}'>";
echo "<input type='hidden' id='currid' name='gid' value='{$gid}'>";
//echo "<center>";

echo "<table><caption style='text-align:center;'>Contract/Filters</caption>";

echo "<tr><td>Contract:</td><td><input type='text' id='icname' name='cname' value='{$cname}'></td>";
echo "<td><input type='checkbox' id='ifcname' name='ifcname'";
if(!empty($_POST['ifcname'])) echo ' checked';
echo ">view with contract like this</td></tr>";

echo "<tr><td>Customer:</td><td><input type='text' id='ibname' name='bname' value='{$bname}'></td>";
echo "<td><input type='checkbox' id='ifbname' name='ifbname'";
if(!empty($_POST['ifbname'])) echo ' checked';
echo ">view with customer like this</td></tr>";

echo "<tr><td>Address:</td><td><input type='text' id='iaddress' name='address' value='{$address}'></td>";
echo "<td><input type='checkbox' id='ifaddress' name='ifaddress'";
if(!empty($_POST['ifaddress'])) echo ' checked';
echo ">view with customer like this</td></tr>";

echo "<tr><td>ID:</td><td><input type='text' id='ibid' name='bid' value='{$bid}'></td>";
echo "<td><input type='checkbox' id='ifbid' name='ifbid'";
if(!empty($_POST['ifbid'])) echo ' checked';
echo ">view with ID like this</td></tr>";

echo "<tr><td>Operating&nbsp;sector:</td><td><input type='text' id='iosec' name='osec' value='{$osec}'></td>";
echo "<td><input type='checkbox' id='ifosec' name='ifosec'";
if(!empty($_POST['ifosec'])) echo ' checked';
echo ">view with Operating&nbsp;sector like this</td></tr>";

echo "<tr><td>Contact:</td><td><input type='text' id='icontact' name='contact' value='{$contact}'></td>";
echo "<td><input type='checkbox' id='ifcontact' name='ifcontact'";
if(!empty($_POST['ifcontact'])) echo ' checked';
echo ">view with contact like this</td></tr>";

echo "<tr><td>language:</td><td><input type='text' id='ilang' name='lang' value='{$lang}'></td>";
echo "<td><input type='checkbox' id='iflang' name='iflang'";
if(!empty($_POST['iflang'])) echo ' checked';
echo ">view with language like this</td></tr>";

echo "<tr><td>tel:</td><td><input type='text' id='itel' name='tel' value='{$tel}'></td>";
echo "<td><input type='checkbox' id='iftel' name='iftel'";
if(!empty($_POST['iftel'])) echo ' checked';
echo ">view with tel like this</td></tr>";

echo "<tr><td>url:</td><td><input type='text' id='iurl' name='url' value='{$url}'></td>";
echo "<td><input type='checkbox' id='ifurl' name='ifurl'";
if(!empty($_POST['ifurl'])) echo ' checked';
echo ">view with url like this</td></tr>";

echo "<tr><td>Start&nbsp;date:</td><td><input type='date' id='idstart' name='dstart' value='{$dstart}'></td>";
echo "<td><input type='checkbox' id='ifdstart' name='ifdstart'";
if(!empty($_POST['ifdstart'])) echo ' checked';
echo ">view with start&nbsp;date <= this</td></tr>";

echo "<tr><td>End&nbsp;date:</td><td><input type='date' id='idend' name='dend' value='{$dend}'></td>";
echo "<td><input type='checkbox' id='ifdend' name='ifdend'";
if(!empty($_POST['ifdend'])) echo ' checked';
echo ">view with end&nbsp;date <= this</td></tr>";

echo "<tr><td>Status:</td><td><input type='text' id='istatus' name='status' value='{$status}'></td>";
echo "<td><input type='checkbox' id='ifstatus' name='ifstatus'";
if(!empty($_POST['ifstatus'])) echo ' checked';
echo ">view with status like this</td></tr>";

echo "<tr><td>Max # contract's users:</td><td><input type='text' id='imaxusers' name='maxusers' value='{$maxusers}'></td>";
echo "<td><input type='checkbox' id='ifmaxusers' name='ifmaxusers'";
if(!empty($_POST['ifmaxusers'])) echo ' checked';
echo ">view with max number of users like this</td></tr>";

echo "<tr><td>Manager:</td><td><input type='text' id='imanager' name='manager' value='{$manager}'></td>";
echo "<td><input type='checkbox' id='ifmanager' name='ifmanager'";
if(!empty($_POST['ifmanager'])) echo ' checked';
echo ">view with manager like this</td></tr>";

echo "<tr><td>Contract group:</td><td><input type='text' id='icontractgroups' name='contractgroups' value='{$contractgroups}'></td>";
echo "<td><input type='checkbox' id='ifcontractgroups' name='ifcontractgroups'";
if(!empty($_POST['ifcontractgroups'])) echo ' checked';
echo ">view with contract group like this</td></tr>";

echo "<tr><td>User's group:</td><td><input type='text' id='iusersgroups' name='usersgroups' value='{$usersgroups}'></td>";
echo "<td><input type='checkbox' id='ifusersgroups' name='ifusersgroups'";
if(!empty($_POST['ifusersgroups'])) echo ' checked';
echo ">view with user's group like this</td></tr>";

echo "<tr><td>Apps:</td><td><input type='text' id='iapps' name='apps' value='{$apps}'></td>";
echo "<td><input type='checkbox' id='ifapps' name='ifapps'";
if(!empty($_POST['ifapps'])) echo ' checked';
echo ">view with apps like this</td></tr>";

echo "<tr><td>email:</td><td><input type='text' id='iemail' name='email' value='{$email}'></td>";
echo "<td><input type='checkbox' id='ifemail' name='ifemail'";
if(!empty($_POST['ifemail'])) echo ' checked';
echo ">view with email like this</td></tr>";

echo "<tr><td>password:</td><td><input type='password' id='ipsword' name='psword' value=''></td>";
echo "<td></td></tr>";

/* echo "<tr><td>rungroups:</td><td><textarea id='irungroups' name='rungroups'>"; */
/* echo rtrim( ltrim($rungroups,'{'),'}')."</textarea></td>"; */
/* echo "<td><input type='checkbox' id='ifrungroups' name='ifrungroups'"; */
/* if(!empty($_POST['ifrungroups'])) echo ' checked'; */
/* echo ">view with rungroups contains</td></tr>"; */

echo "</table>";

echo "<center>";
echo <<<EOT
<table><caption>&nbsp;</caption>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="Insert">Add</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete">Delete</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update">Update</td></tr>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="View" checked>View</td>
<td><input type="checkbox" id="ufilters" name="usefilters" value="usefilters"
EOT;
if(!empty($_POST['usefilters'])) echo ' checked';
echo '>use filters</td><td><input type="submit" name="pcmd" value="Submit" style=""></td></tr>';
echo '</table>';

echo '</table>';

$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) > 1){
    echo $stat[1];
}else echo $last_error;

echo <<<EOT
<!--<div><a href="../cis/login.php">Go Back</a></div>-->
<!--<div>&copyCIS Contracts Control Tool</div>-->
</center>
</form>

<form method="post" action="../cis/util_contractslist.php" target="contractslist" id="contractslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fcname" name="fcname">
<input type="hidden" id="fbname" name="fbname">
<input type="hidden" id="faddress" name="faddress">
<input type="hidden" id="fbid" name="fbid">
<input type="hidden" id="fosec" name="fosec">
<input type="hidden" id="fcontact" name="fcontact">
<input type="hidden" id="flang" name="flang">
<input type="hidden" id="ftel" name="ftel">
<input type="hidden" id="femail" name="femail">
<input type="hidden" id="furl" name="furl">
<input type="hidden" id="fdstart" name="fdstart">
<input type="hidden" id="fdend" name="fdend">
<input type="hidden" id="fmanager" name="fmanager">
<input type="hidden" id="fstatus" name="fstatus">
<input type="hidden" id="fcontractgroups" name="fcontractgroups">
<input type="hidden" id="fusersgroups" name="fusersgroups">
<input type="hidden" id="fapps" name="fapps">
</form>
</body>
</html>
EOT;
?>