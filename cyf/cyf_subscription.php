<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';

function printEmpty($opt){
    switch($opt){
    case 'uselect':
        echo "<option value='empty'>empty</option>";
        break;
        
    case 'li':
        echo "<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>";
        echo "empty</strong>";
        echo "</li>";
        echo "<li>";
echo "<a href='../cis/login.php?is_exit=1' rel='external' class='ui-btn ui-btn-icon-right ui-icon-carat-r'>";
        echo "<h3><strong>empty</strong></h3>";
        echo "</a>";
        echo "</li>";
        break;
        
    default:
        echo '[{"id":0}]';
    }
}
function printJson($res){
    if ($res and pg_num_rows($res) >0) {
        $r = array();
        $d='[';
        while (($r=pg_fetch_assoc ( $res ))){
            echo $d.'{';
            $agr= json_encode(explode('~^~',$r['agr'] ));
            $pgr= json_encode(explode('~^~',$r['pgr']));
            $subthem= json_encode(explode('~^~',$r['subthem']));
            echo '"id":"'.$r['id'].'","theme":"'.$r['theme'].'"';
            echo ',"owner":"'.$r['owner'].'"';
            echo ',"subthemes":'.$subthem;
            echo ',"allowgroups":'.$agr;
            echo ',"publishgroups":'.$pgr;
            echo "}";
            $d=',';
        }
        echo ']';
    }else{
        echo '[{"id":0}]';
    }
}

$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();
if (empty($_REQUEST['opt'])) $opt = 'json';
else $opt = $_REQUEST['opt'];

$cyf = cyfConnect();
//$self=htmlspecialchars($_SERVER['PHP_SELF']);
//print_r($param);echo "<br>";
    switch($_REQUEST['eaction']){
    case 'Dsubscription'://delete //1
        if(!empty($_REQUEST['eid']) AND $_REQUEST['eid'] >0){
            $q ="DELETE FROM subscriptions";
            $q.=" WHERE id=".pg_escape_string($_REQUEST['eid']);
            $q.=" AND (";
            $q.=" owner='{$param['email']}'";
            $q.=" OR ARRAY[owner] && '{$param['ownergroups']}'";
//            $q.=" OR ARRAY[owner] && '{$param['membergroups']}'";
            $q.=");";
            $res = pg_query($cyf, $q);
        }
        break;
    case 'Csubscription'://update  //1
        if(!empty($_REQUEST['eid']) AND $_REQUEST['eid'] >0
        AND !empty($_REQUEST['owner'])
        AND !empty($_REQUEST['themes'])){
            $q ="UPDATE subscriptions SET";
            $q.=" owner='".pg_escape_string($_REQUEST['owner'])."'";
            $q.=" ,theme='".pg_escape_string($_REQUEST['theme'])."'";
            if(!empty($_REQUEST['subthemes']))
                $q.=" ,subthemes='".pg_escape_string($_REQUEST['subthemes'])."'";
            if(!empty($_REQUEST['allowgroups']))
                $q.=" ,allowgroups='".pg_escape_string($_REQUEST['allowgroups'])."'";
            if(!empty($_REQUEST['publishgroups']))
                $q.=" ,publishgroups='".pg_escape_string($_REQUEST['publishgroups'])."'";
            $q.=" WHERE id=".pg_escape_string($_REQUEST['eid']);
            $q.=" AND (";
            $q.=" owner='{$param['email']}'";
            $q.=" OR ARRAY[owner] && '{$param['ownergroups']}'";
//            $q.=" OR ARRAY[owner] && '{$param['membergroups']}'";
            $q.=");";
            $res = pg_query($cyf, $q);
        }
        break;

    case 'Nsubscription'://insert //1
        if(!empty($_REQUEST['owner']) AND !empty($_REQUEST['themes'])) {
            $i =array();
            $i['owner'] = "'".pg_escape_string($_REQUEST['owner'])."'";
            $i['theme'] = "'".pg_escape_string($_REQUEST['theme'])."'";
            if(!empty($_REQUEST['subthemes']))
                $i['subthemes'] = "'".pg_escape_string($_REQUEST['subthemes'])."'";
            if(!empty($_REQUEST['allowgroups']))
                $i['allowgroups'] = "'".pg_escape_string($_REQUEST['allowgroups'])."'";
            if(!empty($_REQUEST['publishgroups']))
                $i['publishgroups'] = "'".pg_escape_string($_REQUEST['publishgroups'])."'";
            $n = array();
            $v =array();
            foreach($i as $k => $s){
                $n[] = $k;
                $v[] = $s; 
            }
            $q = "INSERT INTO subscriptions (".implode(',', $n).") VALUES (".implode(',', $v).");";
        }
        break;
     
        case 'Gsubscription':  //?????????????????????????????????????????????????????????
//select
        $q ="SELECT theme FROM subscriptions";
        $q.=" WHERE owner='{$param['email']}'";
        $q.=" OR ARRAY[owner] && '{$param['membergroups']}'";
        $q.=" OR publishgroups && '{{$param['email']}}'";
        $q.=" OR publishgroups && '{$param['membergroups']}'";
        $q.=";";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            switch($opt){
            case 'uselect':
//                echo "<option data-placeholder="true">Choose subscriptions</option>";
                while (($r=pg_fetch_assoc ( $res )))
                    echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
                break;
            case 'li':
                 while (($r=pg_fetch_assoc ( $res ))) echo "<li>{$r['theme']}</li>";
               break;
            default:
                $del = "[";
                while (($r=pg_fetch_assoc ( $res ))){
                    echo $del;
                    echo $r['theme'];
                    $del = ",";
                }
                echo ']';
            }
        }else printEmpty($opt);
        exit();
        break;
        
        case 'Jsubscription':  //1
            if(!empty($_REQUEST['eid']) AND $_REQUEST['eid'] >0){
                $q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem";
                $q.= ",array_to_string(allowgroups,'~^~') as agr";
                $q.= ",array_to_string(publishgroups,'~^~') as pgr FROM subscriptions";
                $q.= "  WHERE id=".pg_escape_string($_REQUEST['eid']);
                $q.=" AND (";
                $q.=" owner='{$param['email']}'";
                $q.=" OR ARRAY[owner] && '{$param['ownergroups']}'";
//            $q.=" OR ARRAY[owner] && '{$param['membergroups']}'";
                $q.=");";
                $res = pg_query($cyf, $q);
				
                printJson($res);
            }else printEmpty($opt);
                exit();
                break;

        case 'Asubscriptions': //1
        default:
        }
    
//select
$q = "SELECT id,owner,theme,array_to_string(subthemes,'~^~') as subthem,array_to_string(allowgroups,'~^~') as agr,array_to_string(publishgroups,'~^~') as pgr FROM subscriptions";
$q.=" WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}'";
$q.=" OR allowgroups && '{$param['membergroups']}'";
$q.=" OR allowgroups && '{$param['ownergroups']}'";    
$q.=" ORDER BY theme";
//$q .= " OFFSET $1 LIMIT $2 
$q .= ";";

$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
    switch($opt){
    case 'uselect':
        while (($r=pg_fetch_assoc ( $res ))) echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
        exit();
        break;
        
    case 'li':
        while (($r=pg_fetch_assoc ( $res ))){
            echo "<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'><strong>";
            echo $r['theme']."</strong> ({$r['owner']})</li>";
            echo "<li><a href='#subscription' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Said'";
            echo " data-user-id='{$r['id']}'";
            echo ">";
//            echo "<input type='hidden' name='eid' id='eid' value='{$r['id']}'>"; //????
            echo "<h3><strong>{$r['theme']}</strong></h3>";
            echo "<p><strong>Cross reference themes: {$r['subthemes']}</strong></p>";
            echo "<p><strong>Publishing groups: {$r['publishgroups']}</strong></p>";
            echo "<p><strong>Reading only groups: {$r['allowgroups']}</strong></p>";
            echo "</a>";
			echo "</li>";
        }
        break;

    default:
        printJson($res);
    }
}else printEmpty($opt);
?>