<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services - Subscriptions' : $_REQUEST['provider'];
$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
$(document).bind('pageinit',
//$(document).ready(
 function()
  {
   var p= $.param( {opt: 'uselect',eaction:'Vsubscription'});
   $("select#Ssubthemes").load("../cyf/cyf_subscription.php", p);
   p= $.param( {opt: 'li',eaction:'Vsubscription'});
   $("#subscriptions").load("../cyf/cyf_subscription.php", p);
});
$( document ).on( 'vclick', "a#Said", function() {
    $.mobile.changePage('#subscription');
    $('#Snid').val($(this).data('userId'));
    if($(this).data('userId')!=0){
            var p= $.param( {id: $(this).data('userId'),eaction: 'Jsubscription'});
     $.ajax({url: "../cyf/cyf_subscription.php",data: p,
            success: function(msg){
			if(msg.length>0){
//                alert(msg);
                var pg =   JSON.parse(msg);
                $('#Snid').val(pg.id);
                $("select#Sowner").val(pg.owner);
                $('select#Sowner').selectmenu().selectmenu('refresh',true);  
                $("select#Sallowgroups").val(pg.allowgroups);
                $('select#Sallowgroups').selectmenu().selectmenu('refresh',true);  
                $('select#Spublishgroups').val(pg.publishgroups);
                $('select#Spublishgroups').selectmenu('refresh',true);
                $("select#Ssubthemes").val(pg.subthemes);
                $('select#Ssubthemes').selectmenu().selectmenu('refresh',true);  
 
                $('#Stheme').val(pg.theme);
            }
         }
    });
    }
    return false;
});
$( document ).on( "vclick", "a#dsubscription", function() {
    var p= $.param( {
            opt: 'li',
            eaction: 'Dsubscription',
            eid: $('#Snid').val()
    });
    $("#subscriptions").load("../cyf/cyf_subscription.php", p);
    return false;
});
$( document ).on( "vclick", "a#csubscription", function() {
    var p= $.param( {
            opt: 'li',
            eaction: 'Csubscription',
            eid:   $('#Snid').val(),
            owner: $('#Sowner').val(),
            theme: $('#Stheme').val(),
            subthemes: '{'+($("select#Ssubthemes").val()!=null ? $("select#Ssubthemes").val().join() : $('#Ssubthemes').val())+'}',
            allowgroups: '{'+($("select#Sallowgroups").val()!=null ? $("select#Sallowgroups").val().join() : $('#Sallowgroups').val())+'}',
            publishgroups: '{'+($("select#Spublishgroups").val()!=null ? $("select#Spublishgroups").val().join() : $('#Spublishgroups').val())+'}'
    });
    $("#subscriptions").load("../cyf/cyf_subscription.php", p);
    return true;
});
$( document ).on( "vclick", "a#nsubscription", function() {
    var p= $.param( {
            opt: 'li',
            eaction: 'Nsubscription',
            owner: $('#Sowner').val(),
            theme: $('#Stheme').val(),
            subthemes: '{'+($("select#Ssubthemes").val()!=null ? $("select#Ssubthemes").val().join() : $('#Ssubthemes').val())+'}',
            allowgroups: '{'+($("select#Sallowgroups").val()!=null ? $("select#Sallowgroups").val().join() : $('#Sallowgroups').val())+'}',
            publishgroups: '{'+($("select#Spublishgroups").val()!=null ? $("select#Spublishgroups").val().join() : $('#Spublishgroups').val())+'}'
    });
    $("#subscriptions").load("../cyf/cyf_subscription.php", p);
    return true;
});
</script>
</head>
<body >
<form  id="f" method="POST" action="<?php echo $self; ?>">
  <div data-role="page" id="listevent" data-position="fixed">
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='../cis/login.php?is_exit=0' class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external"></a>
<h6 style="font-size:83%;"><a href='<?php echo $self; ?>' rel='external'><?php echo $provider; ?></a></h6>
<a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a>
    </div>

    <div data-role="main" class="ui-content">
        <h3></h3>
        <ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="subscriptions">
          <li>empty</li>
        </ul>

    </div> 
  </div> 

  <div data-role="page" data-dialog="true" data-mini="true" id="subscription">
  <div data-role="header" data-position="fixed">
    <h2>Subscription</h2>
  </div>
    <div data-role="main" class="ui-content" id="msubscription">
	  <ul data-role="listview" id="subscriptionfields">
	  <li >
		<label for="Sowner">Moderator:</label>
		<select name="Sowner" id="Sowner" data-native-menu="false">
<!--         <option data-placeholder="true">Moderator</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Stheme">Theme:</label>
		<textarea name="Stheme" id="Stheme" placeholder="Text ..."></textarea>
        <input type='hidden' name='Snid' id='Snid'>
	  </li>
	  <li >
		<label for="Ssubthemes">Cross reference themes:</label>
		<select name="Ssubthemes[]" id="Ssubthemes" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Themes</option>
		</select>
	  </li>
	  <li >
		<label for="Sallowgroups">Reading only groups:</label>
		<select name="Sallowgroups[]" id="Sallowgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Reading only groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Spublishgroups">Publishing groups:</label>
		<select name="Spublishgroups[]" id="Spublishgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Publishing groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  </ul>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dsubscription">Delete subscription</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="csubscription">Change subscription</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nsubscription">Save new subscription</a>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
    </div>
  </div>

<div data-role="page" data-dialog="true" data-mini="true" id="User">
  <div data-role="header" data-position="fixed">
    <h2>User</h2>
  </div>
  <div data-role="main" class="ui-content">
    <ul data-role="listview">
    <li>User: <?php echo $param['email']; ?></li>
    <li>Groups member:<br><?php echo $param['membergroups']; ?></li>
    <li>Groups owner:<br><?php echo $param['ownergroups']; ?></li>
    </ul>
	
  </div>
</div>
</form>
</body>
</html>
