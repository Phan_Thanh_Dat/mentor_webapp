<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cyf/cyf_contracts.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf=cyfConnect();
echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<style>
td {vertical-align:top;margin-left: 6px;text-align:left;}
th {margin-left: 6px;text-align:center;font-size:1.2em;}
</style>
<title>Contracts Control Tool</title>
<script>
window.onload = function(){}
</script>
</head><body>
<center><br>
EOT;

//echo "post: ";print_r($_REQUEST);echo "<br>";
$fmanager = ltrim(rtrim($_REQUEST['rgroups'],'"}'),'{"'); //$param['email']
echo <<<EOT
<table border=1>
<tr><th>Contracts<br>
<i style="font-size:0.9em;text-align:center;">(Customer, Contact, Manager)</i></th><th style='text-align:center;'>{$fmanager} Contracts Control Tool<br>
 <a href="../cis/login.php">Exit</a><br>manager: {$param['email']}</th>
<th>{$fmanager} Groups<br>(Group / Owner)</th>
</tr>
<tr>
<td>
<iframe name="contractslist" width="500" height="680" src="../cyf/cyf_contractslist.php?
EOT;
echo "usefilters=usefilters"; 
echo "&fmanager={$fmanager}"; 
echo <<<EOT
"></iframe></td>
<td>
<iframe name="contractsform" width="800" height="680" src="../cyf/cyf_contractsform.php?
EOT;
echo "usefilters=usefilters"; 
echo "&fmanager={$fmanager}"; 
echo <<<EOT
"></iframe></td>
<td>
<iframe name="groupslist" width="300" height="680" src="../cyf/cyf_groupslist.php?
EOT;
echo "usefilters=usefilters"; 
echo "&fmanager={$fmanager}"; 
echo <<<EOT
"></iframe></td>
</tr>
</table>
<!-- border:none; -->
</center>
</body></html>
EOT;
?>