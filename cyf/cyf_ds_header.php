<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//cyf_ds_header.php

$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$page = 'I own Turku';
$provider = 'Turku Univercity';
echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYF Digital Service</title>
<style>
td {
margin-left: 6px;
padding-left:6px;
text-align:center;
}
</style>
<script>
function setParam(e){
 document.getElementById('hpage').value=e.dataset.page;//userPage;
 document.getElementById('mpage').value=e.dataset.page;//userPage;
// document.getElementById('fpage').value=e.dataset.page;//userPage;
 document.getElementById('cyfheader').submit();
 document.getElementById('cyfmain').submit();
// document.getElementById('cyffooter').submit();
}
</script>
</head>
<body>
EOT;

echo '<form id="cyfheader" target="cyf_header" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf = cyfConnect();

if(!isset($_POST['page']) or ($_POST['page']=='' )) $page = 'Main';
else $page = $_POST['page'];
echo "<input type='hidden' id='hpage' name='page' value='{$page}'>";

echo <<<EOT
  <div class="container" style="text-align:center;">
    <span style="opacity:0.5;text-align:center;font-style: italic;font-size: 0.875em;">CYF Digital Service</span><br>
	<span style="text-align:center;font-style: italic;font-size: 1.275em;">{$provider}</span>
    <div class="well-sm">
      <table style="width:100%;"><tr>
        <td style="width:10%;" onclick="setParam(this)" data-user-app="list" data-page="Main">
          <a href="#">
          <span class="glyphicon glyphicon-home"></span></a></td>
        <td style="width:80%;">{$post['page']}</td>
        <td style="width:10%;" onclick="setParam(this)" data-user-app="list" data-page="Settings">
           <a href="#">
           <span class="glyphicon glyphicon-wrench"></span></a>
        </td></tr></table>
    </div>
  </div>
</form>
<form method="post" action="../cyf/cyf_ds_main.php" target="cyf_main" id="cyfmain">
<input type="hidden" id="mpage" name="page" value="{$page}">
</form>
<!--
<form method="post" action="../cyf/cyf_ds_footer.php" target="cyf_footer" id="cyffooter">
<input type="hidden" id="fpage" name="page" value="{$page}">
</form>
-->
</body>
</html>
EOT;

?>