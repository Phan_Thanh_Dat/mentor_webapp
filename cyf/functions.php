<?php
function showFooter(){
echo <<<EOT
</form>
</body>
</html>
EOT;
}
function showDOCTYPE($title){
echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>($title)</title>
<style>
td {
margin-left: 6px;
padding-left:6px;
text-align:center;
}
</style>
EOT;
}
function showDETAIL($error){
    $stat = explode('DETAIL:',$error);
    echo "<div>";
    if(count($stat) == 2){
        echo '<span class="glyphicon glyphicon-alert"></span>&nbsp;';
        echo $stat[1];
    }
//    else echo $error;
    echo "</div>";
}

function submit0($source,$page){
    echo <<<EOT
<script>
function setParam(e){
 document.getElementById('i{$source}page').value=e;
 document.getElementById('f{$source}').submit();
}
</script>
</head>
<body>
<form id='f{$source}' method='POST' action='
EOT;
    echo htmlspecialchars($_SERVER['PHP_SELF'])."'>";
    echo "<input type='hidden' id='i{$source}page' name='page' value='Main'>";
}

function submit1($source,$target,$action){
    echo <<<EOT
<script>
function loadPage(){
 document.getElementById('i{$target}page').value=document.getElementById('i{$source}page').value;
 document.getElementById('f{$target}').submit();
}
function setItem(e){
// document.getElementById('i{$target}page').value=e.dataset.page;
// document.getElementById('i{$source}page').value=e.dataset.page;
 document.getElementById('i{$target}page').value=document.getElementById('category').value;
 document.getElementById('i{$source}page').value=document.getElementById('category').value;
 document.getElementById('argid').value=e.dataset.id;
 document.getElementById('app').value=e.dataset.app;
 document.getElementById('arg').innerHTML=e.dataset.arg;
}
</script>
</head>
<body onload="loadPage();">
<form method='post' action='{$action}' target='{$target}' id='f{$target}'>
<input type='hidden' id='i{$target}page' name='page' value=''>
</form>
<form id='f{$source}' target='{$source}' method='POST' action='
EOT;
    echo htmlspecialchars($_SERVER['PHP_SELF'])."'>";
    echo "<input type='hidden' id='i{$source}page' name='page' value='Main'>";
}

function submit2($source,$target,$action,$page){
    echo <<<EOT
<script>
function setParam(e){
 document.getElementById('i{$source}page').value=e;
 document.getElementById('i{$target}page').value=e;
 document.getElementById('f{$source}').submit();
 document.getElementById('f{$target}').submit();
}
</script>
</head>
<body>
<form method='post' action='{$action}' target='{$target}' id='f{$target}'>
<input type='hidden' id='i{$target}page' name='page' value='{$page}'>
</form>
<form id='f{$source}' target='{$source}' method='POST' action='
EOT;
    echo htmlspecialchars($_SERVER['PHP_SELF'])."'>";
    echo "<input type='hidden' id='i{$source}page' name='page' value='Main'>";
}

function submin3($source,$target,$action,$page,$debug,$debugaction){
    echo <<<EOT
<script>
function setParam(e){
 document.getElementById('i{$source}page').value=e;
 document.getElementById('f{$source}').submit();
 document.getElementById('i{$target}page').value=e;
 document.getElementById('f{$target}').submit();
 document.getElementById('i{$debug}page').value=e;
 document.getElementById('f{$debug}').submit();
}
</script>
</head>
<body>
<form method='post' action='{$debugaction}' target='{$debug}' id='f{$debug}'>
<input type='hidden' id='i{$debug}page' name='page' value='{$page}'>
</form>
<form method='post' action='{$action}' target='{$target}' id='f{$target}'>
<input type='hidden' id='i{$target}page' name='page' value='{$page}'>
</form>
<form id='f{$source}' target='{$source}' method='POST' action='
EOT;
    echo htmlspecialchars($_SERVER['PHP_SELF'])."'>";
    echo "<input type='hidden' id='i{$source}page' name='page' value='Main'>";
}

function showHeader($provider,$page){
    echo <<<EOT
<div class="container" style="text-align:center;">
  <span style="opacity:0.5;text-align:center;font-style: italic;font-size: 0.875em;">
     CYF Digital Service
  </span><br>
  <span style="text-align:center;font-style: italic;font-size: 1.275em;">{$provider}</span>
  <div class="well-sm">
    <table style="width:100%;"><tr>
      <td style="width:10%;" onclick="setParam('Main')">
        <a href="#"> <span class="glyphicon glyphicon-home"></span></a></td>
      <td style="width:80%;">{$page}</td>
      <td style="width:10%;" onclick="setParam('Settings')">
         <a href="#"><span class="glyphicon glyphicon-wrench"></span></a></td>
    </tr></table>
  </div>
</div>
EOT;
    
}
function printList($item){
echo <<<EOT
<a href="#" class="list-group-item" onclick="setParam('{$item}')">{$item}</a>
EOT;
}
 function printTag($r){
   $tag = $r['tag']; 
    $class = isset($r['class']) ? "class='{$r['class']}'" : '';
    $id = isset($r['id']) ? "id='{$r['id']}'" : '';
    $nid = isset($r['id']) ? $r['id'] : '';
    $style = isset($r['style']) ? "style='{$r['style']}'" : '';
    $label = isset($r['label']) ? $r['label'] : '';
    $url = isset($r['url']) ? $r['url'] : '#';
    $target = isset($r['target']) ?  "target='{$r['target']}'" : '';
    $name = isset($r['target']) ? "name='{$r['target']}'" : '';
    $width = isset($r['width']) ? "width='{$r['width']}'" : 'width="400"';
    $height = isset($r['height']) ? "height='{$r['height']}'" : 'height="300"';
    if($tag == 'iframe'){
        echo "<iframe {$class} {$id} {$style} {$width} {$height} {$name} src='{$url}'></iframe>";
    }else{
        if($tag == 'a'){
        echo "<a {$class} {$id} {$style} {$target} href='{$url}'>{$label}</a>";
        }else{
           echo "<br>unknown tag:";print_r($r);echo "<br>"; 
        }
    }
}
function evaluateJson($json){
    $r = json_decode($json,true);
    if(is_array($r)){
        if(isset($r['setting']) and isset($r['setting']['lang'])){
            $_SESSION['setting']['lang'] = $r['setting']['lang'];
        }else{
            if(isset($r['tag'])){
                printTag($r);
            }else{
                if(isset($r['linklist'])){
                    foreach ($r['linklist'] as $k => $v){
                        printTag($v);echo "<br>";
                    }
                }else{
                    echo "<br>unknown json: ";print_r($r);echo "<br>";
                }
            }
        }
    }else{
        echo "<br>Check json: ";print_r($r);echo "<br>";
    }
}
    
?>