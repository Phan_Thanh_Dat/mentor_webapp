<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cyf/cyf_contractform.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

function checkMembergroups($ogroups,$mgroups){
    $m = explode(',', ltrim(rtrim($mgroups,'}" '),'{" '));
    $p = explode(',', ltrim(rtrim($ogroups,'}" '),'{" '));
    $a = array();
    foreach($m as $c){
        foreach($p as $s){
            $c= ltrim(rtrim($c,' '),' ');
            $s= ltrim(rtrim($s,' '),' ');
            if($c === $s) $a[] = $c;
            else echo "'$c' != '$s'<br>";
        }
    }
    return implode(',', $a);
}
function setContractGroup($conn, $cgroups, $manager){
    if(empty($cgroups) or empty($manager)) return false;
    $q="select count(*) as c  from groups where gname='{$cgroups}' and owner!='{$manager}';";
//echo "<hr>";print_r($q);echo "<hr>";
    $res=pg_query($conn,$q);
    $arr = pg_fetch_array($res, 0, PGSQL_NUM);
//    echo "<hr>";print_r($arr);echo "<hr>";
    if($arr[0] !=0) return false;
    $q="insert into groups(gname,owner) values ('{$cgroups}','{$manager}');";
//echo "<hr>";print_r($q);echo "<hr>";
    pg_query($conn,$q);
    return true;
}
function setOwnergroups($conn, $ogroups, $cgroups){
    $o = explode(',', $ogroups);
    $ret=array();
    if(!empty($o) and !empty($cgroups)){
        $w=array();
        $w['owner']= "'".$cgroups."'";
        $res = pgDelete($conn, 'groups', $w);
        foreach($o as $c){
            $s=array();
            $s['gname']= "'".$c."'";
            $s['owner']= "'".$cgroups."'";
//            echo "setOwnergroups<hr>";print_r($s);
            $res = pgInsert($conn, 'groups', $s);
            if($res) $ret[]=$c;
        }
    }
    return implode(',', $ret);
}
/* function setOwnergroups($conn, $email, $ogroups){ */
/*     $m = explode(',', $ogroups); */
/*     $ret=array(); */
/*     if(!empty($m) and !empty($email)){ */
/*         $w=array(); */
/*         $w['owner']= "'".$email."'"; */
/*         $res = pgDelete($conn, 'groups', $w); */
/*         foreach($m as $c){ */
/*             $s=array(); */
/*             $s['gname']= "'".$c."'"; */
/*             $s['owner']= "'".$email."'"; */
/*             $res = pgInsert($conn, 'groups', $s); */
/*             if($res) $ret[]=$c; */
/*         } */
/*     } */
/*     return implode(',', $ret); */
/* } */

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Groups List</title>
<style>
label {padding-left:6px;}
input[type=checkbox] {margin-right:6px;}
input[type=radio] {margin-right:6px;}
input[type=text] {width:300px;}
textarea {width:300px;}
select {width:300px;}
legend {font-size:1.2em;}
sup {color:red;}
td {
margin-left: 6px;
padding-left:6px;
text-align:left;
}
</style>
<script>
function setParam(){
document.getElementById('frame').value='contractsform';
document.getElementById('action').value='../cyf/cyf_contractsform.php';
document.getElementById('usefilters').value='usefilters';
document.getElementById('fmanager').value=document.getElementById('imanager').value;
if(document.getElementById('ifcname').checked)
document.getElementById('fcname').value=document.getElementById('icname').value;
if(document.getElementById('ifbname').checked)
document.getElementById('fbname').value=document.getElementById('ibname').value;
if(document.getElementById('ifaddress').checked)
 document.getElementById('faddress').value=document.getElementById('iaddress').value;
if(document.getElementById('ifbid').checked)
document.getElementById('fbid').value=document.getElementById('ibid').value;
if(document.getElementById('ifosec').checked)
document.getElementById('fosec').value=document.getElementById('iosec').value;
if(document.getElementById('ifcontact').checked)
document.getElementById('fcontact').value=document.getElementById('icontact').value;
if(document.getElementById('iflang').checked)
document.getElementById('flang').value=document.getElementById('ilang').value;
if(document.getElementById('iftel').checked)
document.getElementById('ftel').value=document.getElementById('itel').value;
if(document.getElementById('ifemail').checked)
document.getElementById('femail').value=document.getElementById('iemail').value;
if(document.getElementById('ifurl').checked)
document.getElementById('furl').value=document.getElementById('iurl').value;
if(document.getElementById('ifdstart').checked)
document.getElementById('fdstart').value=document.getElementById('idstart').value;
if(document.getElementById('ifdend').checked)
document.getElementById('fdend').value=document.getElementById('idend').value;
if(document.getElementById('ifstatus').checked) 
document.getElementById('fstatus').value=document.getElementById('istatus').value;
//if(document.getElementById('ifapps').checked)
//document.getElementById('fapps').value=document.getElementById('iapps').value;
document.getElementById('contractslist').submit();

	var f = document.createElement("form");
	f.setAttribute('method',"post");
	f.setAttribute('target',"groupslist");
	f.setAttribute('action',"../cyf/cyf_groupslist.php");

var i = document.createElement("input");
	i.setAttribute('type',"hidden");
	i.setAttribute('name',"fmanager");
    i.setAttribute('value','{$_REQUEST['fmanager']}');
	f.appendChild(i);

//var j = document.createElement("input");
//	j.setAttribute('type',"hidden");
//	j.setAttribute('name',"fltrs");
//  j.setAttribute('value','on');
//	f.appendChild(j);

	document.getElementsByTagName('body')[0].appendChild(f);
	f.submit();

}
</script>
</head>
<body onload="setParam();">
EOT;


error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf=cyfConnect();
//echo "post: ";print_r($_POST);echo "<br>";

$fmanager = $_REQUEST['fmanager'];
$res = pg_query($conn, "select string_agg(gname,',') from groups where owner='{$fmanager}';");
$arr = pg_fetch_array($res, 0, PGSQL_NUM);
$agroups = implode(',',$arr);

$gid = 0;
if(!empty($_POST['gid'])) $gid = $_POST['gid'];
$cname = $_POST['cname'];
$bname = $_POST['bname'];
$address = $_POST['address'];
$bid = $_POST['bid'];
$osec = $_POST['osec'];
$contact = $_POST['contact'];
$lang= $_POST['lang'];
$tel = $_POST['tel'];
$email = $_POST['email'];
$url = $_POST['url'];
$dstart = $_POST['dstart'];
$dend = $_POST['dend'];
$manager = $fmanager;//$_POST['manager'];
$status = $_POST['status'];
$maxusers = $_POST['maxusers'];
$contractgroups = $_POST['contractgroups'];
$usersgroups = $_POST['usersgroups'];
$apps = "'{".$_POST['apps']."}'";

echo '<form id="contractsform" target="contractsform" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';
echo "<input type='hidden' id='imanager' name='fmanager' value='{$fmanager}'>";


if(($_POST['tcmd']==="Delete") or ($_POST['tcmd']==="Update") or ($_POST['tcmd']==="Insert")){
    $c=array();
    $c['cname'] = "'".$_POST['cname']."'";
    $c['bname'] = "'".$_POST['bname']."'";
    $c['address'] = "'".$_POST['address']."'";
    $c['bid'] = "'".$_POST['bid']."'";
    $c['osec'] = "'".$_POST['osec']."'";
    $c['contact'] = "'".$_POST['contact']."'";
    $c['lang'] = "'".$_POST['lang']."'";
    $c['tel'] = "'".$_POST['tel']."'";
    $c['email'] = "'".$_POST['email']."'";
    $c['url'] = "'".$_POST['url']."'";
    $c['dstart'] = "'".$_POST['dstart']."'";
    $c['dend'] = "'".$_POST['dend']."'";
    $c['status'] = "'".$_POST['status']."'";
    $c['manager'] = "'".$_POST['manager']."'";
    $c['maxusers'] = $_POST['maxusers'];
    $c['contractgroups'] = "'".$_POST['contractgroups']."'";
    $c['usersgroups'] = "'".$_POST['usersgroups']."'";
    $c['apps'] = "'{".$_POST['apps']."}'";
    $c['manager'] = "'".$fmanager."'";


/*     if($_POST['tcmd']==="Delete"){ */
/*         $c['id'] = "'".$_POST['gid']."'"; */
/* //        echo "contracts:";print_r($c);echo "<hr>"; */
/*         if (pgDelete($cyf, 'contracts', $c)){ */
/*             $l=array(); */
/*             $l['email']= "'".$_POST['email']."'"; */
/* //            echo "logins:";print_r($l);echo "<hr>"; */
/*             if (pgDelete($conn, 'logins', $l)){ */
/*                 $g=array(); */
/*                 $g['owner']= "'".ltrim(rtrim($_POST['contractgroups'],'}" '),'{" ')."'"; */
/* //                echo "groups:";print_r($g);echo "<hr>"; */
/*                 if (pgDelete($conn, 'groups', $g)){ */
/*                     $g=array(); */
/*                     $g['gname']= "'".ltrim(rtrim($_POST['contractgroups'],'}" '),'{" ')."'"; */
/* //                    echo "groups:";print_r($g);echo "<hr>"; */
/*                     if (pgDelete($conn, 'groups', $g)){ */
/*                         $u=array(); */
/*                         $u['manager']= "'".$_POST['contractgroups']."'"; */
/* //                        echo "users:";print_r($u);echo "<hr>"; */
/*                         if (pgDelete($cyf, 'users', $u)){ */
/*                             unset($_POST['email']); */
/*                             unset($_POST['gid']); */
/*                         } */
/*                     } */
/*                 } */
/*             } */
/*         } */
    if($_POST['tcmd']==="Delete" and !empty($_POST['gid'])){
        $c['id'] = "'".$_POST['gid']."'";
        $q="DELETE FROM contracts WHERE id={$_POST['gid']};";
//      echo "contracts:";print_r($q);echo "<hr>";
        pg_query($cyf, $q);
        $q="DELETE FROM logins WHERE email='{$_POST['email']}';";
//      echo "logins:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        $q="DELETE FROM groups WHERE ARRAY[gname] && '{{$_POST['usersgroups']}}' OR  ARRAY[owner] && '{{$_POST['usersgroups']}}';";
//      echo "groups:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        $q="DELETE FROM groups WHERE ARRAY[gname] && '{{$_POST['contractgroups']}}' OR  ARRAY[owner] && '{{$_POST['contractgroups']}}';";
//      echo "groups:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        $q="DELETE FROM users WHERE ARRAY[manager] && '{{$_POST['contractgroups']}}';";
//      echo "users:";print_r($q);echo "<hr>";
        pg_query($conn, $q);
        unset($_POST['email']);
        unset($_POST['gid']);

    }else{
        $c['maxusers'] = $_POST['maxusers'];
        /* $c['contractgroups'] = ltrim(rtrim($_POST['contractgroups'],'}" '),'{" '); */
        /* $c['usersgroups'] = ltrim(rtrim($_POST['usersgroups'],'}" '),'{" '); */
        /* if(setContractGroup($conn, $c['contractgroups'], $fmanager)){ */
        /*     $c['apps'] = "'{".$_POST['apps']."}'"; */
        /*     $l=array(); */
        /*     if(!empty($_POST['psword'])) $l['psword']= "'".$_POST['psword']."'"; */
        /*     if(!empty($_POST['contact'])) $l['name']= "'".$_POST['contact']."'"; */
        /*     $l['membergroups']= $c['contractgroups']; */
        /*     $l['ownergroups']= "'{".setOwnergroups($conn, $c['usersgroups'], $l['membergroups'])."}'"; */
        /*     $l['membergroups']= "'{".$l['membergroups']."}'"; */
        /*     $c['usersgroups'] = $l['ownergroups']; */
        /*     $c['contractgroups']= "'{".$c['contractgroups']."}'"; */
        /*     if($_POST['tcmd']==="Update"){ */
        /*         $cond=array(); */
        /*         $cond['email']="'".$_POST['email']."'"; */
        /*         if (pgUpdate($conn, 'logins', $l, $cond)){ */
        /*             $cond=array(); */
        /*             $cond['id']="'".$_POST['gid']."'"; */
        /*             $cond['email']="'".$_POST['email']."'"; */
        /*             if (pgUpdate($cyf, 'contracts', $c, $cond)) unset($_POST['gid']); */
        /*         } */
        /*     } */
        /*     if($_POST['tcmd']==="Insert"){ */
        /*         if($_POST['qusers'] >0) */
        /*             if(!empty($_POST['email'])){ */
        /*                 $l['email']= "'".$_POST['email']."'"; */
        /*                 if (pgInsert($conn, 'logins', $l)){ */
        /*                     $c['email'] = "'".$_POST['email']."'"; */
        /*                     if (pgInsert($cyf, 'contracts', $c)) unset($_POST['gid']); */
        /*                 } */
        /*             } */
        /*     } */
        $cgroups = ltrim(rtrim($_POST['contractgroups'],'" }'),'" {');
        $ugroups = ltrim(rtrim($_POST['usersgroups'],'" }'),'" {');
        $q="SELECT count(*) as c  FROM groups WHERE gname='{$cgroups}' AND owner!={$c['manager']};";
//echo "<hr>";print_r($q);echo "<hr>";
        $res=pg_query($conn,$q);
        $arr = pg_fetch_assoc($res, 0);
//echo "<hr>";print_r($arr);echo "<hr>";
        if($arr['c'] ==0) {
            $q="DELETE FROM groups WHERE gname='{$cgroups}' AND owner={$c['manager']};";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            $q="INSERT INTO groups(gname,owner) VALUES ('{$cgroups}',{$c['manager']});";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            $q="DELETE FROM groups WHERE owner={$cgroups};";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            $q="INSERT INTO groups(gname,owner) VALUES ('{$ugroups}','{$cgroups}');";
//echo "<hr>";print_r($q);echo "<hr>";
            pg_query($conn,$q);
            if($_POST['tcmd']==="Update" and !empty($_POST['gid'])){
                $p = '';
                if(!empty($_POST['psword'])) $p= "psword='".$_POST['psword']."',";
                $q="UPDATE logins SET email={$c['email']}, {$p} name={$c['contact']}, ownergroups={$c['usersgroups']}, membergroups={$c['contractgroups']} WHERE email={$c['email']};";
//echo "<hr>";print_r($q);echo "<hr>";
                pg_query($conn,$q);
                $cond=array();
                $cond['id']=$_POST['gid'];
                pgUpdate($cyf, 'contracts', $c, $cond);
                unset($_POST['gid']);
            }
            if($_POST['tcmd']==="Insert"){
//                if($_POST['qusers'] >0)
                    if(!empty($_POST['email']) and !empty($_POST['psword'])){
                        $q="INSERT INTO logins(email,psword,name,ownergroups,membergroups) VALUES ({$c['email']},'{$_POST['psword']}',{$c['contact']},{$c['usersgroups']},{$c['contractgroups']});"; 
//echo "<hr>";print_r($c);echo "<hr>";
                        pg_query($conn,$q);
                        pgInsert($cyf, 'contracts', $c);
                        unset($_POST['gid']);
                
                    }
            }
        }
    }
}

if(!empty($_REQUEST['ifcname']) or !empty($_REQUEST['ifbname']) or !empty($_REQUEST['ifaddress']) or
   !empty($_REQUEST['ifbid']) or !empty($_REQUEST['ifosec']) or !empty($_REQUEST['ifcontact']) or
   !empty($_REQUEST['iflang']) or !empty($_REQUEST['iftel']) or !empty($_REQUEST['ifemail']) or
   !empty($_REQUEST['ifurl']) or !empty($_REQUEST['ifdstart']) or !empty($_REQUEST['ifdend']) or
   !empty($_REQUEST['ifstatus']) or !empty($_POST['ifusersgroups']) or !empty($_POST['ifcontractgroups']) or
   !empty($_POST['ifapps'])){
    $c['cname'] =$_POST['cname'];
    $c['bname'] =$_POST['bname'];
    $c['address'] =$_POST['address'];
    $c['bid'] =$_POST['bid'];
    $c['osec'] =$_POST['osec'];
    $c['contact'] =$_POST['contact'];
    $c['lang'] =$_POST['lang'];
    $c['tel'] =$_POST['tel'];
    $c['email'] =$_POST['email'];
    $c['url'] =$_POST['url'];
    $c['dstart'] =$_POST['dstart'];
    $c['dend'] =$_POST['dend'];
    $c['status'] =$_POST['status'];
    $c['contractgroups'] =$_POST['contractgroups'];
    $c['usersgroups'] =$_POST['usersgroups'];
}else{
    if(!empty($_POST['email']))
        $res = pg_query($cyf, "select * from contracts where manager='{$fmanager}' AND email='{$_POST['email']}';");
    else if(!empty($_POST['gid']))
        $res = pg_query($cyf, "select * from contracts where manager='{$fmanager}' AND id='{$_POST['gid']}';");
    else
        $res = pg_query($cyf, "select * from contracts where manager='{$fmanager}' limit 1;");
    if(!$res) $last_error = pg_last_error($cyf);
    else $c = pg_fetch_assoc ($res);
/*     $cgroup = ltrim(rtrim($c['contractgroups'],'}" '),'{" '); */
/*     $q="select string_agg(gname,',') from groups where owner='{$cgroup}';"; */
/* //    print_r($q); */
/*     $res = pg_query($conn, $q); */
/*     $arr = pg_fetch_array($res, 0, PGSQL_NUM); */
/*     $c['usersgroups'] = implode(',',$arr); */
/* //    echo "<hr>";print_r($c); */
}
//curcount users
$curcount = 0;
$q = "SELECT sum(maxusers) as curcount FROM contracts WHERE manager='{$fmanager}';";
$result = @pg_query($cyf, $q);
if (!$result) $last_error = pg_last_error($cyf);
else{
    $firows = pg_num_rows($result);
    $fi=getValues($result);
    for ($j=0;$j<$firows;$j++){
        $curcount=$fi[$j]['curcount'];
    }
}
//ordinal users?
$ocurcount = 0;
$q = "SELECT count(*) as curcount FROM users WHERE manager='{$fmanager}';";
//echo $q."<br>";
$result = @pg_query($cyf, $q);
if (!$result) $last_error = pg_last_error($cyf);
else{
    $firows = pg_num_rows($result);
    $fi=getValues($result);
    for ($j=0;$j<$firows;$j++){
        $ocurcount=$fi[$j]['curcount'];
    }
}
//maxcount users
$maxcount = 0;
$q = "SELECT sum(maxusers) as maxcount FROM contracts WHERE '{{$fmanager}}' && contractgroups;";
$result = @pg_query($cyf, $q);
if (!$result) $last_error = pg_last_error($cyf);
else{
    $firows = pg_num_rows($result);
    $fi=getValues($result);
    for ($j=0;$j<$firows;$j++){
        $maxcount=$fi[$j]['maxcount'];
    }
}

$tcurr=$maxcount-$ocurcount-$curcount;

echo "<input type='hidden' id='qusers' name='qusers' value='{$tcurr}'>";
echo "<input type='hidden' id='currid' name='gid' value='{$c['id']}'>";

echo "<table><caption style='text-align:center;'>Contract/Filters</caption>";

echo "<tr><td>Contract<sup>*</sup>:</td>";
echo "<td><input type='text' id='icname' name='cname' value='{$c['cname']}'></td>";
echo "<td><input type='checkbox' id='ifcname' name='ifcname'";
if(!empty($_POST['ifcname'])) echo ' checked';
echo ">view with contract like this</td></tr>";

echo "<tr><td>Customer<sup>*</sup>:</td>";
echo "<td><input type='text' id='ibname' name='bname' value='{$c['bname']}'></td>";
echo "<td><input type='checkbox' id='ifbname' name='ifbname'";
if(!empty($_POST['ifbname'])) echo ' checked';
echo ">view with customer like this</td></tr>";

echo "<tr><td>Address<sup>*</sup>:</td>";
echo "<td><input type='text' id='iaddress' name='address' value='{$c['address']}'></td>";
echo "<td><input type='checkbox' id='ifaddress' name='ifaddress'";
if(!empty($_POST['ifaddress'])) echo ' checked';
echo ">view with address like this</td></tr>";

echo "<tr><td>Business ID<sup>*</sup>:</td>";
echo "<td><input type='text' id='ibid' name='bid' value='{$c['bid']}'></td>";
echo "<td><input type='checkbox' id='ifbid' name='ifbid'";
if(!empty($_POST['ifbid'])) echo ' checked';
echo ">view with ID like this</td></tr>";

echo "<tr><td>Operating&nbsp;sector:</td>";
echo "<td><input type='text' id='iosec' name='osec' value='{$c['osec']}'></td>";
echo "<td><input type='checkbox' id='ifosec' name='ifosec'";
if(!empty($_POST['ifosec'])) echo ' checked';
echo ">view with Operating&nbsp;sector like this</td></tr>";

echo "<tr><td>Start&nbsp;date<sup>*</sup>:</td>";
echo "<td><input type='date' id='idstart' name='dstart' value='{$c['dstart']}'></td>";
echo "<td><input type='checkbox' id='ifdstart' name='ifdstart'";
if(!empty($_POST['ifdstart'])) echo ' checked';
echo ">view with start&nbsp;date <= this</td></tr>";

echo "<tr><td>End&nbsp;date<sup>*</sup>:</td>";
echo "<td><input type='date' id='idend' name='dend' value='{$c['dend']}'></td>";
echo "<td><input type='checkbox' id='ifdend' name='ifdend'";
if(!empty($_POST['ifdend'])) echo ' checked';
echo ">view with end&nbsp;date <= this</td></tr>";

echo "<tr><td>Status:</td>";
echo "<td><input type='text' id='istatus' name='status' value='{$c['status']}'></td>";
echo "<td><input type='checkbox' id='ifstatus' name='ifstatus'";
if(!empty($_POST['ifstatus'])) echo ' checked';
echo ">view with status like this</td></tr>";

echo "<tr><td>Contact<sup>*</sup>:</td>";
echo "<td><input type='text' id='icontact' name='contact' value='{$c['contact']}'></td>";
echo "<td><input type='checkbox' id='ifcontact' name='ifcontact'";
if(!empty($_POST['ifcontact'])) echo ' checked';
echo ">view with contact like this</td></tr>";

echo "<tr><td>language:</td>";
echo "<td><input type='text' id='ilang' name='lang' value='{$c['lang']}'></td>";
echo "<td><input type='checkbox' id='iflang' name='iflang'";
if(!empty($_POST['iflang'])) echo ' checked';
echo ">view with language like this</td></tr>";

echo "<tr><td>tel<sup>*</sup>:</td>";
echo "<td><input type='text' id='itel' name='tel' value='{$c['tel']}'></td>";
echo "<td><input type='checkbox' id='iftel' name='iftel'";
if(!empty($_POST['iftel'])) echo ' checked';
echo ">view with tel like this</td></tr>";

echo "<tr><td>url:</td>";
echo "<td><input type='text' id='iurl' name='url' value='{$c['url']}'></td>";
echo "<td><input type='checkbox' id='ifurl' name='ifurl'";
if(!empty($_POST['ifurl'])) echo ' checked';
echo ">view with url like this</td></tr>";

echo "<tr><td>email<sup>*</sup>:</td>";
echo "<td><input type='text' id='iemail' name='email' value='{$c['email']}'></td>";
echo "<td><input type='checkbox' id='ifemail' name='ifemail'";
if(!empty($_POST['ifemail'])) echo ' checked';
echo ">view with email like this</td></tr>";

echo "<tr><td>password<sup>*</sup>:</td>";
echo "<td><input type='password' id='ipsword' name='psword' value='{$password}'>";
echo "<i style='color:blue;'>&nbsp;not empty for Add</i></td>";
echo "<td><i>may be empty for Update</i></td></tr>";

echo "<tr><td colspan='3'><i style='color:green;'>Users limit on a main contract: {$maxcount}.  Now users (own/subcontract users): {$ocurcount}/{$curcount}. Users available {$tcurr}.</i></td></tr>";
echo "<tr><td>Max # of contract users<sup>*</sup>:</td><td><input id='imaxusers' name='maxusers' value='{$c['maxusers']}'></td>";
echo "<td><label></label></td></tr>";

//$v = ltrim(rtrim($c['contractgroups'],'} "'),'{ "');
echo "<tr><td>Contract group<sup>*</sup>:</td><td><input type='text' name='contractgroups' value='{$c['contractgroups']}'>";
/* $v = ltrim(rtrim($c['contractgroups'],'} '),'{ '); */
/* echo $v; */
/* echo "</textarea>"; */
echo "</td>";
echo "<td><i style='color:blue;'>&nbsp;Note: group will be added to {$fmanager} groups</i></td></tr>";

echo "<tr><td colspan='3' style='text-align:left;'><i style='color:blue;'>Users of this contract may be members of the following group:</i></td></tr>";

echo "<tr><td>User's group<sup>*</sup>:</td><td><input type='text' name='usersgroups' value='{$c['usersgroups']}'>";
/* $v = ltrim(rtrim($c['usersgroups'],'} '),'{ '); */
/* echo $v; */
/* echo "</textarea>"; */
echo "</td>";
echo "<td><i style='color:red;'>Be careful, some groups may be unavailable because occupied by other customers</i></td></tr>";

/* echo "<tr><td>Apps:</td>"; */
/* echo "<td><textarea multiple size='2' name='apps'>"; */
/* $v = ltrim(rtrim($c['apps'],'}'),'{'); */
/* echo $v; */
/* echo "</textarea></td>"; */
/* echo "<td><input type='checkbox' id='ifapps' name='ifapps'"; */
/* if(!empty($_POST['ifapps'])) echo ' checked'; */
/* echo ">view with apps like this</td></tr>"; */

//echo "<td></td></tr>";

echo "</table>";

echo "<center>";
echo <<<EOT
<table><caption>&nbsp;</caption>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="Insert">Add</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete">Delete</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update">Update</td></tr>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="View" checked>View</td>
<td><!--<input type="checkbox" id="ufilters" name="usefilters" value="usefilters"
EOT;
if(!empty($_POST['usefilters'])) echo ' checked';
echo '>use filters--></td><td><input type="submit" name="pcmd" value="Submit" style=""></td></tr>';
echo '</table>';

echo '</table>';

$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) > 1){
    echo $stat[1];
}else echo $last_error;

echo <<<EOT
</center>
</form>
<form method="post" action="../cyf/cyf_contractslist.php" target="contractslist" id="contractslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fcname" name="fcname">
<input type="hidden" id="fbname" name="fbname">
<input type="hidden" id="faddress" name="faddress">
<input type="hidden" id="fbid" name="fbid">
<input type="hidden" id="fosec" name="fosec">
<input type="hidden" id="fcontact" name="fcontact">
<input type="hidden" id="flang" name="flang">
<input type="hidden" id="ftel" name="ftel">
<input type="hidden" id="femail" name="femail">
<input type="hidden" id="furl" name="furl">
<input type="hidden" id="fdstart" name="fdstart">
<input type="hidden" id="fdend" name="fdend">
<input type="hidden" id="fmanager" name="fmanager">
<input type="hidden" id="fstatus" name="fstatus">
<input type="hidden" id="fcontractgroups" name="fcontractgroups">
<input type="hidden" id="fusersgroups" name="fusersgroups">
<input type="hidden" id="fapps" name="fapps">
</form>
</body>
</html>
EOT;

?>