<?php
include_once dirname(__DIR__) . "/Dao/GlobalConfiguration.php";
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Pages';
//$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
$lang= $param['lang'];
if($lang=='en'){
$tCreateSubscription="Create subscription";
$tChangeSubscription="Change subscription";
$tDeleteSubscription="Delete subscription";
$tPublishingGroups="Publishing groups";
$tReadingOnlyGroups="Reading only groups";
$tCrossReferenceSubscriptions="Cross reference themes";
$tModerator="Moderator";
$tSubscription="Subscription";
$tSubscriptions="Subscriptions";
$tempty="empty";
$tViewEditEvent="View/Edit event";
$tShowPath="Show path";
$tShowonMap="Show on map";
$tViewEditPOI="View/Edit POI";
$tEvent="Event";
$tEvents="Events";
$tCreateEvent="Create new event";
$tChangeEvent="Change event";
$tOnlyFor="Only for";
$tDeleteEvent="Delete event";
$tPublishEvent="Publish event";
$tToChangeAuthorandGroupsUsePublishafterSave="To change author and target groups use<br> <strong>Publish event</strong> after <strong>Create new event</strong>";
$tForExistingEvents="For existing events";
$temail="email";
$ttel="tel";
$tcontact="contact";
$tLongitude="Longitude";
$tLatitude="Latitude";
$tGetCoordinates="Get coordinates";
$tAddress="Address";
$tURL="URL";
$tlocation="location";
$tbegins="begins";
$tends="ends";
$tdate="date";
$ttime="time";
$tDescription="Description";
$tPOIdescription="POI description";
$tName="Name";
$tPOIname="POI name";
$tText="Text";
$tforgroups="for groups";
$tAuthor="Author";
$tOwner="Owner";
$tCreateEditSubscriptions="Create/Edit Subscriptions";
$tFilters="Filters";
$tAll="All";
$tLastMonth="Last month";
$tLastWeek="Last week";
$tYesterday="Yesterday";
$tToday="Today";
$tNextWeek="Next week";
$tNextMonth="Next month";
$tInTheFuture="In the future";
$tWithoutTime="Without time";
$tCreatePOIcategory="Create POI category";
$tChangePOIcategory="Change POI category";
$tDeletePOIcategory="Delete POI category";
$tCategories="Categories";
$tCategory="Category";
$tCrossReferenceCategories="Cross reference categories";
$tBack="Back";
$tMode="Mode";
$tMyLocation="Where am I?";
$tSearchNearby="Search places nearby";
$tSearchAddress="Search by address";
$tPointsOfInterest="Points Of Interest";
$tAddtoPOIs="Add to POIs";
$tClearSearchResult="Clear search result";
$tRestoreSearchResult="Restore search result";
$tRadius="Radius";
$tkm="km";
$tQueryRestaurant="Restaurant";
$tQueryAddress="275-291 Bedford Ave, Brooklyn, NY 11211, USA";
$tPOI="POI";
$tCreatePOI="Create new POI";
$tChangePOI="Change POI";
$tDeletePOI="Delete POI";
$tPublishPOI="Publish POI";
$tForExistingPOI="For existing POI";
$tavailable="available";
$topen="open";
$tclosed="closed";
$tNewPOI="New POI";
$tCreateEditCategories="Create/Edit Categories";
$tMap="Map";
$tUser="User";
$tMemberofGroups="Member of the groups";
$tOwnerofGroups="Owner of the groups";
$tLanguage="Language";
$tPage="Page";
$tChangePageVisibility="Change page visibility";
$tPageVisibleforGroups="Page visible for groups";
$tOnlyForPageOwner="Only for page owner";
$tSaveNewPage="Save new page";
$tChangePage="Change page";
$tDeletePage="Delete page";
$tItems="Items";
$tDialogPage="Dialog page";
$tAddItem="Add item";
$tEraseItem="Erase item";
$tPageTitle="Page title";
$tType="Type";
$tListItems="List items";
$tSinglePage="Single Page";
$tRawPages="Raw pages";
$tInternalApp="Internal app";
$tInternalPage="Internal page";
$tSearch="Search";
$tFeedback="Feedback";
$tWelcomeText="<h3>Welcome to My mobile tutor!</h3>
<p>The service is designed to help you as a student of University of Turku, to get to know your city and university, find places and create and attend various events.
My mobile tutor is in a pilot phase, so any feedback for further development is highly appreciated.</p>
<p>Enjoy your stay in the city and University of Turku!</p>";
$tAbout="About";
$tHelpEventsText="<h4>Filters/Subscriptions</h4>
<p>Choose what kind of events you want to see.</p>
<h4>Create new event</h4>
<p>You can create either private event or event which other people in your group can see and participate in. To change the target group, publish event after saving it. If not published, only you can see it. Add a link to your event, if you have one!</p>
<p>Choose either your own or available subscription.</p>
<h4>Create/Edit Subscriptions</h4>
<p>You can create your own subscriptions for your events! Only you can see them, but the events in it can be seen by your group, if you want to.</p>
";
$tHelpMapText="<h4>Where am I?</h4> 
<p>Shows your current location. If it doesn’t – activate location services on your device or in your browser.</p>
<h4>Search places nearby</h4>
<p>You can search the places you are interested in by their type, eg. restaurants, swimming pools, schools etc.</p>
<h4>Search by address</h4>
<p>You can find a particular place on the map by searching it by address.</p>
<h4>Points Of Interest</h4>
<p>Here you can see a list of interesting places in Turku, if a category is chosen in the upper right corner, only places of this category will appear in this list. By clicking on the POI, you can view it’s details, get its location or get the path to it from your location. You can also create your own POI!</p>
<h4>Add to POI</h4>
<p>After searching places nearby or places by address, you can click on the marker and choose Add to POI – now you can edit the details and save this place for yourself. Only you will be able to see it.</p>
<h4>Show path</h4>
<p>If you have found or chosen to view a place on the map, click on the marker and choose Show path – the path to the place from your location will appear.</p>
<h4>Restore search result</h4>
<p>If a place you have been searching on the map disappeared – restore the result. You cannot restore the result after clearing it.</p>
<h4>Clear search result</h4>
<p>This clears all the search results from the map. They cannot be restored.</p>
<h4>Create/Edit Categories</h4>
<p>You can create your own categories for your places! Only you can see them.</p>
";
$tHelp="Help";
$tRefresheventlist="Referesh list";
$tChooseanaction="Menu";
$tPasswordQuestion="Change password?";
$tConfirm="Confirm";
$tTypepassword="Type password";
$tOldpass="Old";
$tNewpass="New";
}
if($lang=='ru'){
$tCreateSubscription="Создать подписку";
$tChangeSubscription="Изменить подписку";
$tDeleteSubscription="Удалить подписку";
$tPublishingGroups="Группы публикации";
$tReadingOnlyGroups="Группы чтения";
$tCrossReferenceSubscriptions="Перекрестные подписки";
$tModerator="Модератор";
$tSubscription="Подписка";
$tSubscriptions="Подписки";
$tempty="пусто";
$tViewEditEvent="Посмотреть/Изменить событие";
$tShowPath="Маршрут";
$tShowonMap="Показать на карте";
$tViewEditPOI="Посмотреть/Изменить POI";
$tEvent="Событие";
$tEvents="События";
$tCreateEvent="Создать событие";
$tChangeEvent="Изменить событие";
$tOnlyFor="Только для";
$tDeleteEvent="Удалить событие";
$tPublishEvent="Опубликовать событие";
$tToChangeAuthorandGroupsUsePublishafterSave="Для изменения автора и целевой группы используй <br> <strong>Опубликовать событие</strong> после <strong>Создать событие</strong>";
$tForExistingEvents="Для существующих событий";
$temail="email";
$ttel="тел";
$tcontact="контакт";
$tLongitude="Долгота";
$tLatitude="Широта";
$tGetCoordinates="Координаты";
$tAddress="Адрес";
$tURL="URL";
$tlocation="местоположение";
$tbegins="начало";
$tends="конец";
$tdate="дата";
$ttime="время";
$tDescription="Описание";
$tPOIdescription="Описание POI";
$tName="Название";
$tPOIname="Название POI";
$tText="Текст";
$tforgroups="для групп";
$tAuthor="Автор";
$tOwner="Владелец";
$tCreateEditSubscriptions="Управление подписками";
$tFilters="Фильтры";
$tAll="Все";
$tLastMonth="В прошлом месяце";
$tLastWeek="На прошлой неделе";
$tYesterday="Вчера";
$tToday="Сегодня";
$tNextWeek="На следующей неделе";
$tNextMonth="В следующем месяце";
$tInTheFuture="В будущем";
$tWithoutTime="Без времени";
$tCreatePOIcategory="Создать категорию POI";
$tChangePOIcategory="Изменить категорию POI";
$tDeletePOIcategory="Удалить категорию POI";
$tCategories="Категории";
$tCategory="Категория";
$tCrossReferenceCategories="Перекрестные категории";
$tBack="Назад";
$tMode="Режим";
$tMyLocation="Где я?";
$tSearchNearby="Поиск мест поблизости";
$tSearchAddress="Поиск по адресу";
$tPointsOfInterest="Points Of Interest";
$tAddtoPOIs="Добавить в POI";
$tClearSearchResult="Очистить результаты поиска";
$tRestoreSearchResult="Восстановить результаты поиска";
$tRadius="Радиус";
$tkm="км";
$tQueryRestaurant="Ресторан";
$tQueryAddress="Невский 15, Санкт-Петербург, Россия";
$tPOI="POI";
$tCreatePOI="Создать POI";
$tChangePOI="Изменить POI";
$tDeletePOI="Удалить POI";
$tPublishPOI="Опубликовать POI";
$tForExistingPOI="Для существующих POI";
$tavailable="время открытия";
$topen="открыто";
$tclosed="закрыто";
$tNewPOI="Новое POI";
$tCreateEditCategories="Управление категориями";
$tMap="Карта";
$tUser="Пользователь";
$tMemberofGroups="Член групп";
$tOwnerofGroups="Владелец групп";
$tLanguage="Язык";
$tPage="Страница";
$tChangePageVisibility="Изменить видимость страницы";
$tPageVisibleforGroups="Страница видна для групп";
$tOnlyForPageOwner="Только для владельца страницы";
$tSaveNewPage="Создать страницу";
$tChangePage="Изменить страницу";
$tDeletePage="Удалить страницу";
$tItems="Элементы";
$tDialogPage="Диалоговая страница";
$tAddItem="Добавить элемент";
$tEraseItem="Удалить элемент";
$tPageTitle="Заголовок страницы";
$tType="Тип";
$tListItems="Список элементов";
$tSinglePage="Простая страница";
$tRawPages="Страница HTML";
$tInternalApp="Внутреннее приложение";
$tInternalPage="Внутренняя страница";
$tSearch="Поиск";
$tFeedback="Отзыв";
$tWelcomeText="<h3>Welcome to My mobile tutor!</h3>
<p>The service is designed to help you as a student of University of Turku, to get to know your city and university, find places and create and attend various events.
My mobile tutor is in a pilot phase, so any feedback for further development is highly appreciated.</p>
<p>Enjoy your stay in the city and University of Turku!</p>";
$tAbout="Об услуге";
$tHelpEventsText="<h4>Filters/Subscriptions</h4>
<p>Choose what kind of events you want to see.</p>
<h4>Create new event</h4>
<p>You can create either private event or event which other people in your group can see and participate in. To change the target group, publish event after saving it. If not published, only you can see it. Add a link to your event, if you have one!</p>
<p>Choose either your own or available subscription.</p>
<h4>Create/Edit Subscriptions</h4>
<p>You can create your own subscriptions for your events! Only you can see them, but the events in it can be seen by your group, if you want to.</p>
";
$tHelpMapText="<h4>Where am I?</h4> 
<p>Shows your current location. If it doesn’t – activate location services on your device or in your browser.</p>
<h4>Search places nearby</h4>
<p>You can search the places you are interested in by their type, eg. restaurants, swimming pools, schools etc.</p>
<h4>Search by address</h4>
<p>You can find a particular place on the map by searching it by address.</p>
<h4>Points Of Interest</h4>
<p>Here you can see a list of interesting places in Turku, if a category is chosen in the upper right corner, only places of this category will appear in this list. By clicking on the POI, you can view it’s details, get its location or get the path to it from your location. You can also create your own POI!</p>
<h4>Add to POI</h4>
<p>After searching places nearby or places by address, you can click on the marker and choose Add to POI – now you can edit the details and save this place for yourself. Only you will be able to see it.</p>
<h4>Show path</h4>
<p>If you have found or chosen to view a place on the map, click on the marker and choose Show path – the path to the place from your location will appear.</p>
<h4>Restore search result</h4>
<p>If a place you have been searching on the map disappeared – restore the result. You cannot restore the result after clearing it.</p>
<h4>Clear search result</h4>
<p>This clears all the search results from the map. They cannot be restored.</p>
<h4>Create/Edit Categories</h4>
<p>You can create your own categories for your places! Only you can see them.</p>
";
$tHelp="Помощь";
$tRefresheventlist="Обновить список";
$tChooseanaction="Меню";
$tPasswordQuestion="Изменить пароль?";
$tConfirm="Подтвердить";
$tTypepassword="Наберите пароль";
$tOldpass="Старый";
$tNewpass="Новый";
}
if($lang=='fi'){
$tCreateSubscription="Luo uusi teema";
$tChangeSubscription="Muokkaa teema";
$tDeleteSubscription="Poista teema";
$tPublishingGroups="Ryhmät julkaisuoikeuksilla";
$tReadingOnlyGroups="Ryhmät vain luku -oikeuksilla";
$tCrossReferenceSubscriptions="Risti-viittaus teemoihin";
$tModerator="Moderaattori";
$tSubscription="Teema";
$tSubscriptions="Teemat";
$tempty="tyhjä";
$tViewEditEvent="Näytä/Muokkaa tapahtuma";
$tShowPath="Näytä polku";
$tShowonMap="Näytä kartalla";
$tViewEditPOI="Näytä/Muokkaa paikka";
$tEvent="Tapahtuma";
$tEvents="Tapahtumat";
$tCreateEvent="Luo uusi tapahtuma";
$tChangeEvent="Muokkaa tapahtuma";
$tOnlyFor="Vain käyttäjälle";
$tDeleteEvent="Poista tapahtuma";
$tPublishEvent="Julkaise tapahtuma";
$tToChangeAuthorandGroupsUsePublishafterSave="Jos haluat vaihtaa tekijän ja kohderyhmän,<br> <strong>Julkaise tapahtuma</strong><br> <strong>Luo uusi tapahtuma</strong>-toiminnon jälkeen";
$tForExistingEvents="Koskee olemassa olevia tapahtumia";
$temail="email";
$ttel="puh";
$tcontact="yhteydenotto";
$tLongitude="Pituusaste";
$tLatitude="Leveysaste";
$tGetCoordinates="Koordinaatit";
$tAddress="Osoite";
$tURL="URL";
$tlocation="sijainti";
$tbegins="alkaa";
$tends="loppuu";
$tdate="päivämäärä";
$ttime="aika";
$tDescription="Kuvaus";
$tPOIdescription="Paikan kuvaus";
$tName="Nimi";
$tPOIname="Paikan nimi";
$tText="Teksti";
$tforgroups="ryhmille";
$tAuthor="Tekijä";
$tOwner="Omistaja";
$tCreateEditSubscriptions="Teemojen hallinta";
$tFilters="Suodatin";
$tAll="Kaikki";
$tLastMonth="Viime kuukaudelta";
$tLastWeek="Viime viikolta";
$tYesterday="Eilen";
$tToday="Tänään";
$tNextWeek="Ensi viikolla";
$tNextMonth="Ensi kuukaudella";
$tInTheFuture="Tulevaisuudessa";
$tWithoutTime="Ilman aikaa";
$tCreatePOIcategory="Luo uusi kategoria";
$tChangePOIcategory="Muokkaa kategoria";
$tDeletePOIcategory="Poista kategoria";
$tCategories="Kategoriat";
$tCategory="Kategoria";
$tCrossReferenceCategories="Risti-viittaus kategorioihin";
$tBack="Takaisin";
$tMode="Haku";
$tMyLocation="Missä olen?";
$tSearchNearby="Hae paikat lähettyvillä";
$tSearchAddress="Hae osoitteen mukaan";
$tPointsOfInterest="Kiinnostavat paikat";
$tAddtoPOIs="Lisää kiinnostaviin paikkoihin";
$tClearSearchResult="Pyyhki hakutulokset";
$tRestoreSearchResult="Palauta hakutulokset";
$tRadius="Säde";
$tkm="km";
$tQueryRestaurant="Ravintola";
$tQueryAddress="Aurakatu 8, 20101 TURKU, Suomi";
$tPOI="Paikka";
$tCreatePOI="Luo uusi paikka";
$tChangePOI="Muokkaa paikka";
$tDeletePOI="Poista paikka";
$tPublishPOI="Julkaise paikka";
$tForExistingPOI="Koskee olemassa olevia paikkoja";
$tavailable="aukioloajat";
$topen="auki";
$tclosed="kiinni";
$tNewPOI="Uusi paikka";
$tCreateEditCategories="Kategorioiden hallinta";
$tMap="Kartta";
$tUser="Käyttäjä";
$tMemberofGroups="Jäsen ryhmissä";
$tOwnerofGroups="Omistaa ryhmät";
$tLanguage="Kieli";
$tPage="Sivu";
$tChangePageVisibility="Muuta sivun näkyvyys";
$tPageVisibleforGroups="Sivu näkyvillä ryhmille";
$tOnlyForPageOwner="Vain sivun omistajalle";
$tSaveNewPage="Tallenna uusi sivu";
$tChangePage="Muokkaa sivu";
$tDeletePage="Poista sivu";
$tItems="Osiot";
$tDialogPage="Dialogisivu";
$tAddItem="Lisää osio";
$tEraseItem="Poista osio";
$tPageTitle="Sivun otsikko";
$tType="Tyyppi";
$tListItems="Lista osioita";
$tSinglePage="Yksinkertainen sivu";
$tRawPages="Raaka sivu";
$tInternalApp="Sisäinen appi";
$tInternalPage="Sisäinen sivu";
$tSearch="Haku";
$tFeedback="Palaute";
$tWelcomeText="<h3>Welcome to My mobile tutor!</h3>
<p>The service is designed to help you as a student of University of Turku, to get to know your city and university, find places and create and attend various events.
My mobile tutor is in a pilot phase, so any feedback for further development is highly appreciated.</p>
<p>Enjoy your stay in the city and University of Turku!</p>";
$tAbout="Palvelusta";
$tHelpEventsText="<h4>Filters/Subscriptions</h4>
<p>Choose what kind of events you want to see.</p>
<h4>Create new event</h4>
<p>You can create either private event or event which other people in your group can see and participate in. To change the target group, publish event after saving it. If not published, only you can see it. Add a link to your event, if you have one!</p>
<p>Choose either your own or available subscription.</p>
<h4>Create/Edit Subscriptions</h4>
<p>You can create your own subscriptions for your events! Only you can see them, but the events in it can be seen by your group, if you want to.</p>
";
$tHelpMapText="<h4>Where am I?</h4> 
<p>Shows your current location. If it doesn’t – activate location services on your device or in your browser.</p>
<h4>Search places nearby</h4>
<p>You can search the places you are interested in by their type, eg. restaurants, swimming pools, schools etc.</p>
<h4>Search by address</h4>
<p>You can find a particular place on the map by searching it by address.</p>
<h4>Points Of Interest</h4>
<p>Here you can see a list of interesting places in Turku, if a category is chosen in the upper right corner, only places of this category will appear in this list. By clicking on the POI, you can view it’s details, get its location or get the path to it from your location. You can also create your own POI!</p>
<h4>Add to POI</h4>
<p>After searching places nearby or places by address, you can click on the marker and choose Add to POI – now you can edit the details and save this place for yourself. Only you will be able to see it.</p>
<h4>Show path</h4>
<p>If you have found or chosen to view a place on the map, click on the marker and choose Show path – the path to the place from your location will appear.</p>
<h4>Restore search result</h4>
<p>If a place you have been searching on the map disappeared – restore the result. You cannot restore the result after clearing it.</p>
<h4>Clear search result</h4>
<p>This clears all the search results from the map. They cannot be restored.</p>
<h4>Create/Edit Categories</h4>
<p>You can create your own categories for your places! Only you can see them.</p>
";
$tHelp="Help";
$tRefresheventlist="Päivitä lista";
$tChooseanaction="Valikko";
$tPasswordQuestion="Vaihda salasana";
$tConfirm="Varmista";
$tTypepassword="Syötä salasana";
$tOldpass="Vanha";
$tNewpass="Uusi";
}
switch($lang){
	case 'fi':
	$main='Main_fi';
		break;
	case 'ru':
	$main='Main_ru';
		break;
	default:
		$main='Main_en';
}
$page= empty($_REQUEST['page']) ? $main : $_REQUEST['page'];
//<a href='../cyf/cyf_pages.php?page=INFO&provider=CYF+Digital+Services' rel='external'></a>
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<style>
/*    
@media (min-width: 858px) {
    html {
        font-size: 12px;
    }
}

@media (min-width: 780px) {
    html {
        font-size: 11px;
    }
}

@media (min-width: 702px) {
    html {
        font-size: 10px;
    }
}

@media (min-width: 724px) {
    html {
        font-size: 9px;
    }
}

@media (max-width: 623px) {
    html {
        font-size: 8px;
    }
}
*/
.ui-grid-solo img {
    width  : 100%;
    height : auto;
}
</style>
<script>
function getTextNewsubscription(){
    return <?php echo '"'.$tNewsubscription.'"'; ?>
};
function getTextNewpoicategory(){
    return <?php echo '"'.$tNewpoicategory.'"'; ?>
};

var user = "<?php echo $param['email']; ?>";
var basepage = "<?php echo $page; ?>";
var myLocation = {lat: 60.451555,lng: 22.26718340000002};
var myLocationMarker;
var myInfoWindow;
var selectedplace= {};
var Gmap;
var myKey;
var myRadius = 500;
var infoWindow;
var geocoder;
var directionsDisplay;
var directionsService;
var items;
var markers=[];
var Placeservice;
var places;
var pageId = 0;
var nextId = 0;
var apiready = 0;
var language='<?php echo $lang; ?>';
</script>
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXUJpVG-9VwNCRg6E2GVkJenwOxJhzx3Y&libraries=places&callback=initMap"></script> -->	
<script src="../cyf/cyf_digital_service_user.js"></script>

</head>
<body>
<!-- cyf_pages.php -->	
<input type='hidden' id='provider' value="<?php echo $provider; ?>">
<!--
<div data-role="page" data-position="fixed" data-mini="true" id="WelcomePage">
  <div data-role="header" data-position="fixed" class="ui-grid-solo">

  <img border="0" src="../cyf/img/UTU_app_Final_header.png" alt="Logo"/>

  <a href="../cis/login.php?is_exit=0" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-power ui-btn-icon-notext" rel="external"></a>
  <a href='<?php echo $self; ?>?'  class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-notext' rel='external'></a>  

</div>
<div data-role="content" class="ui-content">
</div> 
  <div data-role="footer" data-position="fixed" style="text-align:center;"> 
  </div>
</div>
-->	

<!-- page -->
<div data-role="page" data-position="fixed" data-mini="true" id="MainPage">
  <div data-role="header" data-position="fixed" class="ui-grid-solo">
<!-- 	  <h6 style="font-size:83%;"><a href='<?php echo $self; ?>?' rel='external'><?php echo $provider; ?></a></h6> 
-->
<!--<h6 style="font-size:83%;"><?php echo $provider; ?></h6>-->
<!--<img src="../cyf/img/header_v3.png" alt="logo" class="align-right"/>-->
 <img border="0" src="../cyf/img/UTU_general_header.png" alt="Logo"/>
<!--<a href="../cis/login.php?is_exit=0" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-power ui-btn-icon-notext" rel="external"></a>-->
<a href="<?php echo $self; ?>" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-notext" rel="external"></a> 
<a href="#pageMenu" data-rel="popup" data-transition="slideup" class="ui-btn ui-btn-right ui-corner-all ui-shadow ui-btn-inline ui-icon-gear ui-btn-icon-left ui-btn-a ui-btn-icon-notext">Actions...</a>
<div data-role="popup" id="pageMenu" data-theme="b">
    <ul data-role="listview" data-inset="true" style="min-width:210px;">
      <li data-role="list-divider"><?php echo $tChooseanaction; ?></li>
      <li><a href="#User" class='ui-btn ui-corner-all ui-icon-user ui-btn-icon-left' id="user"><?php echo $tUser; ?></a></li>
      <li><a href="#AboutApp" class='ui-btn ui-corner-all ui-icon-info ui-btn-icon-left'><?php echo $tAbout; ?></a></li>
      <li><a href="http://chooseyourfuture.fi/feedback/" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-left" rel="external"><?php echo $tFeedback; ?></a></li>
    </ul>
</div>  
<!-- -->
</div>
<div data-role="content" class="ui-content" id="SinglePage">
<ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false" class="ui-listview" id="npages">
          <li><?php echo $tempty; ?></li>
    </ul>
</div> 
  <div data-role="footer" data-position="fixed" style="text-align:center;" class="ui-grid-solo">
<!--    <div data-role="controlgroup" data-type="horizontal">
	<a href='#User' class='ui-btn ui-corner-all ui-icon-user ui-btn-icon-left' id="user">&nbsp;&nbsp;<?php echo $tUser; ?>&nbsp;&nbsp;</a>
	<a href='#AboutApp' class='ui-btn ui-corner-all ui-icon-info ui-btn-icon-notext'>empty</a> 	
	<a href="http://chooseyourfuture.fi/feedback/" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-left" rel="external"><?php echo $tFeedback; ?></a> 
	</div>
	-->
    <img border="0" src="../cyf/img/UTU_general_footer.png" alt="Logo"/>
  </div>
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="DialogPage">
  <div data-role="header" data-position="fixed">
    <h2 id="dialogpagetitle"><?php echo $tDialogPage; ?></h2>
  </div>
  <div data-role="content" class="ui-content" id="dialogpagebody">
  </div>
  <div data-role="footer" data-position="fixed">
  </div>
</div>
    
<div data-role="page" id="Item0">
</div>
<div data-role="page" id="Item1">
</div>
<div data-role="page" id="Item2">
</div>
<div data-role="page" id="Item3">
</div>
<div data-role="page" id="Item4">
</div>
<div data-role="page" id="Item5">
</div>
<div data-role="page" id="Item6">
</div>
<div data-role="page" id="Item7">
</div>
<div data-role="page" id="Item8">
</div>
<div data-role="page" id="Item9">
</div>
<div data-role="page" id="Item10">
</div>
<div data-role="page" id="Item11">
</div>
<div data-role="page" id="Item12">
</div>
<div data-role="page" id="Item13">
</div>
<div data-role="page" id="Item14">
</div>
<div data-role="page" id="Item15">
</div>
<div data-role="page" id="Item16">
</div>
<div data-role="page" id="Item17">
</div>
<div data-role="page" id="Item18">
</div>
<div data-role="page" id="Item19">
</div>
<div data-role="page" id="Item20">
</div>
<div data-role="page" id="Item21">
</div>
<div data-role="page" id="Item22">
</div>
<div data-role="page" id="Item23">
</div>
<div data-role="page" id="Item24">
</div>
<div data-role="page" id="Item25">
</div>
<div data-role="page" id="Item26">
</div>
<div data-role="page" id="Item27">
</div>
<div data-role="page" id="Item28">
</div>
<div data-role="page" id="Item29">
</div>
<div data-role="page" id="Item30">
</div>
<div data-role="page" id="Item31">
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="newpage"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="newpagetitle"><?php echo $tPage; ?></h2>
  </div>
  <div data-role="content" class="ui-content" id="ipage">
    <input type='hidden' name='Igid' id='Igid'>
	<ul data-role="listview" id="listpage">
	  <li data-mini="true" >
		<label for="Ipauthor"><?php echo $tOwner; ?>:</label>
		<select name="Ipauthor" id="Ipauthor" data-native-menu="false">
<!--     <option data-placeholder="true">Author</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
    <input type="hidden" name="Inpid" id="Inpid">
	<label for="Itpage"><?php echo $tPageTitle; ?>:</label>
    <input type="text" name="Itpage" id="Itpage">
	  </li>
	  <li>
		<select name="Iptype" id="Iptype" data-native-menu="false">
		  <option data-placeholder="true"><?php echo $tType; ?></option>
          <option value="list"><?php echo $tListItems; ?></option>
          <option value="SinglePage"><?php echo $tSinglePage; ?></option>
          <option value="DialogPage"><?php echo $tDialogPage; ?></option>
          <option value="html"><?php echo $tRawPages; ?></option>
          <option value="app"><?php echo $tInternalApp; ?></option>
		  <option value="InternalPage"><?php echo $tInternalPage; ?></option>
<!--
          <option value="json">json</option>
          <option value="php">php</option>
          <option value="js">js</option>
-->
		</select>
	  </li>

	  <li data-role="collapsible" data-iconpos="right" data-inset="false" data-mini="true" data-collapsed-icon='edit' data-expanded-icon='edit' id="eitems">

<button type="button" data-icon="plus" data-iconpos="right" data-mini="true" data-inline="true" id="additem"><?php echo $tAddItem; ?></button>
<button type="button" data-icon="minus" data-iconpos="right" data-mini="true" data-inline="true" id="removelastitem"><?php echo $tEraseItem; ?></button>
<!--<button type="button" data-icon="recycle" data-iconpos="right" data-mini="true" data-inline="true" id="saveitems">Save</button>-->
<div data-role="collapsibleset" data-content-theme="a" data-iconpos="right" id="items"></div>
    <h2 id="pagn"><?php echo $tItems; ?></h2>
	  </li>
                                                                        
<!--
    <h2>For pages of private group &quot;<?php echo $param['email'];?>&quot; only ...</h2>
 -->
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2><?php echo $tOnlyForPageOwner; ?>...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpage"><?php echo $tDeletePage; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpage"><?php echo $tChangePage; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npage"><?php echo $tSaveNewPage; ?></a>
	  </li>
                                                                        
    <li data-inset="false">
    	<label for="Ipgroups"><?php echo $tPageVisibleforGroups; ?>:</label>
		<select name="Ipgroups[]" id="Ipgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
		</li>
	<li>
<!--
<a href='#' class="ui-btn ui-btn-inline ui-corner-all ui-icon-eye ui-btn-icon-right" id="spage"><?php echo $tChangePageVisibility; ?></a>
	  </li>
    <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing pages ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="spage">Publish page</a>
	  </li>
 -->     
	</ul>
  </div>
</div> <!-- page -->

<div data-role="page" data-dialog="true" id="DNearbySearch" data-mini="true"> 
  <div data-role="header" data-position="fixed">
    <h2><?php echo $tSearchNearby; ?></h2>
  </div>
<div data-role="content" class="ui-content">
    <div id="Dmap" style="height: 400px;width: 100%;"></div>
</div> 
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href='#' data-role="button" data-icon="navigation" class="ui-bar" id="groute"><?php echo $tShowPath; ?></a>
</div>
</div>
    
<div data-role="page" data-dialog="true" id="User"><!-- data-mini="true" --> 
  <div data-role="header" data-position="fixed">
    <h2><?php echo $param['email']; ?></h2>
  </div>
  <div data-role="content" class="ui-content">
    <ul data-role="listview">
    <!--<li><?php echo $tUser; ?>: <?php echo $param['email']; ?></li> -->
    <li>
    <img src="../cyf/img/<?php echo $param['lang']; ?>.png" alt="lang" class="ui-li-icon"/>
<!--     <?php echo $tLanguage; ?>: -->
		<select id="Ulang" data-native-menu="false">
		  <option data-placeholder="true"><?php echo $param['lang'] ?></option>
          <option value="en">en</option>
          <option value="fi">fi</option>
          <option value="ru">ru</option>
		</select>
    </li>
<!--     
    <li><?php echo $tMemberofGroups; ?>:<br><p><?php echo $param['membergroups']; ?></p></li>
	<li><?php echo $tOwnerofGroups; ?>:<br><p><?php echo $param['ownergroups']; ?></p></li>
 -->
<!-- in case regular user has the right to own groups-->
<li><div data-role="collapsible">
    <h4><?php echo $tPasswordQuestion; ?></h4>
    <ul data-role="listview">
        <li><a href="#" class="ui-btn uicorner-all ui-shadow" ><?php echo $tTypepassword; ?>
        <div class="ui-field-contain">
	       <label for="oldpass"><?php echo $tOldpass; ?>:</label>
           <input type="password" name="oldpass" id="oldpass">
	       <label for="newpass"><?php echo $tNewpass; ?>:</label>
           <input type="password" name="newpass" id="newpass">
	    </div>
    </a></li>
   <li>
  <a href="../cis/login.php?is_exit=1" class="ui-btn uicorner-all ui-shadow ui-icon-check ui-btn-icon-left" id="chpas" rel="external"><center><?php echo $tConfirm; ?></center></a>
  </li>    
    </ul>
</li>
    </ul>
</div>	
  </div>
</div>
<!-- information about the app-->
<div data-role="page" data-dialog="true" id="AboutApp"><!-- data-mini="true" --> 
  <div data-role="header" data-position="fixed">
    <h2><?php echo $tAbout; ?></h2>
  </div>
  <div data-role="content" class="ui-content">
	<?php echo $tWelcomeText; ?>
  </div>
</div>

<!-- help for maps-->
<div data-role="page" data-dialog="true" id="HelpMap"><!-- data-mini="true" --> 
  <div data-role="header" data-position="fixed">
    <h2><?php echo $tHelp; ?></h2>
  </div>
  <div data-role="content" class="ui-content">
	<?php echo $tHelpMapText; ?>
  </div>
</div>

<!-- help for events-->
<div data-role="page" data-dialog="true" id="HelpEvents"><!-- data-mini="true" --> 
  <div data-role="header" data-position="fixed">
    <h2><?php echo $tHelp; ?></h2>
  </div>
  <div data-role="content" class="ui-content">
	<?php echo $tHelpEventsText; ?>
  </div>
</div>
<!-- cyf_pois.html-->

<div data-role="page" id="Pois" data-position="fixed" data-cache="false"> <!-- page -->
	<div data-role="header" data-position="fixed">
<!-- <a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-right ui-btn-icon-notext' id="gohomefrommap"></a> -->
      <a href="<?php echo $self; ?>" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-notext" rel="external"></a>  
		<h6><?php echo $tMap; ?></h6>
<a href="#mapMenu" data-rel="popup" data-transition="slideup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-bars ui-btn-icon-left ui-btn-a ui-btn-icon-notext">Actions...</a>
<div data-role="popup" id="mapMenu" data-theme="b">
    <ul data-role="listview" data-inset="true" style="min-width:210px;">
      <li data-role="list-divider"><?php echo $tChooseanaction; ?></li>
      <li><a href="#poicategorylist" class="ui-btn ui-corner-all ui-icon-edit ui-btn-icon-left"><?php echo $tCreateEditCategories; ?></a></li> <!-- transfer to other place -->
      <li><a href="#HelpMap" class="ui-btn ui-corner-all ui-icon-info ui-btn-icon-left"><?php echo $tHelp; ?></a></li>
      <li><a href="http://chooseyourfuture.fi/feedback/" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-left" rel="external"><?php echo $tFeedback; ?></a></li>
    </ul>
</div>
  <div data-role="navbar">
	<ul>
	  <li>
		<select class="Coptions" id="Moptions" data-native-menu="false">
		  <option data-placeholder="true"><?php echo $tMode; ?></option>
          <option value="myloc"><?php echo $tMyLocation; ?></option>
          <option value="nsearch"><?php echo $tSearchNearby; ?></option><!--Nearby Search place renamed-->
          <option value="asearch"><?php echo $tSearchAddress; ?></option><!--Address location renamed-->
          <option value="gfav"><?php echo $tPointsOfInterest; ?></option><!--Favorites renamed-->
          <option value="addfav"><?php echo $tAddtoPOIs; ?></option><!--Add to favorites renamed-->
          <option value="gpath"><?php echo $tShowPath; ?></option>
          <option value="csearch"><?php echo $tClearSearchResult; ?></option>
          <option value="rsearch"><?php echo $tRestoreSearchResult; ?></option>
		</select>
	  </li>
      <li>
		<select class="ecategories" name="Mfecategories[]" id="Mfecategories" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true"><?php echo $tCategories; ?></option>
<!--      <option value="1">Get categories</option> -->
<?php
$q="SELECT theme FROM poicategories WHERE allowgroups='{}' OR allowgroups && '{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
//echo $q."<br>";
$res = pg_query($cyf, $q);
if ($res) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
		if(!empty($_REQUEST['theme']) and $_REQUEST['theme']===$r['theme'])
	        echo "<option value='{$r['theme']}' selected>{$r['theme']}</option>";
	  else
            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
            //echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
    }
}
?>
		</select>
	  </li>
    </ul>
  </div>
</div>

<div data-role="content" class="ui-content">
    <div id="Map" style="height: 400px;width: 100%;"></div>
</div> 
<div data-role="footer" class="ui-bar" style="text-align:right;">
</div>
</div>

  <input type='hidden' name='xlt' id='xlt' value='0'>
  <input type='hidden' name='xln' id='xln' value='0'>

<div data-role="page" data-dialog="true" data-mini="true" id="Poilist">
  <div data-role="header" data-position="fixed">
    <h5><?php echo $tPointsOfInterest; ?></h5><!-- Favourites renamed-->
  </div>

  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview">
<!--	<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>-->
<li><a href='#newpoi' id='Mpid' data-user-id='0'>
<h6><?php echo $tCreatePOI; ?>...</h6>

</a></li>	
	</ul>
        <ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="poilist">
          <li><?php echo $tempty; ?></li>
        </ul>
  </div> 
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="newpoi"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h5 id="poititle"><?php echo $tPOI; ?></h5>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="a">
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="spoi"><?php echo $tShowonMap; ?></a></li>
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="ppoi"><?php echo $tShowPath; ?></a></li>
    <li><a href="#viewpoi" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="epoi"><?php echo $tViewEditPOI; ?></a></li>
    </ul>
 </div>
</div> <!-- page data-dialog="true" -->

<input type='hidden' name='Mnid' id='Mnid'>

  <div data-role="page" data-dialog="true" data-mini="true" id="viewpoi"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h5 id="Mpoititle"><?php echo $tNewPOI; ?></h5>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview">
	  <li >
		<label for="Mpoiname"><?php echo $tPOIname; ?>:</label>
        <input type="text" name="Mpoiname" id="Mpoiname" placeholder="<?php echo $tName; ?> ...">
		<label for="Mdescr"><?php echo $tPOIdescription; ?>:</label>
		<textarea name="Mdescr" id="Mdescr" placeholder="<?php echo $tDescription; ?> ..."></textarea>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tdate; ?>, <?php echo $ttime; ?></h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<fieldset class="ui-grid-b">
              <label for="Maday"><?php echo $tavailable; ?>:</label><input type="text" name="Maday" id="Maday">
			  <div class="ui-block-a"><label for="Mopentime"><?php echo $topen; ?>:</label>
                <input data-mini="true" type="time" name="Mopentime" id="Mopentime"></div>
			  <div class="ui-block-b"><label for="Mclosetime"><?php echo $tclosed; ?>:</label>
                <input data-mini="true" type="time" name="Mclosetime" id="Mclosetime"></div>
              <div class="ui-block-c"></div>
			</fieldset>
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tlocation; ?></h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Murl"><?php echo $tURL; ?>:</label>
			<input type="url" name="Murl" id="Murl">
			<label for="Mlocation"><?php echo $tAddress; ?>:</label>
			<textarea name="Mlocation" id="Mlocation"></textarea>
			<p><a href="#" class="ui-btn ui-btn-inline" id="getcoorpoi"><?php echo $tGetCoordinates; ?></a></p>
			<label for="Mlongt"><?php echo $tLongitude; ?>:</label>
			<input type="number" step="any" name="Mlongt" id="Mlongt">
			<label for="Mlat"><?php echo $tLatitude; ?>:</label>
			<input type="number" step="any" name="Mlat" id="Mlat">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tcontact; ?></h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Mcontact"><?php echo $tcontact; ?>:</label>
			<input type="text" name="Mcontact" id="Mcontact" placeholder="Name ...">
			<label for="Mtel"><?php echo $ttel; ?>:</label>
			<input type="tel" name="Mtel" id="Mtel" placeholder="tel ...">
			<label for="Memail"><?php echo $temail; ?>:</label>
			<input type="email" name="Memail" id="Memail" placeholder="email ...">
		  </li>
		</ul>
	  </li>
<!--	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>Categories</h2> -->
	<li >
<div class="ui-field-contain">
		<label for="Mnecategories"><?php echo $tCategories; ?>:</label>
		<select multiple="multiple" data-native-menu="false" name="Mnecategories[]" id="Mnecategories" data-mini="true" data-inset="true">
<!--		  <option data-placeholder="true">Categories</option>-->
		</select>
</div> 
<!--	</li>
	<li >-->
<div class="ui-field-contain">
		<label for="Mauthor"><?php echo $tModerator; ?>:</label>
		<select name="Mauthor" id="Mauthor" data-native-menu="false" data-mini="true" data-inset="true">
<!--     <option data-placeholder="true">Moderator</option>-->
<?php
    echo "<option value='{$param['email']}'";
    echo ">{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
</div> 
<div class="ui-field-contain">
		<label for="Magroups"><?php echo $tforgroups; ?>:</label>
		<select name="Magroups[]" id="Magroups" multiple="multiple" data-native-menu="false" data-mini="true" data-inset="true">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
</div> 
	</li>
<!-- 
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2><?php echo $tForExistingPOI; ?>...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="upoi"><?php echo $tPublishPOI; ?></a>
	</li>
-->     
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2><?php echo $tOnlyFor; ?> &quot;<?php echo $param['email'];?>&quot;...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpoi"><?php echo $tDeletePOI; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpoi"><?php echo $tChangePOI; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npoi"><?php echo $tCreatePOI; ?></a>
	</li>
    </ul>
  </div>
</div> <!-- page data-dialog="true" -->
<!-- aaa -->
<div data-role="page" data-dialog="true" data-mini="true" id="AddrSearch">
  <div data-role="header" data-position="fixed">
    <h5><?php echo $tSearchAddress; ?></h5>
  </div>
<div data-role="content" class="ui-content">
<label for="addrsearch"><?php echo $tSearchAddress; ?>:</label>
<input type="search" id="addrsearch" placeholder="<?php echo $tQueryAddress; ?>" autocomplete="off" value="">
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="asr" ><?php echo $tSearch; ?></a>
  </div>
</div>
<!-- aaa -->

<div data-role="page" data-dialog="true" data-mini="true" id="NearbySearch">
  <div data-role="header" data-position="fixed">
    <h5><?php echo $tSearchNearby; ?></h5>
  </div>
                                                                        
<div data-role="content" class="ui-content">
<label for="nearbysearch"><?php echo $tSearchNearby; ?>:</label>
<input type="search" id="nearbysearch" placeholder="<?php echo $tQueryRestaurant; ?>" autocomplete="off" value="">
		<select name="radius" id="radius" data-native-menu="false">
		  <option data-placeholder="true"><?php echo $tRadius; ?></option>
          <option value="500" selected><?php echo $tRadius; ?> &lt;0.5 <?php echo $tkm; ?></option>
          <option value="1000"><?php echo $tRadius; ?> &lt;1 <?php echo $tkm; ?></option>
          <option value="3000"><?php echo $tRadius; ?> &lt;3 <?php echo $tkm; ?></option>
          <option value="50000"><?php echo $tRadius; ?> &gt;3 <?php echo $tkm; ?></option>
		</select>
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nsr" ><?php echo $tSearch; ?></a>
  </div>
</div>

<div data-role="page" data-mini="true" id="poimap"> <!-- page -->
    <div data-role="content" class="ui-content">
       <div id="map" style="width:600px; height:400px;"></div>
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back"><?php echo $tBack; ?></a>
    </div>
</div> <!-- page -->

<div data-role="page" data-mini="true" id="poipath"> <!-- page -->
    <div data-role="content" class="ui-content">
       <div id="pmap" style="width:600px; height:400px;"></div>
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="poipathback"><?php echo $tBack; ?></a>
    </div>
</div> <!-- page -->

<!-- poicategories -->
  <div data-role="page" id="poicategorylist" data-position="fixed">
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=1" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='#Pois' class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-back ui-btn-icon-left ui-btn-icon-notext"></a>
<h6><?php echo $tCategories; ?></h6>
<!--      <a href="<?php echo $self; ?>" class="ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-notext" rel="external"></a> -->  
<!--
<a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext'></a>
-->
    </div>

    <div data-role="main" class="ui-content">
		<ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview">
			<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid' data-user-id="0">
			<strong><?php echo $tCreatePOIcategory; ?>...</strong>
			</a></li>
		</ul>	
        <h3></h3>
        <ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview" id="poicategories">
          <li><?php echo $tempty; ?></li>
        </ul>

    </div> 
  </div> 

  <div data-role="page" data-dialog="true" data-mini="true" id="poicategory">
  <div data-role="header" data-position="fixed">
    <h2><?php echo $tCategory; ?></h2>
  </div>
    <div data-role="main" class="ui-content" id="mpoicategory">
	  <ul data-role="listview" id="poicategoryfields">
	  <li >
		<label for="Powner"><?php echo $tModerator; ?>:</label>
		<select name="Powner" id="Powner" data-native-menu="false">
<!--         <option data-placeholder="true">Moderator</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Ptheme"><?php echo $tCategory; ?>:</label>
		<textarea name="Ptheme" id="Ptheme" placeholder="<?php echo $tText; ?>..."></textarea>
        <input type='hidden' name='Pnid' id='Pnid'>
	  </li>
	  <li >
		<label for="Psubthemes"><?php echo $tCrossReferenceCategories; ?>:</label>
		<select name="Psubthemes[]" id="Psubthemes" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true"><?php echo $tCategories; ?></option>
		</select>
	  </li>
	  <li >
		<label for="Pallowgroups"><?php echo $tReadingOnlyGroups; ?>:</label>
		<select name="Pallowgroups[]" id="Pallowgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Reading only groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Ppublishgroups"><?php echo $tPublishingGroups; ?>:</label>
		<select name="Ppublishgroups[]" id="Ppublishgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Publishing groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  </ul>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpoicategory"><?php echo $tDeletePOIcategory; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpoicategory"><?php echo $tChangePOIcategory; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npoicategory"><?php echo $tCreatePOIcategory; ?></a>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
    </div>
  </div>
<!-- page ends -->


<!-- cyf_events.html -->
<div data-role="page" id="Events" data-position="fixed"> <!-- page -->
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
<a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<h6><?php echo $tEvents; ?></h6>
<!-- <a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-right ui-btn-icon-notext' id="gohomefromevent"></a> -->
<a href="<?php echo $self; ?>" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-home ui-btn-icon-notext" rel="external"></a>
<a href="#eventsMenu" data-rel="popup" data-transition="slideup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-bars ui-btn-icon-left ui-btn-a ui-btn-icon-notext">Actions...</a>
<div data-role="popup" id="eventsMenu" data-theme="b">
    <ul data-role="listview" data-inset="true" style="min-width:210px;">
      <li data-role="list-divider"><?php echo $tChooseanaction; ?></li>
      <li><a href="#Events" class="ui-btn ui-corner-all ui-icon-refresh ui-btn-icon-left" id='eventsRefresh'><?php echo $tRefresheventlist; ?></a></li>
<!--      <li><a href="#newevent" class="ui-btn ui-corner-all ui-icon-plus ui-btn-icon-left" data-user-Event='New event' data-user-id='0' id='Eaid'><?php echo $tCreateEvent; ?></a></li> -->
      <li><a href="#subscrevent" class="ui-btn ui-corner-all ui-icon-edit ui-btn-icon-left"><?php echo $tCreateEditSubscriptions; ?></a></li>
      <li><a href="#HelpEvents" class="ui-btn ui-corner-all ui-icon-info ui-btn-icon-left"><?php echo $tHelp; ?></a></li>
      <li><a href="http://chooseyourfuture.fi/feedback/" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-left" rel="external"><?php echo $tFeedback; ?></a></li>
    </ul>
</div>
    
    <div data-role="navbar">
	<ul>
	  <li>
		<select name="Efilters" id="Efilters" data-native-menu="false">
		  <option data-placeholder="true"><?php echo $tFilters; ?></option>
          <option value="all"><?php echo $tAll; ?></option>
          <option value="lmonth"><?php echo $tLastMonth; ?></option>
          <option value="lweek"><?php echo $tLastWeek; ?></option>
          <option value="lday"><?php echo $tYesterday; ?></option>
          <option value="tday"><?php echo $tToday; ?></option>
          <option value="nweek"><?php echo $tNextWeek; ?></option>
          <option value="nmonth"><?php echo $tNextMonth; ?></option>
          <option value="future"><?php echo $tInTheFuture; ?></option>
          <option value="notime"><?php echo $tWithoutTime; ?></option>
		</select>
	  </li>
      <li>
		<select class="ecategories" name="Efecategories[]" id="Efecategories" multiple="multiple" data-native-menu="false">
       <option data-placeholder="true"><?php echo $tSubscriptions; ?></option>
<!--      <option value="1">Get subscriptions</option> -->
<?php
$q="SELECT theme FROM subscriptions WHERE allowgroups='{}' OR allowgroups && '{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
//echo $q."<br>";
$res = pg_query($cyf, $q);
if ($res) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
    }
}
?>
		</select>
	  </li>
    </ul>
  </div>
	</div>
	  
<div data-role="content" class="ui-content">
	<ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview">
		<li><a href='#newevent' class='ui-btn ui-btn-icon-right ui-icon-carat-r' data-user-id='0' id="Eaid">
		<h6><?php echo $tCreateEvent; ?>...</h6>
	</a>
	</li>
	</ul>
        <h3></h3>
        <ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview" id="events">
          <li><?php echo $tempty; ?></li>
        </ul>

</div> 
<div data-role="footer" class="ui-bar" style="text-align:right;">
</div>
</div>

<!-- page -->
<div data-role="page" data-dialog="true" data-mini="true" id="newevent"> 
  <div data-role="header" data-position="fixed">
    <h2 id="newpagetitle"><?php echo $tEvent; ?></h2>
  </div>
  <div data-role="content" class="ui-content" id="ievent">
    <input type='hidden' name='Enid' id='Enid'>
	<ul data-role="listview" id="listevent">
	  <li >
		<label for="Eauthor"><?php echo $tAuthor; ?>:</label>
		<select name="Eauthor" id="Eauthor" data-native-menu="false">
<!--     <option data-placeholder="true">Author</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Eagroups"><?php echo $tforgroups; ?>:</label>
		<select name="Eagroups[]" id="Eagroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Eevent"><?php echo $tName; ?>:</label>
		<textarea name="Eevent" id="Eevent" placeholder="<?php echo $tText; ?> ..."></textarea>
	  </li>
	  <li >
		<label for="Emsg"><?php echo $tDescription; ?>:</label>
		<textarea name="Emsg" id="Emsg" placeholder="<?php echo $tText; ?> ..."></textarea>
	  </li>
	  
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tdate; ?>, <?php echo $ttime; ?></h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<fieldset class="ui-grid-b">
			  <div class="ui-block-a"><label for="Estime"><?php echo $tbegins; ?>:</label><input data-mini="true" type="date" name="Esdate" id="Esdate"><input data-mini="true" type="time" name="Estime" id="Estime"></div>
			  <div class="ui-block-b"><label for="Eetime"><?php echo $tends; ?>:</label><input data-mini="true" type="date" name="Eedate" id="Eedate"><input data-mini="true" type="time" name="Eetime" id="Eetime"></div>
    <div class="ui-block-c"></div>
			</fieldset>
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tlocation; ?></h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Eurl"><?php echo $tURL; ?>:</label>
			<input type="url" name="Eurl" id="Eurl">
			<label for="Elocation"><?php echo $tAddress; ?>:</label>
			<textarea name="Elocation" id="Elocation"></textarea>
			<p><a href="#" class="ui-btn ui-btn-inline" id="getcoorev"><?php echo $tGetCoordinates; ?></a></p>
			<label for="Elongt"><?php echo $tLongitude; ?>:</label>
			<input type="number" step="any" name="Elongt" id="Elongt">
			<label for="Elat"><?php echo $tLatitude; ?>:</label>
			<input type="number" step="any" name="Elat" id="Elat">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tcontact; ?></h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Econtact"><?php echo $tcontact; ?>:</label>
			<input type="text" name="Econtact" id="Econtact" placeholder="Name ...">
			<label for="Etel"><?php echo $ttel; ?>:</label>
			<input type="tel" name="Etel" id="Etel" placeholder="tel ...">
			<label for="Eemail"><?php echo $temail; ?>:</label>
			<input type="email" name="Eemail" id="Eemail" placeholder="email ...">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2><?php echo $tSubscriptions; ?></h2>
		<select multiple="multiple" data-native-menu="false" name="Enecategories[]" id="Enecategories" data-mini="true">
<!--		  <option data-placeholder="true">Choose subscriptions</option> -->
		</select>
	  </li>
     
	  <li data-role="collapsible" data-iconpos="right" data-inset="false" data-collapsed="false">
    <h2><?php echo $tOnlyFor; ?> <br> &quot;<?php echo $param['email'];?>&quot;</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="devent"><?php echo $tDeleteEvent; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cevent"><?php echo $tChangeEvent; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nevent"><?php echo $tCreateEvent; ?></a>
<!--
	<h3><?php echo $tForExistingEvents; ?> :</h3>
	<p style= "text-align:center"><i><?php echo $tToChangeAuthorandGroupsUsePublishafterSave; ?></i></p>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="sevent"><?php echo $tPublishEvent; ?></a>
 -->
	  </li>
	</ul>
  </div>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="showevent"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="poititle"><?php echo $tEvent; ?></h2>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="a">
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="mapevent"><?php echo $tShowonMap; ?></a></li>
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="pathevent"><?php echo $tShowPath; ?></a></li>
    <li><a href="#newevent" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r"><?php echo $tViewEditEvent; ?></a></li>
    </ul>
 </div>
</div><!-- page ends -->

<!-- page Subscriptions -->
  <div data-role="page" id="subscrevent" data-position="fixed">
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='#Events' class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-back ui-btn-icon-left ui-btn-icon-notext"></a>
<h6><?php echo $tSubscriptions; ?></h6>
<!--<a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext'></a>-->
    </div>

    <div data-role="main" class="ui-content">
		<ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview">
			<li><a href='#subscription' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Said' data-user-id="0">
			<h6><?php echo $tCreateSubscription; ?>...</h6>
			</a></li>
		</ul>
        <h3></h3>		
        <ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview" id="subscriptions">
          <li><?php echo $tempty; ?></li>
        </ul>

    </div> 
  </div> 

  <div data-role="page" data-dialog="true" data-mini="true" id="subscription">
  <div data-role="header" data-position="fixed">
    <h2><?php echo $tSubscription; ?></h2>
  </div>
    <div data-role="main" class="ui-content" id="msubscription">
	  <ul data-role="listview" id="subscriptionfields">
	  <li >
		<label for="Sowner"><?php echo $tModerator; ?>:</label>
		<select name="Sowner" id="Sowner" data-native-menu="false">
<!--         <option data-placeholder="true">Moderator</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Stheme"><?php echo $tSubscription; ?>:</label>
		<textarea name="Stheme" id="Stheme" placeholder="<?php echo $tText; ?> ..."></textarea>
        <input type='hidden' name='Snid' id='Snid'>
	  </li>
	  <li >
		<label for="Ssubthemes"><?php echo $tCrossReferenceSubscriptions; ?>:</label>
		<select name="Ssubthemes[]" id="Ssubthemes" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true"><?php echo $tSubscriptions; ?></option>
		</select>
	  </li>
	  <li >
		<label for="Sallowgroups"><?php echo $tReadingOnlyGroups; ?>:</label>
		<select name="Sallowgroups[]" id="Sallowgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Reading only groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Spublishgroups"><?php echo $tPublishingGroups; ?>:</label>
		<select name="Spublishgroups[]" id="Spublishgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Publishing groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  </ul>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dsubscription"><?php echo $tDeleteSubscription; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="csubscription"><?php echo $tChangeSubscription; ?></a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nsubscription"><?php echo $tCreateSubscription; ?></a>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
    </div>
  </div>
<!-- page ends -->


</body>
</html>