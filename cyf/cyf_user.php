<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

if(!empty($_POST['act']) AND ($_POST['act']=="Nlogin")){
//print_r($_POST);
    $q = "INSERT INTO logins (name,ownergroups,membergroups,email,psword) VALUES (";
    $q.= " '".pg_escape_string($_POST['nm'])."'";
    $q.= ",'".pg_escape_string($_POST['ownergroups'])."'";
    $q.= ",'".pg_escape_string($_POST['membergroups'])."'";
    if(!empty($_POST['em'])){
        $q.= ",'".pg_escape_string($_POST['em'])."'";
        if(!empty($_POST['ps'])){
            $q.= ",'".pg_escape_string($_POST['ps'])."'";
            $q.= ");";
            echo "<p>".$q."</p>";
            $res = pg_query($conn, $q);
            if ($res and pg_num_rows($res) >0) {
                echo "<p>Ok</p>";
            }else
                echo "<p>".pg_last_error($conn)."</p>";
        }
    }
    exit();
}

$last_error = 'Ok';
$fmanager = ltrim(rtrim($_REQUEST['rgroups'],'"}'),'{"');
    switch($_REQUEST['eaction']){
    case 'Cuserlang':
//update lang
        if(!empty($_REQUEST['email'])) {
            $q = "UPDATE users SET lang='{$_REQUEST['lang']}'";
            $q.= " WHERE email='{$_REQUEST['email']}';";
            $res = pg_query($cyf, $q);
            $q = "UPDATE logins SET lang='{$_REQUEST['lang']}'";
            $q.= " WHERE email='{$_REQUEST['email']}';";
            $res = pg_query($conn, $q);
        }
        $_SESSION['lang'] = $_REQUEST['lang'];
        echo '{"ok":"'.$_REQUEST['lang'].'"}';
        exit();
        break;
    case 'Cpasslang':
//update pass
        if(empty($_REQUEST['email'])) { echo '{"rc":"email?"}'; exit();}
        if(empty($_REQUEST['oldpass'])) { echo '{"rc":"oldpass?"}'; exit();}
        if(empty($_REQUEST['newpass'])) { echo '{"rc":"newpass?"}'; exit();}
        $q = "UPDATE logins SET psword='".pg_escape_string($_REQUEST['newpass'])."'";
        $q.= " WHERE email='".pg_escape_string($_REQUEST['email'])."'";
        $q.= " AND psword='".pg_escape_string($_REQUEST['oldpass'])."';";
        $res = pg_query($conn, $q);
        if ($res and pg_affected_rows($res) >0)
            echo '{"rc":"1"}';
        else
            echo '{"rc":"0"}';
        exit();
        break;
    case 'Duser':
//delete
        $q = "DELETE FROM users";
        $q.= " WHERE id={$_REQUEST['eid']}";
        $q.= " AND (";
        $q.= " manager='".$param['email']."'";
        $q.= " OR ARRAY[manager] && '{$param['ownergroups']}'";
        $q.= " OR ARRAY[manager] && '{$param['membergroups']}'";
        $q.= ");";
        $res = pg_query($cyf, $q);
        break;
    case 'Cuser':
//update email
        if(!empty($_REQUEST['email'])) {
            $q = "UPDATE users SET email='".htmlspecialchars($_REQUEST['email'])."'";
            if(!empty($_REQUEST['contact'])) $q.= ",contact='".htmlspecialchars($_REQUEST['contact'])."'";
            if(!empty($_REQUEST['lang']))    $q.= ",lang='".htmlspecialchars($_REQUEST['lang'])."'";
            if(!empty($_REQUEST['address'])) $q.= ",address='".htmlspecialchars($_REQUEST['address'])."'";
            if(!empty($_REQUEST['tel']))     $q.= ",tel='".htmlspecialchars($_REQUEST['tel'])."'";
            if(!empty($_REQUEST['dstart']))  $q.= ",dstart='".htmlspecialchars($_REQUEST['dstart'])."'";
            if(!empty($_REQUEST['dend']))    $q.= ",dend='".htmlspecialchars($_REQUEST['dend'])."'";
            if(!empty($_REQUEST['manager'])) $q.= ",manager='".htmlspecialchars($_REQUEST['manager'])."'";
            if(!empty($_REQUEST['membergroups'])) $q.= ",membergroups='".$_REQUEST['membergroups']."'";
            $q.= ",status='active'";
            $q.= " WHERE id={$_REQUEST['eid']}";
            $q.= " AND (";
            $q.= " manager='".$param['email']."'";
            $q.= " OR ARRAY[manager] && '".$param['ownergroups']."'";
            $q.= " OR ARRAY[manager] && '".$param['membergroups']."'";
            $q.= ");";
            $res = pg_query($cyf, $q);
        }
        break;
    case 'Suser':
//update manager
        $q = "UPDATE users SET manager='{$_REQUEST['manager']}'";
        $q.= " WHERE id={$_REQUEST['eid']}";
        $q.= " AND (";
        $q.= " manager='".$param['email']."'";
        $q.= " OR ARRAY[manager] && '".$param['ownergroups']."'";
        $q.= " OR ARRAY[manager] && '".$param['membergroups']."'";
        $q.= ");";
        $res = pg_query($cyf, $q);
        break;
    case 'Nuser':
//insert 	id	email contact address tel lang dstart dend cname manager status membergroups
        if(!empty($_REQUEST['email'])) {
            $i =array();
            $i['email'] = "'".htmlspecialchars($_REQUEST['email'])."'";
            if(!empty($_REQUEST['cname'])) $i['cname'] = "'".htmlspecialchars($_REQUEST['cname'])."'";
            if(!empty($_REQUEST['address'])) $i['address'] = "'".htmlspecialchars($_REQUEST['address'])."'";
            if(!empty($_REQUEST['contact'])) $i['contact'] = "'".htmlspecialchars($_REQUEST['contact'])."'";
            if(!empty($_REQUEST['tel']))     $i['tel'] = "'".htmlspecialchars($_REQUEST['tel'])."'";
            if(!empty($_REQUEST['lang']))    $i['lang'] = "'".htmlspecialchars($_REQUEST['lang'])."'";
            if(!empty($_REQUEST['manager'])) $i['manager'] = "'".htmlspecialchars($_REQUEST['manager'])."'";
            if(!empty($_REQUEST['dstart']))  $i['dstart'] = "'".htmlspecialchars($_REQUEST['dstart'])."'";
            if(!empty($_REQUEST['dend']))    $i['dend'] = "'".htmlspecialchars($_REQUEST['dend'])."'";
            if(!empty($_REQUEST['status']))  $i['status'] = "'".htmlspecialchars($_REQUEST['status'])."'";
            if(!empty($_REQUEST['membergroups'])) $i['membergroups'] = "'".$_REQUEST['membergroups']."'";
//            $i['status'] = "active";
//            echo "<li>".print_r($i)."</li>";
            pgInsert($cyf, 'users', $i);
        }
        break;
     case 'Guser':
//get user by $_REQUEST['email']
//email,psword,ownergroups,membergroups,name,lang
        $q = "SELECT array_to_string(ownergroups,'~^~') as ogr FROM logins";
        $q.= " WHERE email='".$_REQUEST['email']."';";
        $res = pg_query($conn, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $r=pg_fetch_assoc($res);
            $ogr= json_encode(explode('~^~',$r['ogr'] ));
            
            $q = "SELECT id,email,contact,address,tel,lang,dstart,dend,cname,manager,status";
            $q.= ",array_to_string(membergroups,'~^~') as sgr FROM users";
            $q.= " WHERE email='".$_REQUEST['email']."';";
            $res = pg_query($cyf, $q);
            if ($res and pg_num_rows($res) >0) {
                $r = array();
                $r=pg_fetch_assoc($res);
                $sgr= json_encode(explode('~^~',$r['sgr'] ));
                echo '{"id":"'.$r['id'].'","email":"'.$r['email'].'","contact":"'.$r['contact'].'"';
                echo ',"address":"'.$r['address'].'","tel":"'.$r['tel'].'","lang":"'.$r['lang'].'"';
                echo ',"dstart":"'.$r['dstart'].'","dend":"'.$r['dend'].'","cname":"'.$r['cname'].'"';
                echo ',"ownergroups":'.$ogr.',"membergroups":'.$sgr;
                echo ',"manager":"'.$r['manager'].'","status":"'.$r['status'].'"}';
            }else{
                echo '{"id":"0","str":"'.htmlspecialchars($q).'"}';
            }
        }else{
            echo '{"id":"0","str":"'.htmlspecialchars($q).'"}';
        }
        exit();
        break;
   case 'Juser':
        $q = "SELECT id,email,contact,address,tel,lang,dstart,dend,cname,manager,status";
        $q.= ",array_to_string(membergroups,'~^~') as sgr FROM users";
        $q.= " WHERE id=".$_REQUEST['eid'].";";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $r=pg_fetch_assoc($res);
            $sgr= json_encode(explode('~^~',$r['sgr'] ));
            echo '{"id":"'.$r['id'].'","email":"'.$r['email'].'","contact":"'.$r['contact'].'"';
            echo ',"address":"'.$r['address'].'","tel":"'.$r['tel'].'","lang":"'.$r['lang'].'"';
            echo ',"dstart":"'.$r['dstart'].'","dend":"'.$r['dend'].'","cname":"'.$r['cname'].'"';
            echo ',"manager":"'.$r['manager'].'","membergroups":'.$sgr.',"status":"'.$r['status'].'"}';
        }else{
            echo '{"id":"0"}';
        }
        exit();
        break;
    case 'Jusers':
        $q ="SELECT id,email,contact,address,tel,lang,dstart,dend,cname,manager,status";
        $q.=",array_to_string(membergroups,'~^~') as sgr FROM users";
        $q.=" WHERE  manager='".$param['email']."'";
        $q.=" OR ARRAY[manager] && '".$param['ownergroups']."'";
//        $q.=" OR ARRAY[manager] && '".$param['membergroups']."'";
        $q.=" ORDER BY contact;";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $d='[';
            while (($r=pg_fetch_assoc ( $res ))){
                echo $d;
                $sgr= json_encode(explode('~^~',$r['sgr'] ));
                echo '{"id":"'.$r['id'].'","email":"'.$r['email'].'","contact":"'.$r['contact'].'"';
                echo ',"address":"'.$r['address'].'","tel":"'.$r['tel'].'","lang":"'.$r['lang'].'"';
                echo ',"dstart":"'.$r['dstart'].'","dend":"'.$r['dend'].'","cname":"'.$r['cname'].'"';
                echo ',"manager":"'.$r['manager'].'","membergroups":'.$sgr.',"status":"'.$r['status'].'"}';
                $d=',';
            }
            echo ']';
        }else{
            echo '[{"id":"0"}]';
        }
        exit();
    default:
    }


$q = "SELECT id,email,contact,address,tel,lang,dstart,dend,cname,manager,status,membergroups FROM users";
$q.= " WHERE manager='".$param['email']."'";
$q.= " OR ARRAY[manager] && '".$param['ownergroups']."'";
//$q.= " OR ARRAY[manager] && '".$param['membergroups']."'";
$q.= " ORDER BY contact;";
$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
    $d='[';
    while (($r=pg_fetch_assoc ( $res ))){
//        echo "<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>";
        echo "<li><a href='#newuser' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Uaid'";
        echo " data-user-id='".$r['id']."' data-user-email='".$r['email']."'";
        echo " data-user-manager='".$r['manager']."'>";
        echo "<p><b style='font-size:130%;'>".$r['email']."</b> (<i>".$r['manager']."</i>)</p></a></li>";
    }
}else{
    echo "<li>".$q."</li>";
}
?>
