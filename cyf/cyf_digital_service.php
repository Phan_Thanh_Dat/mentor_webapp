<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

if ($param['email'] == 'guest') header("Location: https://mail.google.com");
$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Pages';
//$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
$lang= $param['lang'];
$page= empty($_REQUEST['page']) ? 'Main' : $_REQUEST['page'];
//<a href='../cyf/cyf_pages.php?page=INFO&provider=CYF+Digital+Services' rel='external'></a>
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script>
var basepage = "<?php echo $page; ?>";
var myLocation = {lat: 60.0,lng: 30.0};
var myLocationMarker;
var myInfoWindow;
var selectedplace= {};
var Gmap;
var myKey;
var myRadius = 500;
var infoWindow;
var geocoder;
var directionsDisplay;
var directionsService;
var items;
var markers=[];
var Placeservice;
var places;
var pageId = 0;
var nextId = 0;
var apiready = 0;
var language = '<?php echo $lang; ?>';
</script>
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXUJpVG-9VwNCRg6E2GVkJenwOxJhzx3Y&libraries=places&callback=initMap"></script> -->	
<script src="../cyf/cyf_digital_service.js"></script>

</head>
<body>
<!-- cyf_pages.php -->	
<input type='hidden' id='provider' value="<?php echo $provider; ?>">

<!-- page -->
<div data-role="page" data-position="fixed" data-mini="true" id="MainPage">
  <div data-role="header" data-position="fixed">
<!-- 	  <h6 style="font-size:83%;"><a href='<?php echo $self; ?>?' rel='external'><?php echo $provider; ?></a></h6> 
-->
	<h6 style="font-size:83%;"><?php echo $provider; ?></h6>  
<a href="../cis/login.php?is_exit=0" class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-power ui-btn-icon-notext" rel="external">Exit</a>
  <a href='<?php echo $self; ?>?'  class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-notext' rel='external'>Pages</a>  

</div>
<div data-role="content" class="ui-content" id="SinglePage">
<ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false" class="ui-listview" id="npages">
          <li>empty</li>
    </ul>
</div> 
  <div data-role="footer" data-position="fixed" style="text-align:center;">
   <a href='#User' class='ui-btn ui-corner-all ui-icon-user ui-btn-icon-right' id="user">User</a> 
  </div>
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="DialogPage">
  <div data-role="header" data-position="fixed">
    <h2 id="dialogpagetitle">DialogPage</h2>
  </div>
  <div data-role="content" class="ui-content" id="dialogpagebody">
  </div>
  <div data-role="footer" data-position="fixed">
  </div>
</div>
    
<div data-role="page" id="Item0">
</div>
<div data-role="page" id="Item1">
</div>
<div data-role="page" id="Item2">
</div>
<div data-role="page" id="Item3">
</div>
<div data-role="page" id="Item4">
</div>
<div data-role="page" id="Item5">
</div>
<div data-role="page" id="Item6">
</div>
<div data-role="page" id="Item7">
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="newpage"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="newpagetitle">Page</h2>
  </div>
  <div data-role="content" class="ui-content" id="ipage">
    <input type='hidden' name='Igid' id='Igid'>
	<ul data-role="listview" id="listpage">
	  <li data-mini="true" >
		<label for="Ipauthor">Owner:</label>
		<select name="Ipauthor" id="Ipauthor" data-native-menu="false">
<!--     <option data-placeholder="true">Author</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
    <input type="hidden" name="Inpid" id="Inpid">
	<label for="Itpage">Page title:</label>
    <input type="text" name="Itpage" id="Itpage">
	  </li>
	  <li>
		<select name="Iptype" id="Iptype" data-native-menu="false">
		  <option data-placeholder="true">Type</option>
          <option value="list">List items</option>
          <option value="SinglePage">Single Page</option>
          <option value="DialogPage">Dialog Page</option>
          <option value="html">Raw pages</option>
          <option value="app">Internal app</option>
		  <option value="InternalPage">Internal page</option>
<!--
          <option value="json">json</option>
          <option value="php">php</option>
          <option value="js">js</option>
-->
		</select>
	  </li>

	  <li data-role="collapsible" data-iconpos="right" data-inset="false" data-mini="true" data-collapsed-icon='edit' data-expanded-icon='edit' id="eitems">

<button type="button" data-icon="plus" data-iconpos="right" data-mini="true" data-inline="true" id="additem">Push item</button>
<button type="button" data-icon="minus" data-iconpos="right" data-mini="true" data-inline="true" id="removelastitem">Pop item</button>
<!--<button type="button" data-icon="recycle" data-iconpos="right" data-mini="true" data-inline="true" id="saveitems">Save</button>-->
<div data-role="collapsibleset" data-content-theme="a" data-iconpos="right" id="items"></div>
    <h2 id="pagn">Items</h2>
	  </li>
                                                                        
<!--
    <h2>For pages of private group &quot;<?php echo $param['email'];?>&quot; only ...</h2>
 -->
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>Only for page owner ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpage">Delete page</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpage">Change page</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npage">Save new page</a>
	  </li>
                                                                        
    <li data-inset="false">
    	<label for="Ipgroups">Page visible for groups:</label>
		<select name="Ipgroups[]" id="Ipgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
		</li>
	<li>
<a href='#' class="ui-btn ui-btn-inline ui-corner-all ui-icon-eye ui-btn-icon-right" id="spage">Change page visibility</a>
	  </li>
<!--
    <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing pages ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="spage">Publish page</a>
	  </li>
 -->     
	</ul>
  </div>
</div> <!-- page -->

    
<div data-role="page" data-dialog="true" data-mini="true" id="User">
  <div data-role="header" data-position="fixed">
    <h2>User</h2>
  </div>
  <div data-role="content" class="ui-content">
    <ul data-role="listview">
    <li>User: <?php echo $param['email']; ?></li>
    <li>Language: <?php echo $param['lang']; ?></li>
    <li>Member of the groups:<br><?php echo $param['membergroups']; ?></li>
    <li>Owner of the groups:<br><?php echo $param['ownergroups']; ?></li>
    </ul>
	
  </div>
</div>

<!-- cyf_pois.html-->

<div data-role="page" id="Pois" data-position="fixed" data-cache="false"> <!-- page -->
	<div data-role="header" data-position="fixed">
<h6 style="font-size:83%;">Map</h6>
<a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-right ui-btn-icon-notext'>Pages</a>
  <div data-role="navbar">
	<ul>
	  <li>
		<select class="Coptions" id="Moptions" data-native-menu="false">
		  <option data-placeholder="true">Mode</option>
          <option value="myloc">My location</option>
          <option value="nsearch">Search places nearby</option><!--Nearby Search place renamed-->
          <option value="asearch">Search by address</option><!--Address location renamed-->
          <option value="gfav">Points Of Interest</option><!--Favorites renamed-->
          <option value="addfav">Add to POIs</option><!--Add to favorites renamed-->
          <option value="gpath">Get path</option>
          <option value="csearch">Clear search result</option>
          <option value="rsearch">Restore search result</option>
		</select>
	  </li>
      <li>
		<select class="ecategories" name="Mfecategories[]" id="Mfecategories" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Categories</option>
<!--      <option value="1">Get categories</option> -->
<?php
/*    
$q="SELECT theme FROM poicategories WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
//echo $q."<br>";
$res = pg_query($cyf, $q);
if ($res) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
		if(!empty($_REQUEST['theme']) and $_REQUEST['theme']===$r['theme'])
	        echo "<option value='{$r['theme']}' selected>{$r['theme']}</option>";
	  else
            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
            //echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
    }
}
*/
?>
		</select>
	  </li>
    </ul>
  </div>
</div>

<div data-role="content" class="ui-content">
    <div id="Map" style="height: 400px;width: 100%;"></div>
</div> 
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href='#poicategorylist' data-role="button" data-icon="plus" class="ui-bar">Create/Edit Categories</a>
</div>
</div>

  <input type='hidden' name='xlt' id='xlt' value='0'>
  <input type='hidden' name='xln' id='xln' value='0'>

<div data-role="page" data-dialog="true" data-mini="true" id="Poilist">
  <div data-role="header" data-position="fixed">
    <h5>Points Of Interest</h5><!-- Favourites renamed-->
  </div>

  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview">
<!--	<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>-->
<li><a href='#newpoi' id='Mpid' data-user-id='0'>
<h6>Create new POI ...</h6>
<br>
</a></li>	
	</ul>
        <ul data-role="listview" data-theme="d" data-divider-theme="d" class="ui-listview" id="poilist">
          <li>empty</li>
        </ul>
  </div> 
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="newpoi"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h5 id="poititle">POI</h5>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="a">
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="spoi">Show on map</a></li>
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="ppoi">Show path</a></li>
    <li><a href="#viewpoi" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="epoi">View/Edit POI</a></li>
    </ul>
 </div>
</div> <!-- page data-dialog="true" -->

<input type='hidden' name='Mnid' id='Mnid'>

  <div data-role="page" data-dialog="true" data-mini="true" id="viewpoi"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h5 id="Mpoititle">New POI</h5>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview">
	  <li >
		<label for="Mpoiname">POI name:</label>
        <input type="text" name="Mpoiname" id="Mpoiname" placeholder="POI name ...">
		<label for="Mdescr">POI description:</label>
		<textarea name="Mdescr" id="Mdescr" placeholder="POI description ..."></textarea>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>date, time</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<fieldset class="ui-grid-b">
              <label for="Maday">available:</label><input type="text" name="Maday" id="Maday">
			  <div class="ui-block-a"><label for="Mopentime">open:</label>
                <input data-mini="true" type="time" name="Mopentime" id="Mopentime"></div>
			  <div class="ui-block-b"><label for="Mclosetime">close:</label>
                <input data-mini="true" type="time" name="Mclosetime" id="Mclosetime"></div>
              <div class="ui-block-c"></div>
			</fieldset>
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>location</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Murl">URL:</label>
			<input type="url" name="Murl" id="Murl">
			<label for="Mlocation">Address:</label>
			<textarea name="Mlocation" id="Mlocation"></textarea>
			<p><a href="#" class="ui-btn ui-btn-inline" id="getcoorpoi">Get coordinates</a></p>
			<label for="Mlongt">Longitude:</label>
			<input type="number" step="any" name="Mlongt" id="Mlongt">
			<label for="Mlat">Latitude:</label>
			<input type="number" step="any" name="Mlat" id="Mlat">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>contact</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Mcontact">contact:</label>
			<input type="text" name="Mcontact" id="Mcontact" placeholder="Name ...">
			<label for="Mtel">tel:</label>
			<input type="tel" name="Mtel" id="Mtel" placeholder="tel ...">
			<label for="Memail">email:</label>
			<input type="email" name="Memail" id="Memail" placeholder="email ...">
		  </li>
		</ul>
	  </li>
<!--	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>Categories</h2> -->
	<li >
<div class="ui-field-contain">
		<label for="Mnecategories">Categories:</label>
		<select multiple="multiple" data-native-menu="false" name="Mnecategories[]" id="Mnecategories" data-mini="true" data-inset="true">
<!--		  <option data-placeholder="true">Categories</option>-->
		</select>
</div> 
<!--	</li>
	<li >-->
<div class="ui-field-contain">
		<label for="Mauthor">Moderator:</label>
		<select name="Mauthor" id="Mauthor" data-native-menu="false" data-mini="true" data-inset="true">
<!--     <option data-placeholder="true">Moderator</option>-->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
</div> 
<div class="ui-field-contain">
		<label for="Magroups">for groups:</label>
		<select name="Magroups[]" id="Magroups" multiple="multiple" data-native-menu="false" data-mini="true" data-inset="true">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
echo "<option value='{$param['email']}'>{$param['email']}</option>";    
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
</div> 
	</li>
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing POI ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="upoi">Publish POI</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpoi">Delete POI</a>
	</li>
     
	<li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For private group &quot;<?php echo $param['email'];?>&quot; POI only ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpoi">Change POI</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npoi">Save new POI</a>
	</li>
    </ul>
  </div>
</div> <!-- page data-dialog="true" -->
<!-- aaa -->
<div data-role="page" data-dialog="true" data-mini="true" id="AddrSearch">
  <div data-role="header" data-position="fixed">
    <h5>Search by address</h5>
  </div>
<div data-role="content" class="ui-content">
<label for="addrsearch">Address location requests:</label>
<input type="search" id="addrsearch" placeholder="275-291 Bedford Ave, Brooklyn, NY 11211, USA" autocomplete="off" value="">
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="asr" >Search</a>
  </div>
</div>
<!-- aaa -->

<div data-role="page" data-dialog="true" data-mini="true" id="NearbySearch">
  <div data-role="header" data-position="fixed">
    <h5>Search places nearby</h5>
  </div>
                                                                        
<div data-role="content" class="ui-content">
<label for="nearbysearch">Nearby search requests:</label>
<input type="search" id="nearbysearch" placeholder="Restaurant" autocomplete="off" value="">
		<select name="radius" id="radius" data-native-menu="false">
		  <option data-placeholder="true">Radius</option>
          <option value="500" selected>radius &lt;0.5 km</option>
          <option value="1000">radius &lt;1 km</option>
          <option value="3000">radius &lt;3 km</option>
          <option value="50000">radius &gt;3 km</option>
		</select>
<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nsr" >Search</a>
  </div>
</div>

<div data-role="page" data-mini="true" id="poimap"> <!-- page -->
    <div data-role="content" class="ui-content">
       <div id="map" style="width:600px; height:400px;"></div>
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Back</a>
    </div>
</div> <!-- page -->

<div data-role="page" data-mini="true" id="poipath"> <!-- page -->
    <div data-role="content" class="ui-content">
       <div id="pmap" style="width:600px; height:400px;"></div>
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="poipathback">Back</a>
    </div>
</div> <!-- page -->

<!-- poicategories -->
  <div data-role="page" id="poicategorylist" data-position="fixed">
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='#Pois' class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-back ui-btn-icon-left ui-btn-icon-notext"></a>
<h6 style="font-size:83%;">Categories</h6>
<a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext'>Pages</a>
    </div>

    <div data-role="main" class="ui-content">
		<ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview">
			<li><a href='#poicategory' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Paid'>
			<input type='hidden' name='eid' id='eid' value='0'>
			<p style= font-size:90%;><strong>Create new category for places...</strong></p>
			</a></li>
		</ul>	
        <h3></h3>
        <ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview" id="poicategories">
          <li>empty</li>
        </ul>

    </div> 
  </div> 

  <div data-role="page" data-dialog="true" data-mini="true" id="poicategory">
  <div data-role="header" data-position="fixed">
    <h2>Theme</h2>
  </div>
    <div data-role="main" class="ui-content" id="mpoicategory">
	  <ul data-role="listview" id="poicategoryfields">
	  <li >
		<label for="Powner">Moderator:</label>
		<select name="Powner" id="Powner" data-native-menu="false">
<!--         <option data-placeholder="true">Moderator</option> -->
<?php
echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Ptheme">Theme:</label>
		<textarea name="Ptheme" id="Ptheme" placeholder="Text ..."></textarea>
        <input type='hidden' name='Pnid' id='Pnid'>
	  </li>
	  <li >
		<label for="Psubthemes">Cross reference themes:</label>
		<select name="Psubthemes[]" id="Psubthemes" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Themes</option>
		</select>
	  </li>
	  <li >
		<label for="Pallowgroups">Reading only groups:</label>
		<select name="Pallowgroups[]" id="Pallowgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Reading only groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Ppublishgroups">Publishing groups:</label>
		<select name="Ppublishgroups[]" id="Ppublishgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Publishing groups</option> -->
<?php
echo "<option value='{$param['email']}'>{$param['email']}</option>";    
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  </ul>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpoicategory">Delete POI category</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpoicategory">Change POI category</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npoicategory">Save new POI category</a>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
    </div>
  </div>
<!-- page ends -->


<!-- cyf_events.html -->
<div data-role="page" id="Events" data-position="fixed"> <!-- page -->
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<h6 style="font-size:83%;">Events</h6>
<a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-right ui-btn-icon-notext'>Pages</a>
  <div data-role="navbar">
	<ul>
	  <li>
		<select name="Efilters" id="Efilters" data-native-menu="false">
		  <option data-placeholder="true">Filters</option>
          <option value="all">All</option>
          <option value="lmonth">Last month</option>
          <option value="lweek">Last week</option>
          <option value="lday">Yesterday</option>
          <option value="tday">Today</option>
          <option value="nweek">Next week</option>
          <option value="nmonth">Next month</option>
          <option value="future">In the future</option>
          <option value="notime">Without time</option>
		</select>
	  </li>
      <li>
		<select class="ecategories" name="Efecategories[]" id="Efecategories" multiple="multiple" data-native-menu="false">
       <option data-placeholder="true">Subscriptions</option>
<?php
/*
$q="SELECT theme FROM subscriptions WHERE allowgroups='{}' OR allowgroups='{{$param['email']}}' OR allowgroups && '{$param['membergroups']}' OR allowgroups && '{$param['ownergroups']}';";
//echo $q."<br>";
$res = pg_query($cyf, $q);
if ($res) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
            echo "<option value='{$r['theme']}'>{$r['theme']}</option>";
    }
}
*/
?>
		</select>
	  </li>
    </ul>
  </div>
	</div>
	  
<div data-role="content" class="ui-content">
	<ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview">
		<li><a href='#newevent' class='ui-btn ui-btn-icon-right ui-icon-carat-r' data-user-id='0'>
		<h6>Create new event...</h6>
	</a>
	</li>
	</ul>
        <h3></h3>
        <ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview" id="events">
          <li>empty</li>
        </ul>

</div> 
<div data-role="footer" class="ui-bar" style="text-align:right;">
<a href='#subscrevent' data-role="button" data-icon="plus" class="ui-bar">Create/Edit Subscriptions</a>
</div>
</div>

<!-- page -->
<div data-role="page" data-dialog="true" data-mini="true" id="newevent"> 
  <div data-role="header" data-position="fixed">
    <h2 id="newpagetitle">Event</h2>
  </div>
  <div data-role="content" class="ui-content" id="ievent">
    <input type='hidden' name='Enid' id='Enid'>
	<ul data-role="listview" id="listevent">
	  <li >
		<label for="Eauthor">Author:</label>
		<select name="Eauthor" id="Eauthor" data-native-menu="false">
<!--     <option data-placeholder="true">Author</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Eagroups">for groups:</label>
		<select name="Eagroups[]" id="Eagroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Eevent">Name:</label>
		<textarea name="Eevent" id="Eevent" placeholder="Text ..."></textarea>
	  </li>
	  <li >
		<label for="Emsg">Description:</label>
		<textarea name="Emsg" id="Emsg" placeholder="Text ..."></textarea>
	  </li>
	  
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>date, time</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<fieldset class="ui-grid-b">
			  <div class="ui-block-a"><label for="Estime">begins:</label><input data-mini="true" type="date" name="Esdate" id="Esdate"><input data-mini="true" type="time" name="Estime" id="Estime"></div>
			  <div class="ui-block-b"><label for="Eetime">ends:</label><input data-mini="true" type="date" name="Eedate" id="Eedate"><input data-mini="true" type="time" name="Eetime" id="Eetime"></div>
    <div class="ui-block-c"></div>
			</fieldset>
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>location</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Eurl">URL:</label>
			<input type="url" name="Eurl" id="Eurl">
			<label for="Elocation">Address:</label>
			<textarea name="Elocation" id="Elocation"></textarea>
			<p><a href="#" class="ui-btn ui-btn-inline" id="getcoorev">Get coordinates</a></p>
			<label for="Elongt">Longitude:</label>
			<input type="number" step="any" name="Elongt" id="Elongt">
			<label for="Elat">Latitude:</label>
			<input type="number" step="any" name="Elat" id="Elat">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>contact</h2>
		<ul data-role="listview" data-theme="a">
		  <li>
			<label for="Econtact">contact:</label>
			<input type="text" name="Econtact" id="Econtact" placeholder="Name ...">
			<label for="Etel">tel:</label>
			<input type="tel" name="Etel" id="Etel" placeholder="tel ...">
			<label for="Eemail">email:</label>
			<input type="email" name="Eemail" id="Eemail" placeholder="email ...">
		  </li>
		</ul>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
		<h2>Subscriptions</h2>
		<select multiple="multiple" data-native-menu="false" name="Enecategories[]" id="Enecategories" data-mini="true">
<!--		  <option data-placeholder="true">Choose subscriptions</option> -->
		</select>
	  </li>
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing events ...</h2>
	<p style= "text-align:center; font-size:60%"><i>To change author and groups who can see the event<br>
		use <strong>Publish event</strong> after <strong>Save new event</strong></i></p>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="sevent">Publish event</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="devent">Delete event</a>
	  </li>
     
	  <li data-role="collapsible" data-iconpos="right" data-inset="false" data-collapsed="false">
    <h2>Only for private group <br> &quot;<?php echo $param['email'];?>&quot;</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cevent">Change event</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nevent">Save new event</a>
	  </li>
	</ul>
  </div>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
</div>

<div data-role="page" data-dialog="true" data-mini="true" id="showevent"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="poititle">Event</h2>
  </div>
  <div data-role="content" class="ui-content">
	<ul data-role="listview" data-theme="a">
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="mapevent">Show on map</a></li>
    <li><a href="#Pois" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r" id="pathevent">Show path</a></li>
    <li><a href="#newevent" class="ui-btn ui-btn-corner-all ui-shadow ui-btn-icon-right ui-icon-carat-r">View/Edit event</a></li>
    </ul>
 </div>
</div><!-- page ends -->

<!-- page Subscriptions -->
  <div data-role="page" id="subscrevent" data-position="fixed">
	<div data-role="header" data-position="fixed">
<!--
    <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h2><a href='<?php echo $self; ?>?page=Main' rel='external'><?php echo $provider; ?></a></h2>
	  <a href='<?php echo $self; ?>?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'><?php echo $lang; ?></a>
-->
<a href='#Events' class="ui-btn ui-btn-left ui-corner-all ui-shadow ui-icon-back ui-btn-icon-left ui-btn-icon-notext"></a>
<h6 style="font-size:83%;">Subscriptions</h6>
<a href='#MainPage' class='ui-btn ui-btn-right ui-corner-all ui-shadow ui-icon-home ui-btn-icon-left ui-btn-icon-notext'>Pages</a>
    </div>

    <div data-role="main" class="ui-content">
		<ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview">
			<li><a href='#subscription' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Said'>
			<h6>Create new subscription ...</h6>
			</a></li>
		</ul>
        <h3></h3>		
        <ul data-role="listview" data-inset="true" data-theme="d" data-divider-theme="d" class="ui-listview" id="subscriptions">
          <li>empty</li>
        </ul>

    </div> 
  </div> 

  <div data-role="page" data-dialog="true" data-mini="true" id="subscription">
  <div data-role="header" data-position="fixed">
    <h2>Subscription</h2>
  </div>
    <div data-role="main" class="ui-content" id="msubscription">
	  <ul data-role="listview" id="subscriptionfields">
	  <li >
		<label for="Sowner">Moderator:</label>
		<select name="Sowner" id="Sowner" data-native-menu="false">
<!--         <option data-placeholder="true">Moderator</option> -->
<?php
echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
//    if($k ==0) echo " selected";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Stheme">Theme:</label>
		<textarea name="Stheme" id="Stheme" placeholder="Text ..."></textarea>
        <input type='hidden' name='Snid' id='Snid'>
	  </li>
	  <li >
		<label for="Ssubthemes">Cross reference themes:</label>
		<select name="Ssubthemes[]" id="Ssubthemes" multiple="multiple" data-native-menu="false">
          <option data-placeholder="true">Themes</option>
		</select>
	  </li>
	  <li >
		<label for="Sallowgroups">Reading only groups:</label>
		<select name="Sallowgroups[]" id="Sallowgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Reading only groups</option> -->
<?php
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
		<label for="Spublishgroups">Publishing groups:</label>
		<select name="Spublishgroups[]" id="Spublishgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Publishing groups</option> -->
<?php
echo "<option value='{$param['email']}'>{$param['email']}</option>";    
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as &$v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'>{$w}</option>";
}
?>
		</select>
	  </li>
	  </ul>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dsubscription">Delete subscription</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="csubscription">Change subscription</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="nsubscription">Save new subscription</a>
<!--
	<a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back">Cancel</a>
 -->
    </div>
  </div>
<!-- page ends -->


</body>
</html>