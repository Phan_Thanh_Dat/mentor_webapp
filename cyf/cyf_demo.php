<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
function printTag($r){
    $tag = $r['tag']; 
    $class = isset($r['class']) ? $r['class'] : '';
    $id = isset($r['id']) ? "id='{$r['id']}'" : '';
    $nid = isset($r['id']) ? $r['id'] : '';
    $style = isset($r['style']) ? "style='{$r['style']}'" : '';
    $label = isset($r['label']) ? $r['label'] : '';
    $url = isset($r['url']) ? $r['url'] : '#';
    $target = isset($r['target']) ?  "target='{$r['target']}'" : '';
    $name = isset($r['target']) ? "name='{$r['target']}'" : '';
    $width = isset($r['width']) ? "width='{$r['width']}'" : 'width="425"';
    $height = isset($r['height']) ? "height='{$r['height']}'" : 'height="240"';
    if($tag == 'iframe'){
        echo "<div><iframe class='{$class}' {$id} {$style} {$width} {$height} {$name} src='{$url}'></iframe></div>";
    }else{
        if($tag == 'a'){
        echo "<a class='mybtn {$class}' {$id} {$style} {$target} href='{$url}' rel='external'>{$label}</a>";
        }else{
           echo "<br>unknown tag:";print_r($r);echo "<br>"; 
        }
    }
}

function evaluateJson($json){
    $r = json_decode($json,true);
    if(is_array($r)){
        if(isset($r['setting']) and isset($r['setting']['lang'])){
            $_SESSION['setting']['lang'] = $r['setting']['lang'];
        }else{
            if(isset($r['tag'])){
                printTag($r);
            }else{
                if(isset($r['linklist'])){
                    foreach ($r['linklist'] as $k => $v){
                        printTag($v);echo "<br>";
                    }
                }else{
                    echo "<br>unknown json: ";print_r($r);echo "<br>";
                }
            }
        }
    }else{
        echo "<br>Check json: ";print_r($r);echo "<br>";
    }
}
    
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$self=htmlspecialchars($_SERVER['PHP_SELF']);
//echo '<form id="f" method="POST" action="'.$self.'">';
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf = cyfConnect();

if(!isset($_REQUEST['page']) or ($_REQUEST['page']=='' )) $page = 'Main';
else $page = $_REQUEST['page'];

$lang = $_SESSION['setting']['lang'];
$provider = 'CYF Digital Services - Demo';

//if(!isset($_REQUEST['page']) or ($_REQUEST['page']=='Main' )){
echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<!--<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.icons-1.4.5.min.css">-->
<!--<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.inline-png-1.4.5.min.css">-->
<!--<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.inline-svg-1.4.5.min.css">-->
<!--<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.structure-1.4.5.min.css">-->
<!--<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.theme-1.4.5.min.css">-->
<!--<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile.external-png-1.4.5.min.css">-->
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<title>CYF Demo App</title>
<script>
$( document ).on( "vclick", "a.mybtn", function() {
//    $('#maincontent').load(this.href);
    document.getElementById('category').value=this.dataset.userPage;
    document.getElementById('g').submit();
    return false;

  });
</script>
<style>#main-page, #maincontent{ width: 100%; height: 100%; padding: 0; }</style>
EOT;

//}

$q="SELECT app as type,unnest(subcategories) as arg FROM categories WHERE category =".'$h$'.$page.'$h$'.";";
//echo $q."<br>";
$result = @pg_query($cyf, $q);
if ($result) {
    if($row=pg_fetch_assoc($result)){
        $type = $row['type'];
        if($type==='js') {
            $header = $row['arg']; //<link rel="stylesheet" href="../*.css"> item 1
            echo $header;
            $row=pg_fetch_assoc($result);
        }
        if($type==='php') {
            eval($row['arg']); //php for header item 1
//            echo $row['arg'];
            $row=pg_fetch_assoc($result);
        }
//if(!isset($_REQUEST['page']) or ($_REQUEST['page']=='Main' )){
        echo '</head><body>';
        echo "<form id='f' method='POST' action='{$self}'>";
        echo '<div data-role="page" id="main-page" data-url="main-page">';
        echo '<div data-role="header" data-position="fixed">';
        echo '<a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>';
        echo "<h2><a href='{$self}?page=Main' rel='external'>{$provider}</a></h2>";
        echo "<a href='{$self}?page=Settings' class='mybtn ui-btn ui-icon-gear ui-btn-icon-right' rel='external'>{$lang}</a></div>";
        echo '<div role="main" class="ui-content" id="maincontent">';
//}
        do{
            switch($type){
            case 'html':
                echo $row['arg'];
                break;
            case 'json':
                evaluateJson($row['arg']);
                break;
            case 'js':
                echo $row['arg']; //after <div role="main" class="ui-content" id="maincontent">
                break;
            case 'php':
                eval($row['arg']); //after <div role="main" class="ui-content" id="maincontent">
//                echo $row['arg']; //after <div role="main" class="ui-content" id="maincontent">
                break;
            case 'list':
                echo '<a class="mybtn ui-btn" href="'.$self."?";
                $data = array('page'=>$row['arg']);
                echo http_build_query($data);
                echo '" data-user-page="'.$row['arg'].'"';
                echo ' rel="external">'.$row['arg'].'</a>';
                break;
            case 'sql':
                evaluateJson($row['arg']);
                break;
            default:
                evaluateJson($row['arg']);
            }
        } while ($row=pg_fetch_assoc($result));
        echo '</div></form>';
    }else{
        echo '</head><body>'.$page."";
    }
}else{
    echo '</head><body>'.$q."";
}


//if(!isset($_REQUEST['page']) or ($_REQUEST['page']=='Main' )){

echo <<<EOT
<form method="post" action="../cyf/cyf_edit.php" target="cyf_edit" id="g">
<input type="hidden" id="category" name="category" value="{$page}">
</form>
EOT;

echo '</body></html>';

//}
?>