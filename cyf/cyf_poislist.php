<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYF Digital Services - POI list View Tool</title>
<style>
td {margin: 1px;padding-left: 6px;text-align:left;}
label {padding-left: 6px;font-style: italic;font-size: 1em;font-weight: normal;}
input[type=text]{
font-size: 0.875em;
width: 100%;
padding: 1px 1px;
margin: 1px 0;
box-sizing: border-box;
}
input[type=date]{
font-size: 0.875em;
width: 100%;
padding: 1px 1px;
margin: 1px 0;
box-sizing: border-box;
}
textarea{
font-size: 0.875em;
width: 100%;
padding: 1px 1px;
margin: 1px 0;
box-sizing: border-box;
}
input[type=submit]{
font-size: 0.875em;
width: 100%;
}
</style>
<title>CYF Users Control Tool</title>
<script>
function setParam(e){
 document.getElementById('poi').value=e.dataset.poiId;
 document.getElementById('g').submit();
 document.getElementById('f').submit();
}
function createMap(t,x,y){
//<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7975.738500054843!2d30.283773399999998
//!3d60.016158499999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1470964505656"  width="
//400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
var iframe = document.createElement('iframe');
iframe.allowfullscreen=1;
iframe.frameborder=0;
iframe.width=400;
iframe.height=300;
iframe.src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7975.738500054843!2d30.283773399999998!3d60.016158499999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1470964505656";
//iframe.src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7975.738500054843!2d" + x + "!3d" + y + " !2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1470964505656";
//document.body.appendChild(iframe);
t.appendChild(iframe);
}
</script>
</head>
<body>
<center>
<div style="width: 800px;">
EOT;
echo '<form id="f" target="cyf_poislist" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf = cyfConnect();

echo <<<EOT
<b><i>CYF Digital Services - POI list View Tool</i></b>
<div>{$lang}</div>
<div>
<a href="#" onclick="setParam(this)" data-user-app="list" data-user-arg="Main">
<span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
<a href="#" onclick="setParam(this)" data-user-app="list" data-user-arg="Settings">
<span class="glyphicon glyphicon-wrench"></span>&nbsp;Settings</a>
</div>
<div><i><b>{$page}</b></i></div>
EOT;

foreach ($_POST as $k => $v){
	$post[$k] = pg_escape_string(htmlspecialchars(stripslashes(trim($v))));
    echo "post $k = $post[$k] <br>";
}

if(isset($post['pcmd']) and ($post['pcmd'] =='Do it!') and isset($post['tcmd'])){
}

echo "POI list";
echo "<hr>";

$query = "SELECT id,poiname,description,opentime,closetime,availableday,address,latitude,longitude,tel,email,url,contract,categories as ctgr  FROM pois;";// WHERE category = '{$page}';";unnest(categories) as ctgr 
//    echo $query."<br>";
$result = @pg_query($cyf, $query);
if (!$result) $last_error = pg_last_error($cyf);
$firows = pg_num_rows($result);
if($firows >0){
    echo '<div class="list-group">';
    $fi=getValues($result);
    for ($j=0;$j<$firows;$j++){
        echo "<div class='list-group-item'>";
        echo "<a href='#'  onclick='setParam(this)' data-poi-id='".$fi[$j]['id']."'>{$fi[$j]['poiname']}</a>";
        echo " {$fi[$j]['description']}";
        echo "<br>open: {$fi[$j]['opentime']}-{$fi[$j]['closetime']} ({$fi[$j]['availableday']})";
        echo "<br> {$fi[$j]['address']} ";
        if($fi[$j]['latitude'] != '' and $fi[$j]['longitude'] != ''){
            echo "<a href='#' onclick='createMap(this,{$fi[$j]['latitude']},{$fi[$j]['longitude']})'>(latitude:{$fi[$j]['latitude']} longitude:{$fi[$j]['longitude']})</a>";
        }
        echo "<br>{$fi[$j]['tel']} ";
        echo "<br>{$fi[$j]['email']} ";
        echo "<br>{$fi[$j]['url']}";
//        echo "<p> {$fi[$j]['ctgr']}</p>";
        echo '</div>';
    }
    echo '</div>';
}
//echo "<input type='hidden' id='poi' name='poi' value='0'>";

echo '<hr>';

$stat = explode('DETAIL:',$last_error);
if(count($stat) == 2){
echo '<div><span class="glyphicon glyphicon-alert"></span>&nbsp;';
    echo $stat[1];
}else echo $last_error;
echo '</div>';

echo <<<EOT
<!--<div><a href="../cis/login.php"><span class="glyphicon glyphicon-off"></span>&nbsp;Exit</a></div>
</div>
<div><a href="#" onclick="document.getElementById('mcmd').value='ClearVar';document.getElementById('f').submit();"><span class="glyphicon glyphicon-wrench"></span>&nbsp;Clear Variables (Testing)</a></div>
<div>&copyCYF Digital Services - POI list View Tool</div><br>-->
</form>
</div>
</center>
<form method="post" action="../cyf/cyf_poisedit.php" target="cyf_poisedit" id="g">
<input type="hidden" id="poi" name="poi" value="">
</form>
</body>
</html>
EOT;
?>
