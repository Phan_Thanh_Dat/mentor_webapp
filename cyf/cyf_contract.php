<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

switch($_REQUEST['eaction']){
    case 'Dcontract':
        $q ="DELETE FROM contracts WHERE id={$_REQUEST['eid']}";
        $q.=" AND (";        
        $q.=" manager='{$param['email']}'";        
        $q.=" OR ARRAY[manager] && '{$param['ownergroups']}'";        
//        $q.=" OR ARRAY[manager] && '{$param['membergroups']}'";        
        $q.=");";        
        break;
    case 'Ccontract':
        $q = "UPDATE contracts SET ";
        if(!empty($_REQUEST['cname'])) $q.= "cname='".htmlspecialchars($_REQUEST['cname'])."'";
        if(!empty($_REQUEST['bname'])) $q.= ",bname='".htmlspecialchars($_REQUEST['bname'])."'";
        if(!empty($_REQUEST['bid'])) $q.= ",bid='".htmlspecialchars($_REQUEST['bid'])."'";
        if(!empty($_REQUEST['osec'])) $q.= ",osec='".htmlspecialchars($_REQUEST['osec'])."'";
        if(!empty($_REQUEST['address'])) $q.= ",address='".htmlspecialchars($_REQUEST['address'])."'";
        if(!empty($_REQUEST['contact'])) $q.= ",contact='".htmlspecialchars($_REQUEST['contact'])."'";
        if(!empty($_REQUEST['tel'])) $q.= ",tel='".htmlspecialchars($_REQUEST['tel'])."'";
        if(!empty($_REQUEST['url'])) $q.= ",url='".htmlspecialchars($_REQUEST['url'])."'";
        if(!empty($_REQUEST['lang'])) $q.= ",lang='".htmlspecialchars($_REQUEST['lang'])."'";
        if(!empty($_REQUEST['status'])) $q.= ",status='".htmlspecialchars($_REQUEST['status'])."'";
        if(!empty($_REQUEST['dstart'])) $q.= ",dstart='".htmlspecialchars($_REQUEST['dstart'])."'";
        if(!empty($_REQUEST['dend'])) $q.= ",dend='".htmlspecialchars($_REQUEST['dend'])."'";
        if(!empty($_REQUEST['maxusers'])) $q.= ",maxusers='".htmlspecialchars($_REQUEST['maxusers'])."'";
        if(!empty($_REQUEST['contractgroups']))
            $q.= ",contractgroups='".$_REQUEST['contractgroups']."'";
        if(!empty($_REQUEST['usersgroups']))
            $q.= ",usersgroups='".$_REQUEST['usersgroups']."'";
        if(!empty($_REQUEST['apps']))
            $q.= ",apps='".$_REQUEST['apps']."'";
        if(!empty($_REQUEST['manager']))
            $q.= ",manager='".htmlspecialchars($_REQUEST['manager'])."'";
        if(!empty($_REQUEST['email'])) $q.= ",email='".htmlspecialchars($_REQUEST['email'])."'";
//        if(!empty($_REQUEST['pass'])) $q.= "pass='".htmlspecialchars($_REQUEST['pass'])."'";
        $q.= " WHERE id=".$_REQUEST['eid'];
        $q.= " AND (";
        $q.= " ARRAY[manager] && '".$param['ownergroups']."'";
//        $q.= " OR ARRAY[manager] && '".$param['membergroups']."'";
        $q.= " OR manager='".$param['email']."'";
        $q.= ");";
            echo "<li>".$q."</li>";
        $res = pg_query($cyf, $q);
//        exit();
        break;
    case 'Ncontract':
        if(!empty($_REQUEST['manager'])) $i['manager'] = "'".htmlspecialchars($_REQUEST['manager'])."'";
        if(!empty($_REQUEST['cname'])) $i['cname'] = "'".htmlspecialchars($_REQUEST['cname'])."'";
        if(!empty($_REQUEST['bname'])) $i['bname'] = "'".htmlspecialchars($_REQUEST['bname'])."'";
        if(!empty($_REQUEST['bid'])) $i['bid'] = "'".htmlspecialchars($_REQUEST['bid'])."'";
        if(!empty($_REQUEST['osec'])) $i['osec'] = "'".htmlspecialchars($_REQUEST['osec'])."'";
        if(!empty($_REQUEST['address'])) $i['address'] = "'".htmlspecialchars($_REQUEST['address'])."'";
        if(!empty($_REQUEST['contact'])) $i['contact'] = "'".htmlspecialchars($_REQUEST['contact'])."'";
        if(!empty($_REQUEST['tel'])) $i['tel'] = "'".htmlspecialchars($_REQUEST['tel'])."'";
        if(!empty($_REQUEST['url'])) $i['url'] = "'".htmlspecialchars($_REQUEST['url'])."'";
        if(!empty($_REQUEST['lang'])) $i['lang'] = "'".htmlspecialchars($_REQUEST['lang'])."'";
        if(!empty($_REQUEST['status'])) $i['status'] = "'".htmlspecialchars($_REQUEST['status'])."'";
        if(!empty($_REQUEST['dstart'])) $i['dstart'] = "'".htmlspecialchars($_REQUEST['dstart'])."'";
        if(!empty($_REQUEST['dend'])) $i['dend'] = "'".htmlspecialchars($_REQUEST['dend'])."'";
        if(!empty($_REQUEST['maxusers'])) $i['maxusers'] = "'".htmlspecialchars($_REQUEST['maxusers'])."'";
        if(!empty($_REQUEST['contractgroups']))
            $i['contractgroups'] = "'".htmlspecialchars($_REQUEST['contractgroups'])."'";
        if(!empty($_REQUEST['usersgroups']))
            $i['usersgroups'] = "'".htmlspecialchars($_REQUEST['usersgroups'])."'";
        if(!empty($_REQUEST['apps']))
            $q.= ",apps='".$_REQUEST['apps']."'";
        if(!empty($_REQUEST['manager']))
            $i['manager'] = "'".htmlspecialchars($_REQUEST['manager'])."'";
        if(!empty($_REQUEST['email'])) $i['email'] = "'".htmlspecialchars($_REQUEST['email'])."'";
        pgInsert($cyf, 'contracts', $i);
        break;
    case 'Jparam':
        $curcount = 0;
        $q = "SELECT sum(maxusers) as curcount FROM contracts";
        $q.= " WHERE";
        $q.= " manager='{$param['email']}'";
        $q.= " OR ARRAY[manager] && '{$param['ownergroups']}'";
//        $q.= " OR ARRAY[manager] && '{$param['membergroups']}'";
        $q.= ";";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            while (($r=pg_fetch_assoc ( $res ))) $curcount+=$r['curcount'];
        }
//ordinal users?
        $ocurcount = 0;
        $q = "SELECT count(*) as ocurcount FROM users";
        $q.= " WHERE manager='{$param['email']}'";
        $q.= " OR ARRAY[manager] && '{$param['ownergroups']}'";
//        $q.= " OR ARRAY[manager] && '{$param['membergroups']}'";
        $q.= ";";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            while (($r=pg_fetch_assoc ( $res ))) $ocurcount+=$r['ocurcount'];
        }
//maxcount users
        $maxcount = 0;
        $q = "SELECT sum(maxusers) as maxcount FROM contracts";
        $q.= " WHERE email='{$param['email']}'";
//        $q.= " OR contractgroups && '{$param['ownergroups']}';";
//        $q.= " OR contractgroups && '{$param['membergroups']}';";
        $q.= ";";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            while (($r=pg_fetch_assoc ( $res ))) $maxcount+=$r['maxcount'];
        }
        $tcurr=$maxcount-$ocurcount-$curcount;
        echo '{"musers":'.$maxcount.',"cusers":'.$curcount.',"ousers":'.$ocurcount.'}';
        exit();
    case 'Gcontract':
//get contract by $_REQUEST['email']
        $q ="SELECT id,cname,bname,address,bid,osec,contact,lang,tel,email,url,manager,status,maxusers";
        $q .=",to_char(dstart,'YYYY-MM-DD') as dstart,to_char(dend,'YYYY-MM-DD') as dend";
        $q .=",array_to_string(contractgroups,'~^~') as cgr";
        $q .=",array_to_string(usersgroups,'~^~') as ugr";
        $q .=",array_to_string(apps,'~^~') as capps";
        $q .=" FROM contracts";
        $q .=" WHERE email='".htmlspecialchars($_REQUEST['email'])."'";
        $q .=" ORDER BY bname;";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $d='[';
            while (($r=pg_fetch_assoc ( $res ))){
                echo $d;
                $cgr= json_encode(explode('~^~',$r['cgr'] ));
                $ugr= json_encode(explode('~^~',$r['ugr'] ));
                $apps= json_encode(explode('~^~',$r['capps'] ));
                echo '{"id":"'.$r['id'].'"';
                echo ',"cname":"'.$r['cname'].'","bname":"'.$r['bname'].'","address":"'.$r['address'].'"';
                echo ',"bid":"'.$r['bid'].'","osec":"'.$r['osec'].'"';
                echo ',"contact":"'.$r['contact'].'","tel":"'.$r['tel'].'"';
                echo ',"lang":"'.$r['lang'].'"';
                echo ',"email":"'.$r['email'].'","url":"'.$r['url'].'","manager":"'.$r['manager'].'"';
                echo ',"dstart":"'.$r['dstart'].'","dend":"'.$r['dend'].'","maxusers":"'.$r['maxusers'].'"';
                echo ',"contractgroups":'.$cgr.',"usersgroups":'.$ugr.',"apps":'.$apps;//.'';
                echo ',"status":"'.$r['status'].'"}';
                $d=',';
            }
            echo ']';
        }else{
            echo '[{"id":"0","str":"'.htmlspecialchars($q).'"}]';
        }
        exit();
        break;
    case 'Jcontract':
        $q ="SELECT id,cname,bname,address,bid,osec,contact,lang,tel,email,url,manager,status,maxusers";
        $q .=",to_char(dstart,'YYYY-MM-DD') as dstart,to_char(dend,'YYYY-MM-DD') as dend";
        $q .=",array_to_string(contractgroups,'~^~') as cgr";
        $q .=",array_to_string(usersgroups,'~^~') as ugr";
        $q .=",array_to_string(apps,'~^~') as capps";
        $q .=" FROM contracts";
        $q .=" WHERE  id={$_REQUEST['eid']}";
        $q .=" AND (";
        $q .=" manager='{$param['email']}'";
        $q .=" OR ARRAY[manager] && '{$param['ownergroups']}'";
//        $q .=" OR ARRAY[manager] && '{$param['membergroups']}'";
        $q .=")";
        $q .=" ORDER BY bname;";
        //echo "<br>".$q."<br>";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $r=pg_fetch_assoc($res);
            $cgr= json_encode(explode('~^~',$r['cgr'] ));
            $ugr= json_encode(explode('~^~',$r['ugr'] ));
            $apps= json_encode(explode('~^~',$r['capps'] ));
            echo '{"id":"'.$r['id'].'"';
            echo ',"cname":"'.$r['cname'].'","bname":"'.$r['bname'].'","address":"'.$r['address'].'"';
            echo ',"bid":"'.$r['bid'].'","osec":"'.$r['osec'].'","contact":"'.$r['contact'].'","tel":"'.$r['tel'].'"';
            echo ',"lang":"'.$r['lang'].'"';
            echo ',"email":"'.$r['email'].'","url":"'.$r['url'].'","manager":"'.$r['manager'].'"';
            echo ',"dstart":"'.$r['dstart'].'","dend":"'.$r['dend'].'","maxusers":"'.$r['maxusers'].'"';
            echo ',"contractgroups":'.$cgr.',"usersgroups":'.$ugr.',"apps":'.$apps;//.'';
            echo ',"status":"'.$r['status'].'"}';
        }else{
            echo '{"id":"0"}';
        }
        exit();
    case 'Jcontracts':
        $q ="SELECT id,cname,bname,address,bid,osec,contact,lang,tel,email,url,manager,status,maxusers";
        $q .=",to_char(dstart,'YYYY-MM-DD') as dstart,to_char(dend,'YYYY-MM-DD') as dend";
        $q .=",array_to_string(contractgroups,'~^~') as cgr";
        $q .=",array_to_string(usersgroups,'~^~') as ugr";
        $q .=",array_to_string(apps,'~^~') as capps";
        $q .=" FROM contracts";
        $q .=" WHERE manager='{$param['email']}'";
        $q .=" OR ARRAY[manager] && '{$param['ownergroups']}'";
//        $q .=" OR ARRAY[manager] && '{$param['membergroups']}'";
        $q .=" ORDER BY bname;";
//echo "<br>".$q."<br>";
        $res = pg_query($cyf, $q);
        if ($res and pg_num_rows($res) >0) {
            $r = array();
            $d='[';
            while (($r=pg_fetch_assoc ( $res ))){
                echo $d;
                $cgr= json_encode(explode('~^~',$r['cgr'] ));
                $ugr= json_encode(explode('~^~',$r['ugr'] ));
                $apps= json_encode(explode('~^~',$r['capps'] ));
                echo '{"id":"'.$r['id'].'"';
                echo ',"cname":"'.$r['cname'].'","bname":"'.$r['bname'].'","address":"'.$r['address'].'"';
                echo ',"bid":"'.$r['bid'].'","osec":"'.$r['osec'].'","contact":"'.$r['contact'].'","tel":"'.$r['tel'].'"';
                echo ',"lang":"'.$r['lang'].'"';
                echo ',"email":"'.$r['email'].'","url":"'.$r['url'].'","manager":"'.$r['manager'].'"';
                echo ',"dstart":"'.$r['dstart'].'","dend":"'.$r['dend'].'","maxusers":"'.$r['maxusers'].'"';
                echo ',"contractgroups":'.$cgr.',"usersgroups":'.$ugr.',"apps":'.$apps;//.'';
                echo ',"status":"'.$r['status'].'"}';
                $d=',';
            }
            echo ']';
        }else{
            echo '[{"id":"0"}]';
        }
        exit();
    default:
}

$q="SELECT id,cname,bname,address,bid,osec,contact,lang,tel,email,url,manager";
$q.=",to_char(dstart,'YYYY-MM-DD') as dstart,to_char(dend,'YYYY-MM-DD') as dend";
$q.=",status,maxusers,contractgroups,usersgroups,apps";
$q.=" FROM contracts";
$q.=" WHERE manager='{$param['email']}'";
$q.=" OR ARRAY[manager] && '".$param['ownergroups']."'";
//$q.=" OR ARRAY[manager] && '".$param['membergroups']."'";
$q.=" ORDER BY cname;";

$res = pg_query($cyf, $q);
if ($res and pg_num_rows($res) >0) {
    $r = array();
    while (($r=pg_fetch_assoc ( $res ))){
//        echo "<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>";
        echo "<li><a href='#newcontract' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Caid'";
        echo " data-user-id='{$r['id']}' data-user-cname='{$r['cname']}'>";
        echo "<p><b style='font-size:130%;'>{$r['cname']} </b> (<i>{$r['contact']}</i>)</p></a></li>";
    }
//        echo "<p>{$q}</p></a></li>";
}else{
//        echo "<li data-role='list-divider' role='heading' class='ui-li ui-divider ui-bar-d'></li>";
        echo "<li><a href='#newcontract' class='ui-btn ui-btn-icon-right ui-icon-carat-r' id='Caid'";
        echo " data-user-id='0' data-user-cname='New subcontract'>";
        echo "<p>{$q}</p></a></li>";
}
?>