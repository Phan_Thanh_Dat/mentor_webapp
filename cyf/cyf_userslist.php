<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Users Control Tool</title>
<style>
tr:nth-child(even) {background-color: #f2f2f2}
tr:hover {background-color: #ddd;}
td {
padding-left:6px;
text-align:left;
}
</style>
<script>
EOT;

echo "function setParam(e){";
if(!empty($_REQUEST['frame'])){
   echo "document.getElementById('gid').value=e.dataset.id;";
//   echo "document.getElementById('fmanager').value='{$_REQUEST['fmanager']}';";
   echo "document.getElementById('{$_REQUEST['frame']}').submit();";
}
echo "}";
echo "</script></head><body><center><table>";

//echo "post: ";print_r($_POST);echo "<br>";

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf=cyfConnect();

$fmanager = $_REQUEST['fmanager'];

$query = "SELECT id,email,contact,address,tel,lang,dstart,dend,cname,status,membergroups FROM users WHERE manager='{$fmanager}'";

//if($_POST['usefilters']=='usefilters'){
    $qs='';
    $filters = array();
//    if(!empty($_POST['fmanager']))
//        $filters[] = "manager='" . pg_escape_string($_POST['fmanager']) ."'";
    if(!empty($_REQUEST['fcontact']))
       $filters[] = "contact LIKE '%" . pg_escape_string($_REQUEST['fcontact']) ."%'";
    if(!empty($_REQUEST['faddress']))
       $filters[] = "address LIKE '%" . pg_escape_string($_REQUEST['faddress']) ."%'";
    if(!empty($_POST['femail']))
        $filters[] = "email LIKE '%" . pg_escape_string($_POST['femail']) ."%'";
    if(!empty($_REQUEST['flang']))
       $filters[] = "lang LIKE '%" . pg_escape_string($_REQUEST['flang']) ."%'";
    if(!empty($_REQUEST['ftel']))
       $filters[] = "tel LIKE '%" . pg_escape_string($_REQUEST['ftel']) ."%'";
    if(!empty($_REQUEST['fdstart']))
       $filters[] = "dstart <= '" . pg_escape_string($_REQUEST['fdstart']) ."'";
    if(!empty($_REQUEST['fdend']))
       $filters[] = "dend <= '" . pg_escape_string($_REQUEST['fdend']) ."'";
    if(!empty($_REQUEST['fstatus']))
       $filters[] = "status = '" . pg_escape_string($_REQUEST['fstatus']) ."'";
    if(!empty($_POST['fmembergroups']))
        $filters[] =  "'{".pg_escape_string($_POST['fmembergroups'])."}' && membergroups";
    $qs = implode(' AND ',$filters);
    if($qs != '') $query .= " AND " . $qs;
//}
$query .= ' ORDER BY contact;';
//echo $query; echo '<br>';
$result = @pg_query($cyf, $query);
if (!$result) $last_error = pg_last_error($cyf);
else{
    $firows = pg_num_rows($result);
    if($firows >0){
        $fi=getValues($result);
        for ($j=0;$j<$firows;$j++){
            echo "<tr onclick='setParam(this)' data-id='{$fi[$j]['id']}'>";
            echo "<td>{$fi[$j]['contact']}</td>";
            echo "<td>{$fi[$j]['email']}</td>";
            echo "<td>{$fi[$j]['membergroups']}</td></tr>";
        }
    }
}

echo "</table></center>";
if(!empty($_POST['frame']) and !empty($_POST['action'])){
   echo "<form method='post' action='{$_POST['action']}' target='{$_POST['frame']}' id='{$_POST['frame']}'>";
   echo "<input type='hidden' id='gid' name='gid'>";
   echo "<input type='hidden' id='fmanager' name='fmanager' value='{$fmanager}'></form";
}

echo <<<EOT
</body></html>
EOT;
?>