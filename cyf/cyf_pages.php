<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

$cyf = cyfConnect();
$self=htmlspecialchars($_SERVER['PHP_SELF']);
$provider= empty($_REQUEST['provider']) ? 'CYF Digital Services' : $_REQUEST['provider'];
$provider = $provider.' - Pages';
//$lang= empty($_REQUEST['lang']) ? 'en' : $_REQUEST['lang'];
$lang= $param['lang'];
$page= empty($_REQUEST['page']) ? 'Main' : $_REQUEST['page'];
//<a href='../cyf/cyf_pages.php?page=INFO&provider=CYF+Digital+Services' rel='external'></a>
?>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
<script src="../cis/jquery-2.1.4.min.js"></script>
<script src="../cis/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script>
    var basepage = "<?php echo $page; ?>";
function Enhance($page){		// Enhance the listview we just injected.
        $page.find( ":jqmData(role=listview)" ).listview().listview("refresh");
        $page.find( ":jqmData(role=collapsibleset)" ).collapsibleset();//.collapsibleset("refresh" );
        $page.find( ":jqmData(role=collapsible)" ).collapsible();//.collapsibleset("refresh" );
        $page.find( ":jqmData(role=selectmenu)" ).selectmenu().selectmenu( "refresh" );
};
//$(document).ready(
$( document ).on( "pagecreate",
    function(){
    getPage(basepage);
//    alert(basepage);
//        var p= $.param( {opt:'fpage',page:'INFO',eaction:'Fpage'} );
//        $("#npages").load("../cyf/cyf_page.php", p);
        return false;
});
$( document ).on( "vclick", "a#dpage", function() {
    var p= $.param( {
            opt: 'li',
            pgid: $('#Inpid').val(),
//            descr:   $('#Itpage').val(),
            eaction: 'Dpage',
            owner:  $('select#Ipauthor').val()
            } );
//    alert("param: "+p);
    $("#npages").load("../cyf/cyf_page.php", p);
    return true;
});
$( document ).on( "vclick", "a#spage", function() {
    var p= $.param( {
            opt: 'li',
            pgid: $('#Inpid').val(),
            eaction: 'Spage',
            owner:  $('select#Ipauthor').val(),
            pgroups: '{'+($("select#Ipgroups").val()!=null ? $("select#Ipgroups").val().join() :"")+'}'
            } );
//    alert("param: "+p);
    $("#npages").load("../cyf/cyf_page.php", p);
    return false;
});
function ChangePage(){
    var p= $.param( {
            opt: 'li',
            pgid: $('#Inpid').val(),
            eaction: 'Cpage',
            etype:  $('select#Iptype').val(),
            descr:   $('#Itpage').val(),
            items: items
            } );
    alert("pgid: "+$('#Inpid').val());
    alert("etype: "+$('select#Iptype').val());
    alert("descr: "+$('#Itpage').val());
    alert("items: "+items[0]);
    $.ajax({type: "POST",url:"../cyf/cyf_page.php",data: p,
            dataType:"json",
            success: function(msg){
    alert("msg: "+msg);
        }
            });
};
$( document ).on( "vclick", "a#cpage", function() {
//    var p= $.param( {
//            opt: 'li',
//            pgid: $('#Inpid').val(),
//            eaction: 'Cpage',
//            etype:  $('select#Iptype').val(),
//            descr:   $('#Itpage').val(),
//            items: items
//            } );
    var p= {
            opt: 'li',
            pgid: $('#Inpid').val(),
            eaction: 'Cpage',
            etype:  $('select#Iptype').val(),
            descr:   $('#Itpage').val(),
            items: items
            };
//    alert("items: "+items);
//    alert("param: "+p);
    $("#npages").load("../cyf/cyf_page.php", p);
//    ChangePage();
    return false;
});
$( document ).on( "vclick", "a#npage", function() {
    var p= {
            opt: 'li',
            etype:  $('select#Iptype').val(),
            descr:   $('#Itpage').val(),
            eaction: 'Npage',
            items: items 
            };
//    alert("param: "+p);
    $("#npages").load("../cyf/cyf_page.php", p);
    return false;
});
var pageId = 0;
var nextId = 0;
$( document ).on('change',".items", function(){
	var Ind = $(this).data('userIndex');
	items[Ind] = $(this).val();
	$( "#items" ).empty();
	for(var i=0; i< items.length; i++){
        h = "<div data-role='collapsible' data-mini='true' data-collapsed-icon='gear' data-expanded-icon='edit'><h3>Item"+i+"</h3><p><textarea class='items' data-user-index='"+i+"'>"+items[i]+"</textarea></p></div>";
		$( "#items" ).append( h ).collapsibleset( "refresh" );
	}
});

$( document ).on( "vclick", "#additem", function() {
h = "<div data-role='collapsible' data-mini='true' data-collapsed-icon='gear' data-expanded-icon='edit' id='item" + nextId + "'><h3>Item"+nextId+"</h3><p><textarea class='items' data-user-index='"+nextId+"'>Item"+nextId+"</textarea></p></div>";
$( "#items" ).append( h ).collapsibleset( "refresh" );
items.push("Item"+nextId);
        nextId++;
    var it="";
    var del = "{";
    for(var i=0; i< items.length; i++){
        it += del + '"'+ items[i] + '"';
        del = ","
    }
    it +="}";
//    alert(it);
});
$( document ).on( "vclick", "#removelastitem", function() {
    $("#items").children(":last").remove();//collapsible( "destroy" );
	items.pop();
    nextId--;
    var it="";
    var del = "{";
    for(var i=0; i< items.length; i++){
        it += del + '"'+ items[i] + '"';
        del = ","
    }
    it +="}";
//    alert(it);
});
$( document ).on( "vclick", "a#Igid", function() {
    $.mobile.changePage('#newpage');

    if($(this).data('userId')!=0){
    var p= $.param( {
            opt: 'json',
            eaction: 'Jpage',
            page:   $(this).data('userPage')
            } );

    $.ajax({url: "../cyf/cyf_page.php",data: p,
            success: function(msg){
//            alert(msg);
			if(msg.length>0){
                var pg =   JSON.parse(msg);

                $('#newpagetitle').text(pg.title);
                $("select#Ipauthor").val(pg.owner);
                $('select#Ipauthor').selectmenu().selectmenu('refresh',true);  
                $("select#Ipgroups").val(pg.sgroups);
                $('select#Ipgroups').selectmenu().selectmenu('refresh',true);  
                $('select#Iptype').val(pg.type);
                $('select#Iptype').selectmenu('refresh',true);
                $('#Inpid').val(pg.id);
//            alert(pg.id);
                $('#Itpage').val(pg.page);
                $( "#items" ).empty();
                nextId = pg.args.length;
                var h = "";
                items = [];
//                var u='';
//                var s='';
                for(var i=0; i< pg.args.length; i++){
                    items.push(pg.args[i]);
                    h="<div data-role='collapsible' data-mini='true' data-collapsed-icon='gear' data-expanded-icon='edit'><h3>Item"+i+"</h3><p><textarea class='items' data-user-index='"+i+"'>"+pg.args[i]+"</textarea></p></div>";
                    $( "#items" ).append( h ).collapsibleset( "refresh" );
                }
            }
        }});
    }
    return false;
});
function getPage(nPage) {// String: page Name
    var p= $.param( {opt: 'json',eaction: 'Jpage',page:nPage} );
    $.ajax({url: "../cyf/cyf_page.php",data: p,
            success: function(msg){
			if(msg.length>0){
                var pg = JSON.parse(msg);
                if(pg.id>0){
                    switch(pg.type){
                    case 'app':
                        window.location.replace(pg.args[0]);
                        break;
                    case 'DialogPage':
                        $( "#dialogpagetitle" ).text(pg.title);
                        var h='';
                        for(var i=0; i< pg.args.length; i++){
                            h+=pg.args[i];
                        }
                        $( "#dialogpagebody").empty().html( h );
                        $( "#DialogPage" ).page();
                        var $page = $( "#DialogPage" );
                        //var $header = $page.children( ":jqmData(role=header)" );
                        //var $content = $page.children( ":jqmData(role=content)" );//role=main
                        //var $footer = $page.children( ":jqmData(role=footer)" );
//                        $content.html( h );
//                        $page.page();
                        Enhance($page);// Enhance the listview we just injected.
                        $.mobile.changePage( "#DialogPage");//, options );
                        break;
                    case 'SinglePage':
                        var h='';
                        for(var i=0; i< pg.args.length; i++){
                            h+=pg.args[i];
                        }
                        $( "#SinglePage").empty().html( h );
                        $( "#MainPage" ).page();
                        var $page = $( "#MainPage" );
                        //var $header = $page.children( ":jqmData(role=header)" );
                        //var $content = $page.children( ":jqmData(role=content)" );//role=main
                        //var $footer = $page.children( ":jqmData(role=footer)" );
                        // Inject the category items markup into the content element.
//                        $content.html( h );
//                        $page.page();
                        Enhance($page);// Enhance the listview we just injected.
                        // Now call changePage() and tell it to switch to the page we just modified.
                        $.mobile.changePage( "#MainPage" );//, options );
                        break;
                    case 'html':
                        var $id="#Item0";
                        for(var i=0; i< pg.args.length; i++){
                            var item = "#Item"+i;
                            var $page = $( item );
                            var o = $( pg.args[i] );
                            $page.replaceWith( o );
                            $page = o;
                            $page.page();
                            Enhance($page);// Enhance the listview we just injected.
                        }
                        $.mobile.changePage( "#Item0" );
                        return false;
                        break;
//                    case 'html1':
//                        var $id="#Item0";
//                        for(var i=0; i< pg.args.length; i++){
//                            var item = "#Item"+i;
//                            var $page = $( item );
//                            var o = $( pg.args[i] );
//                            $page.replaceWith( o );
//                            $page = o;
//                            o.page();
//                            if(i==0) {
//                                $id = o.attr("id");
//                                alert($id);
//                            }
//                            Enhance($page);// Enhance the listview we just injected.
//                        }
//                        $.mobile.changePage( "#"+$id);
//                        return false;
//                        break;
                    case 'list':
                    default:
                        var h='';
                        for(var i=0; i< pg.args.length; i++){
h+='<li class="ui-li-has-alt ui-li-has-thumb">';
h+='<a href="#" class="ui-btn" data-user-id="'+pg.id+'" data-user-page="'+pg.args[i]+'" id="Igvid">';
//'<img src="../_assets/img/album-p.jpg"/>';
h+='<h2>'+pg.args[i]+'</h2><p></p></a>';
h+='<a href="#newpage" data-rel="popup" data-position-to="window" data-transition="pop"';
h+=' aria-haspopup="true" aria-owns="purchase" aria-expanded="false" class="ui-btn ui-btn-icon-notext';
h+=' ui-icon-gear ui-btn-a" title="Edit page" data-user-id="'+pg.id+'"';
h+=' data-user-page="'+pg.args[i]+'" id="Igid"></a>';           
h+='</li>';
                        }
                        $( "#npages" ).empty().append( h );//.collapsibleset( "refresh" );
                    }
                }
            }
        }
    });
};
$( document ).on( "vclick", "a#Igvid", function() {
    if($(this).data('userId')!=0){
        var ret = getPage($(this).data('userPage'));
        return ret;
    }
    return true;
});
</script>
</head>
<body id="MultiPage">
<!-- cyf_pages.php -->	
<form  id="f" method="POST" action="<?php echo $self; ?>">
<input type='hidden' id='provider' value="<?php echo $provider; ?>">


<!-- ---------------------------------------------- -->
<div data-role="page" data-position="fixed" data-mini="true" id="MainPage">
  <div data-role="header" data-position="fixed">
	  <a href="../cis/login.php?is_exit=0" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-icon-notext" rel="external">Exit</a>
	  <h6 style="font-size:83%;"><a href='<?php echo $self; ?>?' rel='external'><?php echo $provider; ?></a></h6>
  <a href='#User' class='mybtn ui-btn ui-icon-user ui-btn-icon-right ui-btn-icon-notext' id="user"></a> 
</div>
<div data-role="content" class="ui-content" id="SinglePage">
<ul data-role="listview" data-split-icon="gear" data-split-theme="a" data-inset="false" class="ui-listview" id="npages">
          <li>empty</li>
    </ul>
</div> 
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="DialogPage">
  <div data-role="header" data-position="fixed">
    <h2 id="dialogpagetitle">DialogPage</h2>
  </div>
  <div data-role="content" class="ui-content" id="dialogpagebody">
  </div>
  <div data-role="footer" data-position="fixed">
  </div>
</div>
    
<div data-role="page" id="Item0">
</div>
<div data-role="page" id="Item1">
</div>
<div data-role="page" id="Item2">
</div>
<div data-role="page" id="Item3">
</div>
<div data-role="page" id="Item4">
</div>
<div data-role="page" id="Item5">
</div>
<div data-role="page" id="Item6">
</div>
<div data-role="page" id="Item7">
</div>
    
<div data-role="page" data-dialog="true" data-mini="true" id="newpage"> <!-- page -->
  <div data-role="header" data-position="fixed">
    <h2 id="newpagetitle">Page</h2>
  </div>
  <div data-role="main" class="ui-content" id="ipage">
    <input type='hidden' name='Igid' id='Igid'>
	<ul data-role="listview" id="listpage">
	  <li data-mini="true" >
		<label for="Ipauthor">Owner:</label>
		<select name="Ipauthor" id="Ipauthor" data-native-menu="false">
<!--     <option data-placeholder="true">Author</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
	  </li>
	  <li >
    <input type="hidden" name="Inpid" id="Inpid">
	<label for="Itpage">Page title:</label>
    <input type="text" name="Itpage" id="Itpage">
	  </li>
	  <li>
		<select name="Iptype" id="Iptype" data-native-menu="false">
		  <option data-placeholder="true">Type</option>
          <option value="list">List items</option>
          <option value="SinglePage">Single Page</option>
          <option value="DialogPage">Dialog Page</option>
          <option value="html">Raw pages</option>
          <option value="app">Internal app</option>
<!--
          <option value="json">json</option>
          <option value="php">php</option>
          <option value="js">js</option>
-->
		</select>
	  </li>

	  <li data-role="collapsible" data-iconpos="right" data-inset="false" data-mini="true" data-collapsed-icon='edit' data-expanded-icon='edit' id="eitems">

<button type="button" data-icon="plus" data-iconpos="right" data-mini="true" data-inline="true" id="additem">Push item</button>
<button type="button" data-icon="minus" data-iconpos="right" data-mini="true" data-inline="true" id="removelastitem">Pop item</button>
<!--<button type="button" data-icon="recycle" data-iconpos="right" data-mini="true" data-inline="true" id="saveitems">Save</button>-->
<div data-role="collapsibleset" data-content-theme="a" data-iconpos="right" id="items"></div>
    <h2 id="pagn">Items</h2>
	  </li>
                                                                        
<!--
    <h2>For pages of private group &quot;<?php echo $param['email'];?>&quot; only ...</h2>
 -->
	  <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For owner page only ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="dpage">Delete page</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="cpage">Change page</a>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="npage">Save new page</a>
	  </li>
                                                                        
    <li >
    	<label for="Ipgroups">Page visible for groups:</label>
		<select name="Ipgroups[]" id="Ipgroups" multiple="multiple" data-native-menu="false">
<!--       <option data-placeholder="true">Groups</option> -->
<?php
    echo "<option value='{$param['email']}'>{$param['email']}</option>";
$a =  explode(',',ltrim(rtrim($param['membergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
$a =  explode(',',ltrim(rtrim($param['ownergroups'],'}'),'{'));
foreach ($a as $k => $v){
    $w = ltrim(rtrim($v,'"'),'"');
    echo "<option value='{$w}'";
    echo ">{$w}</option>";
}
?>
		</select>
<button type="button" data-icon="eye" data-iconpos="right" data-mini="true" data-inline="true" id="spage">Change visible page</button>
	  </li>
<!--
    <li data-role="collapsible" data-iconpos="right" data-inset="false">
    <h2>For existing pages ...</h2>
    <a href="#" class="ui-btn ui-btn-corner-all ui-shadow" data-rel="back" id="spage">Publish page</a>
	  </li>
 -->     
	</ul>
  </div>
</div> <!-- page -->

    
<div data-role="page" data-dialog="true" data-mini="true" id="User">
  <div data-role="header" data-position="fixed">
    <h2>User</h2>
  </div>
  <div data-role="main" class="ui-content">
    <ul data-role="listview">
    <li>User: <?php echo $param['email']; ?></li>
    <li>Lang: <?php echo $param['lang']; ?></li>
    <li>Groups member:<br><?php echo $param['membergroups']; ?></li>
    <li>Groups owner:<br><?php echo $param['ownergroups']; ?></li>
    </ul>
	
  </div>
</div>

</form>
</body>
</html>
