<?php
include '../cis/dbfunctions.php';
include '../cis/validate.php';
//../cyf/cyf_contractform.php
$auth = new AuthClass();
$conn = $auth->connect();
$param = $auth->isAuth();

//$param['user_id'] $param['email'] $param['name'] $param['ownergroups'] $param['membergroups']
function checkMembergroups($ogroups,$mgroups){
    $m = explode(',', ltrim(rtrim($mgroups,'}"'),'{"'));
    $p = explode(',', ltrim(rtrim($ogroups,'}"'),'{"'));
    $a = array();
    foreach($m as $c){
        foreach($p as $s){
            if($c === $s) $a[] = $c;
//            else echo "$c != $s<br>";
        }
    }
    return implode(',', $a);
}

echo <<<EOT
<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
<link rel="stylesheet" href="../cis/bootstrap-3.3.7/css/bootstrap.min.css">
<script src="../cis/jquery-3.1.0.min.js"></script>
<script src="../cis/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<title>CYS Groups List</title>
<style>
label {padding-left:6px;}
input[type=checkbox] {margin-right:6px;}
input[type=radio] {margin-right:6px;}
input[type=text] {width:300px;}
textarea {width:300px;}
select {width:300px;}
legend {font-size:1.2em;}
sup {color:red;}
td {
margin-left: 6px;
padding-left:6px;
text-align:left;
}
</style>
<script>
function setParam(){
document.getElementById('frame').value='usersform';
document.getElementById('action').value='../cyf/cyf_usersform.php';
document.getElementById('usefilters').value='usefilters';
document.getElementById('fmanager').value=document.getElementById('imanager').value;
if(document.getElementById('ifemail').checked)
document.getElementById('femail').value=document.getElementById('iemail').value;
if(document.getElementById('ifcontact').checked)
document.getElementById('fcontact').value=document.getElementById('icontact').value;
if(document.getElementById('ifaddress').checked)
 document.getElementById('faddress').value=document.getElementById('iaddress').value;
if(document.getElementById('iflang').checked)
document.getElementById('flang').value=document.getElementById('ilang').value;
if(document.getElementById('iftel').checked)
document.getElementById('ftel').value=document.getElementById('itel').value;
if(document.getElementById('ifdstart').checked)
document.getElementById('fdstart').value=document.getElementById('idstart').value;
if(document.getElementById('ifdend').checked) 
document.getElementById('fdend').value=document.getElementById('idend').value;
if(document.getElementById('ifstatus').checked)
document.getElementById('fstatus').value=document.getElementById('istatus').value;

document.getElementById('userslist').submit();
}
</script>
</head>
<body onload="setParam();">
EOT;

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$last_error = 'Ok';
$cyf=cyfConnect();

//echo "post: ";print_r($_POST);echo "<br>";

$fmanager = $_REQUEST['fmanager'];
$res = pg_query($conn, "select string_agg(gname,',') from groups where owner='{$fmanager}';");
$arr = pg_fetch_array($res, 0, PGSQL_NUM);
$agroups = implode(',',$arr);

echo '<form id="usersform" target="usersform" method="POST" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'">';
echo "<input type='hidden' id='imanager' name='fmanager' value='{$fmanager}'>";

if(($_POST['tcmd']==="Delete") or ($_POST['tcmd']==="Update") or ($_POST['tcmd']==="Insert")){
    $c=array();
    $c['contact'] = "'".$_POST['contact']."'";
    $c['address'] = "'".$_POST['address']."'";
    $c['lang'] = "'".$_POST['lang']."'";
    $c['tel'] = "'".$_POST['tel']."'";
    $c['dstart'] = "'".$_POST['dstart']."'";
    $c['dend'] = "'".$_POST['dend']."'";
    $c['status'] = "'".$_POST['status']."'";
    $c['cname'] = "'".$_POST['cname']."'"; //$_POST['cname']
//    $c['manager'] = "'".$param['email']."'";
    $c['manager'] = "'".$fmanager."'";
    $c['membergroups'] = "'".$param['ownergroups']."'";

    if($_POST['tcmd']==="Delete"){
        $c['id'] = "'".$_POST['gid']."'";
        pgDelete($cyf, 'users', $c);
        $l=array();
        $l['email']= "'".$_POST['email']."'";
        pgDelete($conn, 'logins', $l);
        $g=array();
        $g['owner']= "'".$fmanager."'";
//        pgDelete($conn, 'groups', $g);
    }else{
//        $c['membergroups'] = "'".$_POST['membergroups']."'";
//      $c['apps'] = "'{".$_POST['apps']."}'";
        $l=array();
        if(!empty($_POST['psword']))  $l['psword']= "'".$_POST['psword']."'";
        if(!empty($_POST['contact'])) $l['name']  = "'".$_POST['contact']."'";
        $l['membergroups']= $c['membergroups'];
//        $l['ownergroups']= "'{}'";
        $l['ownergroups']= "'{{$_POST['email']}}'";
        if($_POST['tcmd']==="Update"){
            $cond=array();
            $cond['email']="'".$_POST['email']."'";
            pgUpdate($conn, 'logins', $l, $cond);
            $cond=array();
            $cond['id']="'".$_POST['gid']."'";
            $cond['email']="'".$_POST['email']."'";
            pgUpdate($cyf, 'users', $c, $cond);
            unset($_POST['gid']);
        }
        if($_POST['tcmd']==="Insert"){
            if($_POST['qusers'] >0)
                if(!empty($_POST['email'])){
                    if(!empty($_POST['psword'])){
                        $l['email']= "'".$_POST['email']."'";
//        echo "l:";print_r($l);echo "<br>";
                        pgInsert($conn, 'logins', $l);
                    }
                    $c['email'] = "'".$_POST['email']."'";
//        echo "c:";print_r($c);echo "<br>";
                    pgInsert($cyf, 'users', $c);
                    unset($_POST['gid']);
                }
        }
    }
}


if(!empty($_REQUEST['ifcontact']) or !empty($_REQUEST['ifaddress']) or !empty($_REQUEST['iflang']) or
   !empty($_REQUEST['iftel']) or !empty($_REQUEST['ifdstart']) or !empty($_REQUEST['ifdend']) or
   !empty($_REQUEST['ifstatus']) or !empty($_POST['ifemail'])){
    $c['contact'] =$_POST['contact'];
    $c['address'] =$_POST['address'];
    $c['tel'] =$_POST['tel'];
    $c['dstart'] =$_POST['dstart'];
    $c['dend'] =$_POST['dend'];
    $c['lang'] =$_POST['lang'];
    $c['status'] =$_POST['status'];
    $c['email'] =$_POST['email'];
}else{
    if(!empty($_POST['email']))
        $r = pg_query($cyf, "select * from users where manager='{$fmanager}' AND email='{$_POST['email']}';");
    else if(!empty($_POST['gid']))
        $r = pg_query($cyf, "select * from users where manager='{$fmanager}' AND id='{$_POST['gid']}';");
    else
        $r = pg_query($cyf, "select * from users where manager='{$fmanager}' limit 1;");
    if(!$r) $last_error = pg_last_error($cyf);
    else $c = pg_fetch_assoc ($r);
}
//curcount users
$curcount = 0;
$q = "SELECT sum(maxusers) as curcount FROM contracts WHERE manager='{$fmanager}';";
$r = @pg_query($cyf, $q);
if (!$r) $last_error = pg_last_error($cyf);
else{
    $arr = pg_fetch_assoc ($r);
    $curcount=$arr['curcount'];
}
//ordinal users?
$ocurcount = 0;
$q = "SELECT count(*) as ocurcount FROM users WHERE manager='{$fmanager}';";
//echo $q."<br>";
$r = @pg_query($cyf, $q);
if (!$r) $last_error = pg_last_error($cyf);
else{
    $arr = pg_fetch_assoc ($r);
    $ocurcount=$arr['ocurcount'];
}
//maxcount users
$maxcount = 0;
$q = "SELECT cname,usersgroups,maxusers FROM contracts WHERE '{{$fmanager}}' && contractgroups;";
$cont = array();
$memb = array();
$result = @pg_query($cyf, $q);
if (!$result) $last_error = pg_last_error($cyf);
else{
    $firows = pg_num_rows($result);
    $fi=getValues($result);
    for ($j=0;$j<$firows;$j++){
        $maxcount += $fi[$j]['maxusers'];
        $cont[] = $fi[$j]['cname'];
        $memb[] = $fi[$j]['ownergroups'];
    }
    $contract = implode(',',$cont);
    $membership = implode(',',$memb);
}
$tcurr=$maxcount-$ocurcount-$curcount;

echo "<input type='hidden' id='qusers' name='qusers' value='{$tcurr}'>";
echo "<input type='hidden' id='currid' name='gid' value='{$c['id']}'>";
$c['cname']=$contract;
echo "<table><caption style='text-align:center;'>Contract: {$contract}";
echo "<input type='hidden' id='icname' name='cname' value='{$c['cname']}'>";//{$c['cname']}
echo "<br>Users of this contract may be members of the following groups:<br>";//{$param['ownergroups']}
echo "Groups (membership): {$c['membergroups']}";
echo "<input type='hidden' name='membergroups' value='{$c['membergroups']}'>";
/* $v = ltrim(rtrim($c['membergroups'],'}'),'{'); */
/* echo $v; */
/* echo "</textarea>"; */
//echo "<i style='color:blue;'><br>Note: groups must be selected from the list above</i>";
echo "</caption>";

echo "<td style='text-align:center;font-weight: bold;'>User</td>";
echo "<td style='text-align:center;font-weight: bold;'>Filters</td></tr>";

echo "<tr><td>Contact<sup>*</sup>:</td>";
echo "<td><input type='text' id='icontact' name='contact' value='{$c['contact']}'></td>";
echo "<td><input type='checkbox' id='ifcontact' name='ifcontact'";
if(!empty($_POST['ifcontact'])) echo ' checked';
echo "><i style='font-size:12px;'>view with contact like this</i></td></tr>";

echo "<tr><td>Address<sup>*</sup>:</td>";
echo "<td><input type='text' id='iaddress' name='address' value='{$c['address']}'></td>";
echo "<td><input type='checkbox' id='ifaddress' name='ifaddress'";
if(!empty($_POST['ifaddress'])) echo ' checked';
echo "><i style='font-size:12px;'>view with address like this</i></td></tr>";

echo "<tr><td>tel<sup>*</sup>:</td>";
echo "<td><input type='text' id='itel' name='tel' value='{$c['tel']}'></td>";
echo "<td><input type='checkbox' id='iftel' name='iftel'";
if(!empty($_POST['iftel'])) echo ' checked';
echo "><i style='font-size:12px;'>view with tel like this</i></td></tr>";

echo "<tr><td>language:</td>";
echo "<td><input type='text' id='ilang' name='lang' value='{$c['lang']}'></td>";
echo "<td><input type='checkbox' id='iflang' name='iflang'";
if(!empty($_POST['iflang'])) echo ' checked';
echo "><i style='font-size:12px;'>view with language like this</i></td></tr>";

echo "<tr><td>Start&nbsp;date<sup>*</sup>:</td>";
echo "<td><input type='date' id='idstart' name='dstart' value='{$c['dstart']}'></td>";
echo "<td><input type='checkbox' id='ifdstart' name='ifdstart'";
if(!empty($_POST['ifdstart'])) echo ' checked';
echo "><i style='font-size:12px;'>view with start&nbsp;date <= this</i></td></tr>";

echo "<tr><td>End&nbsp;date<sup>*</sup>:</td>";
echo "<td><input type='date' id='idend' name='dend' value='{$c['dend']}'></td>";
echo "<td><input type='checkbox' id='ifdend' name='ifdend'";
if(!empty($_POST['ifdend'])) echo ' checked';
echo "><i style='font-size:12px;'>view with end&nbsp;date <= this</i></td></tr>";

echo "<tr><td>Status:</td>";
echo "<td><input type='text' id='istatus' name='status' value='{$c['status']}'></td>";
echo "<td><input type='checkbox' id='ifstatus' name='ifstatus'";
if(!empty($_POST['ifstatus'])) echo ' checked';
echo "><i style='font-size:12px;'>view with status like this</i></td></tr>";

echo "<tr><td></td>";
echo "<td></td>";
echo "<td>&nbsp;</td></tr>";

echo "<tr><td>email<sup>*</sup>:</td>";
echo "<td><input type='text' id='iemail' name='email' value='{$c['email']}'></td>";
echo "<td><input type='checkbox' id='ifemail' name='ifemail'";
if(!empty($_POST['ifemail'])) echo ' checked';
echo "><i style='font-size:12px;'>view with email like this</i></td></tr>";

echo "<tr><td>password<sup>*</sup>:</td>";
echo "<td><input type='password' id='ipsword' name='psword' value='{$password}'>";
echo "<i style='color:blue;font-size:12px;'>&nbsp;not&nbsp;empty&nbsp;for&nbsp;Add</i></td>";
echo "<td><i style='font-size:12px;'>may be empty for Update</i></td></tr>";

echo "</table>";

echo "<center>";
echo <<<EOT
<table style='width:50%'><caption style='text-align:center;'>
<i style='color:blue;font-size:14px;'>Users&nbsp;limit&nbsp;on&nbsp;a&nbsp;main&nbsp;contract:&nbsp;{$maxcount}.
<br>Now&nbsp;users&nbsp;(own/subcontract&nbsp;users):&nbsp;{$ocurcount}&#47;{$curcount}.
<br>Users&nbsp;available:&nbsp;{$tcurr}.</i>
</caption>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="Insert">Add</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Delete">Delete</td>
<td><input type="radio" id="tcmd" name="tcmd" value="Update">Update</td></tr>
<tr><td><input type="radio" id="tcmd" name="tcmd" value="View" checked>View</td>
<td></td><td><input type="submit" name="pcmd" value="Submit" style=""></td></tr>
</table>
EOT;

//echo '</table>';

$stat = explode('DETAIL:',$last_error);
echo '<div>Status: ';
if(count($stat) > 1){
    echo $stat[1];
}else echo $last_error;

echo <<<EOT
</center>
</form>

<form method="post" action="../cyf/cyf_userslist.php" target="userslist" id="userslist">
<input type="hidden" id="frame" name="frame">
<input type="hidden" id="action" name="action">
<input type="hidden" id="usefilters" name="usefilters">
<input type="hidden" id="fcname" name="fcname">
<input type="hidden" id="faddress" name="faddress">
<input type="hidden" id="fcontact" name="fcontact">
<input type="hidden" id="flang" name="flang">
<input type="hidden" id="ftel" name="ftel">
<input type="hidden" id="femail" name="femail">
<input type="hidden" id="fdstart" name="fdstart">
<input type="hidden" id="fdend" name="fdend">
<input type="hidden" id="fmanager" name="fmanager">
<input type="hidden" id="fstatus" name="fstatus">
<input type="hidden" id="fmembergroups" name="fmembergroups">
</form>
</body>
</html>
EOT;
?>