<?php
namespace Dao;
include_once __DIR__ . "/CisDatabase.php";

class HakaAccountsTable extends CisDatabase
{

    /**
     * HakaAccountsTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->tableName = "haka_accounts";
    }

    public function insert_user($display_name, $email, $detail) {
        return pg_insert($this->conn, $this->tableName, [
            "displayName"   => $display_name,
            "email"         => $email,
            "detail"        => $detail
        ]);
    }
}