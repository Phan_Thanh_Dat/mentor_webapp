<?php
namespace Dao;
include_once __DIR__ . "/GlobalConfiguration.php";

class CisDatabase
{
    public $conn;
    protected  $tableName;

    function __construct()
    {
        $this->conn = pg_connect(\GlobalConfiguration::getCisConnectionString());
    }

    public function execute($query_string) {
        $result = pg_prepare($this->conn, "raw_query", $query_string);
        $result = pg_execute($this->conn, "raw_query", []);
        return $result;
    }

    /**
     * Only support table which has "email" column
     *
     * @param $email
     */
    public function delete_user($email) {
        pg_delete($this->conn, $this->tableName, ["email" => $email]);
    }

    /**
     * Only support table which has "email" column
     *
     * @param $email
     * @return boolean
     */
    public function is_existed_user($email) {
        $user = $this->get_user($email);
        return !empty($user);
    }

    /**
     * Only support table which has "email" column
     *
     * @param $email
     * @return boolean
     */
    public function get_user($email) {
        $user = pg_select($this->conn, $this->tableName, ["email" => $email]);
        if ($user) {
            return $user[0];
        }
        return [];
    }

}