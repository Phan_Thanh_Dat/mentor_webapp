--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: interests; Type: TABLE; Schema: public; Owner: choos562; Tablespace: 
--

CREATE TABLE interests (
    id integer DEFAULT nextval('subscriptions_id_seq'::regclass) NOT NULL,
    content text NOT NULL,
    subscription_id integer
);


ALTER TABLE public.interests OWNER TO choos562;

--
-- Data for Name: interests; Type: TABLE DATA; Schema: public; Owner: choos562
--

COPY interests (id, content, subscription_id) FROM stdin;
11	{"en": "Hobby", "fi":"Harrastus"}	3
12	{"en": "Sports", "fi":"Urheilu"}	\N
13	{"en": "Culture", "fi":"Kulttuuri"}	\N
14	{"en": "Party", "fi":"Bileet"}	7
15	{"en": "Hanging out", "fi":"Hengailu"}	8
16	{"en": "Study", "fi":"Opiskelu"}	2
17	{"en": "Professional", "fi":"Ammatillinen"}	10
18	{"en": "University messages", "fi":"Yliopiston viestit"}	1
\.


--
-- Name: interests_id_key; Type: CONSTRAINT; Schema: public; Owner: choos562; Tablespace: 
--

ALTER TABLE ONLY interests
    ADD CONSTRAINT interests_id_key UNIQUE (id);


--
-- PostgreSQL database dump complete
--

